# Code Review

Below are the criteria used to evaluate code that has been submitted for review. 

### Validation test ✅

New code must be accompanied by a test that is already integrated into the makefiles such that it will be run by the nightly validations.  Modifications of existing code have to include a statement of which tests exercise the code.  It is expected that code has passed specific unit tests and is in the process of being validated by a full validation run with the compiler chosen by the developer.

### Performance test 🚀
New code must be accompanied by a test that evaluates the performance of the code.  This may be a separate test from the validation.  We currently do not require they be incorporated into the build system.  Results from the performance test must be provided from an ad hoc run of the test prior to commit.

### Code documentation 🗒
All functions classes in namespace stapl have appropriate doxygen comments that include a brief statement of the purpose of the class/function and its parameters.  Classes are documented with an explanation of the abstraction they provide and their purpose in any hierarchy they are part of. This is used to generate the doxygen documentation for STAPL.

All functions/classes not in `namespace stapl` have to be documented with an explanation of the purpose they serve.  Full documentation of parameters is not required.  If methods of a class are named in a self-explanatory manner they need not be documented.

Does the documentation make sense?  Are there typos or grammar errors?

> Future criteria: We currently do not include extended documentation of new components in a reference manual
>
> Future criteria: We currently do not require written documentation of the design of algorithms implemented (e.g. locality extraction), though some do exist in the design_docs portion of the STAPL repository.

### Interfaces 🏠 
Are parameters received by the appropriate reference? Are parameters that are commonly provided using temporary values in the calling code received by copy and moved into the class to avoid copies?

Are all data members of a class/struct protected/private?  Public data members require justification in the explanation of changes accompanying a diff.

Are classes/structs providing the appropriate public members?  get/set methods are likely not the proper interface and will be questioned.

### Code duplication 👯 

Does similar code exist in STAPL already that should be generalized instead of new code being added?

Is there new code in the diff that needs to be generalized to avoid duplication?

### Implementation evaluation 🔬 

The implementations of all functions is checked to ensure it does something the function name indicates it does (e.g. update_foo makes an assignment to foo).

For an imagined input, does the implementation seem to perform the required operations?  This is a mental validation of the code. 

If the implementation is too complex to allow this mental validation, it must include comments inline explaining the logic of the algorithm.

> Future criteria: complexity analysis of the implementations, especially of larger algorithms, though I think this should be covered in section 3.

### Code formatting 🗑 
The code submitted has to follow the coding conventions in stapl/docs/coding_conventions.  This is done while all of the above goes on, not as a separate pass.

# Validation

Before your change can be committed to trunk, it needs to be validated. This involves running the entire STAPL test suite with your patch.

You can perform a full validation by invoking make test:

```bash
make test &> test.log
```

This process will take several hours. In a separate window, you can view the progress:

```bash
tail -f test.log
```

### Determining if validation passed
Once the make command completes, you can inspect the log to see if there are any failures by using grep:

```bash
$ grep -i abort test.log
$ grep -i assert test.log
$ grep -i fail test.log
$ grep -i error test.log | grep -v Werror
```

If everything looks good, then validation passed.
