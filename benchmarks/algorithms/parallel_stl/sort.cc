/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include "../utilities.hpp"
#include "../timer.hpp"

#include <vector>
#include <iostream>

int main(int argc, char *argv[])
{
  size_t size;
  if (argc > 1)
    size = atoll(argv[1]);
  else
    size=13;
  int num_threads;
  if (argc > 2)
    num_threads = atoi( argv[2] );
  else
    num_threads = 1;

  // Force the use of parallel STL versions of the algorithms 
  __gnu_parallel::_Settings s;
  s.algorithm_strategy = __gnu_parallel::force_parallel;
  __gnu_parallel::_Settings::set(s);

  omp_set_num_threads(num_threads);

  std::vector<double> par_samples(10,0.), sor_samples(10,0.),
    nth_samples(10,0.), min_samples(10,0.), max_samples(10,0.),
    lex_samples(10,0.), sort_samples(10,0.), sta_samples(10,0.);

  bool par_correct(true), sor_correct(true), nth_correct(true),
    min_correct(true), max_correct(true), lex_correct(true),
    sort_correct(true), sta_correct(true);

  // MIN ELEMENT
  for (int sample = 0; sample != 10; ++sample)
  {
    std::vector<data_t> x(size);
    fill_random(x);

    auto time = start_timer();

    double result = *(__gnu_parallel::min_element(x.begin(), x.end()));

    min_samples[sample] = stop_timer(time);

    // Validate
    auto found = std::find_if(x.begin(), x.end(),
      [result](double i){return i<result;});
    if (found != std::end(x))
      min_correct = false;
  }
  report_result("b_min_element","gnu_parallel", min_correct, min_samples);

  // MAX ELEMENT
  for (int sample = 0; sample != 10; ++sample)
  {
    std::vector<data_t> x(size);
    fill_random(x);

    auto time = start_timer();

    double result = *(__gnu_parallel::max_element(x.begin(), x.end()));

    max_samples[sample] = stop_timer(time);

    // Validate
    auto found = std::find_if(x.begin(), x.end(),
      [result](double i){return i>result;});
    if (found != std::end(x))
      max_correct = false;
  }
  report_result("b_max_element","gnu_parallel", max_correct, max_samples);

  // LEXICOGRAPHICAL COMPARE
  for (int sample = 0; sample != 10; ++sample)
  {
    std::vector<data_t> x(size);
    std::vector<data_t> y(size);
    fill_random(x);
    y = x;

    auto time = start_timer();

    bool first_less = __gnu_parallel::lexicographical_compare(
      x.begin(), x.end(), y.begin(), y.end());

    lex_samples[sample] = stop_timer(time);

    lex_correct = lex_correct && !first_less;
  }
  report_result("b_lex_compare","gnu_parallel", lex_correct, lex_samples);

  return 0;
}
