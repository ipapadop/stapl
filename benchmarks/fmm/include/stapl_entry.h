/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_BENCHMARKS_FMM_STAPL_ENTRY_H
#define STAPL_BENCHMARKS_FMM_STAPL_ENTRY_H

#include <stapl/utility/do_once.hpp>

#include <stapl/stream.hpp>
#include <stapl/runtime.hpp>

#include <stapl/array.hpp>
#include <stapl/vector.hpp>
#include <stapl/views/proxy/proxy.hpp>
#include <stapl/skeletons/utility/tags.hpp>
#include <stapl/views/metadata/coarseners/all_but_last.hpp>
#include <stapl/views/metadata/coarseners/multiview.hpp>
#include <stapl/skeletons/functional/map.hpp>
#include <stapl/skeletons/functional/allreduce.hpp>
#include <stapl/skeletons/functional/alltoall.hpp>
#include <stapl/skeletons/functional/sink_value.hpp>
#include <stapl/skeletons/functional/broadcast_to_locs.hpp>
#include <stapl/skeletons/map.hpp>
#include <stapl/skeletons/executors/execute.hpp>
#include <stapl/skeletons/environments/graphviz_env.hpp>
#include <stapl/algorithms/algorithm_fwd.hpp>
#include <stapl/algorithms/algorithm.hpp>
#include <stapl/algorithms/functional.hpp>
#include <stapl/skeletons/functional/notify_map.hpp>
#include <boost/shared_ptr.hpp>
#include <stapl/skeletons/utility/lightweight_vector.hpp>
#include <stapl/skeletons/functional/allgather.hpp>
#include <iostream>
#include <typeinfo>


#define STRONG_SCALE(BODIES, SIZE) (BODIES)
#define WEAK_SCALE(BODIES, SIZE) (BODIES*SIZE)

#define STRONG_SCALE_RANK(BODIES, SIZE) (BODIES/SIZE)
#define WEAK_SCALE_RANK(BODIES, SIZE) (BODIES)
#define PRINT 0


using namespace std;
class EntrySTAPL
{
public:
  size_t rank;
  size_t size;

  EntrySTAPL()
  {
    rank = stapl::get_location_id();
    size = stapl::get_num_locations();
  }
};


template <typename V1, typename V2 = typename std::decay<V1>::type>
struct is_stapl_proxy
  : public false_type
{ };

template <typename V1, typename T, typename A>
struct is_stapl_proxy<V1, stapl::proxy<T, A>>
  : public true_type
{ };

#endif // STAPL_BENCHMARKS_FMM_STAPL_ENTRY_H
