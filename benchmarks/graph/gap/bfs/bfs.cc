/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/containers/graph/algorithms/graph_io.hpp>
#include <stapl/containers/graph/algorithms/breadth_first_search.hpp>

#include "../../common/command_line_app.hpp"
#include "../../common/sources.hpp"
#include "../../common/graph_benchmark.hpp"
#include "verify.hpp"
#include <string>

void run_breadth_first_search(const command_line_app<no_options>& app)
{
  auto trials = app.trials();
  auto vw = app.create_graph<stapl::properties::bfs_property>();
  auto policy = app.execution_policy(vw);

  using sources_type = std::vector<std::uint32_t>;

  auto bench = benchmark_builder{}
    .with_setup([&]() -> sources_type {
      return select_sources(vw, trials);
    })
    .with_runner([&](int trial, sources_type const& sources) {
      return breadth_first_search(policy, vw, sources[trial]);
    })
    .with_stats([&](std::size_t iters) { return iters; })
    .with_verifier([&](std::size_t iters) {
      return verify_bfs(vw);
    })
    .with_trials(trials)
    .build();
  bench(std::cout);
}

stapl::exit_code stapl_main(int argc, char* argv[])
{
  command_line_app<no_options> app;
  app(argc, argv);

  run_breadth_first_search(app);

  return EXIT_SUCCESS;
}
