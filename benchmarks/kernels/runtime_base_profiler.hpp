/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_BENCHMARKS_KERNELS_RUNTIME_BASE_PROFILER_HPP
#define STAPL_BENCHMARKS_KERNELS_RUNTIME_BASE_PROFILER_HPP

#include <stapl/runtime.hpp>
#include <stapl/profiler/base_profiler.hpp>
#include <cstring>
#include <iostream>
#include <iomanip>

//////////////////////////////////////////////////////////////////////
/// @brief Base profiler implementation.
///
/// @ingroup performanceMonitor
//////////////////////////////////////////////////////////////////////
template<typename Counter = stapl::counter<stapl::default_timer>>
class runtime_base_profiler
: public stapl::base_profiler<Counter>
{
private:
  typedef stapl::base_profiler<Counter> base_type;

public:
  using base_type::report;

  explicit runtime_base_profiler(std::string const& inname,
                                 int argc = 0, char **argv = nullptr)
  : base_type(inname, "STAPL", argc, argv)
  { }

  void report(std::stringstream& ss)
  {
    if (stapl::get_location_id()==0) {
      ss << std::fixed << std::setprecision(8);
      ss << this->get_name()       << '\t'
         << this->get_iterations() << '\t'
         << this->get_avg()        << '\t'
         << this->get_min()        << '\t'
         << this->get_max()        << '\t'
         << this->get_conf()       << std::endl;
    }
  }
};


inline void print_parallelism(std::ostream& os)
{
  if (stapl::get_location_id()==0) {
    os << stapl::get_num_locations() << ' ';
    if (stapl::get_available_levels()>0)
      stapl::execute([&os] { print_parallelism(os); });
  }
}

#endif
