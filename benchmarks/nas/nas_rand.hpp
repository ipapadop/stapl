/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef NAS_RAND_HPP
#define NAS_RAND_HPP

inline
double randlc(double& seed, double const& a)
{
  const double d2m46     = std::pow((double) 0.5, 46);
  const long int i246m1  = 0x00003FFFFFFFFFFF;
  const long int La      = a;

  long int Lx = seed;

  Lx = (Lx * La) & i246m1;
  seed = static_cast<double>(Lx);

  return d2m46 * static_cast<double>(Lx);
}


// FIXME - template with View / no more pointers
//
void vranlc(int const& n, double& seed, double const& a, double* y)
{
  stapl_assert(sizeof(long int) == 8, "long int is not 8 bytes");

  const double d2m46     = std::pow((double) 0.5, 46);
  const long int i246m1  = 0x00003FFFFFFFFFFF;
  const long int La      = a;

  long int Lx = seed;

  for (int i=0; i<n; ++i)
  {
    Lx = (Lx * La) & i246m1;
    y[i] = d2m46 * static_cast<double>(Lx); 
  }

  seed = static_cast<double>(Lx);
}


inline
double compute_seed(double problem_seed, double an, size_t offset)
{
  randlc(an, an);

  for (size_t i=1; i<=100; ++i)
  {
    if (offset & 1)
      randlc(problem_seed, an);

    offset /= 2;
   
    if (offset == 0)
      break;

    randlc(an, an);
  }

  return problem_seed;
}

#endif // ifndef NAS_RAND_HPP
