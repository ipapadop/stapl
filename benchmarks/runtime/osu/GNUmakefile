# Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
# component of the Texas A&M University System.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

ifndef STAPL
  STAPL = $(shell echo "$(PWD)" | sed 's,/benchmarks/runtime/osu,,')
endif

include $(STAPL)/GNUmakefile.STAPLdefaults

OBJS=async_rmi_acc_latency async_rmi_bw async_rmi_latency async_rmi_multi_lat \
     sync_rmi_acc_latency sync_rmi_bw sync_rmi_cas_latency \
       sync_rmi_fop_latency sync_rmi_latency \
     collective one_sided_collective sync \
     serialization_latency
LIB+=-l$(shell ls $(Boost_LIBRARY_DIRS) | grep -e boost_program_options | head -n 1 | sed 's/lib//g' | sed 's/\.so//g' | sed 's/\.a//g')

ifdef BOOST_MPI_LIBRARY_DIR
  BOOST_MPI_LIB=-L$(BOOST_MPI_LIBRARY_DIR) -l$(shell ls $(BOOST_MPI_LIBRARY_DIR) | grep -e boost_mpi | head -n 1 | sed 's/lib//g' | sed 's/\.so//g' | sed 's/\.a//g')
  OBJS+=boost_mpi_latency
endif

default: compile

test: all

all: compile

compile: $(OBJS)

clean:
	rm -rf *.o core* a.out ii_files rii_files $(OBJS)

boost_%: boost_%.cc
	${CC} ${CXXFLAGS} -DNDEBUG -o $@ $< ${BOOST_MPI_LIB} ${LIB} ${LIB_EPILOGUE}
