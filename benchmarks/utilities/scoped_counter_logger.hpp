/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_BENCHMARKS_GAP_SCOPED_COUNTER_LOGGER_HPP
#define STAPL_BENCHMARKS_GAP_SCOPED_COUNTER_LOGGER_HPP

#include <stapl/runtime.hpp>
#include <stapl/utility/do_once.hpp>

//////////////////////////////////////////////////////////////////////
/// @brief A scoped object that uses RAII to start a counter when being
///        initialized and print the value of the counter when the scope
///        ends.
///
///        This could use std::make_scope_exit if it makes it to C++17.
///
/// @tparam T The type of the counter
//////////////////////////////////////////////////////////////////////
template<typename T>
struct scoped_counter_logger
{
private:
  using counter_type = stapl::counter<T>;

  std::string m_message;
  counter_type m_counter;

public:
  scoped_counter_logger(std::string message)
    : m_message(std::move(message)), m_counter{}
  {
    m_counter.start();
  }

  ~scoped_counter_logger(void)
  {
    const auto count = m_counter.stop();
    stapl::do_once([&]() {
      std::cout << m_message << '\t' << count << std::endl;
    });
  }
};

#endif
