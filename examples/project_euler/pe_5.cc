/*
// Copyright (c) 2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <iostream>
#include <algorithm>
#include <utility>
#include <time.h>
#include <stapl/utility/do_once.hpp>
#include <stapl/array.hpp>
#include <stapl/algorithm.hpp>
#include <stapl/views/counting_view.hpp>
#include <stapl/algorithms/functional.hpp>


using namespace stapl;


struct divid
{
  template <typename T>
  long operator() (T x)
  {
    int counter = 0;

    for (int i = 1; i < 21; ++i)
    {
      if (x%i == 0)
      {
        counter++;
      }
    }
    if (counter == 20)
    {
      return x;
    } else {
      return std::numeric_limits<long>::max();
    }
  }
};


stapl::exit_code stapl_main(int argc, char* argv[])
{
  long i = 1;
  long result = 1;
  long n = 5000000;

  do {
    auto vw = counting_view(n,i);

    result = map_reduce(divid(), stapl::min<long> (), vw);

    if (result != std::numeric_limits<long>::max())
    {
      stapl::do_once( [&] {
        std::cout << " result is "<< result << std::endl;
      });
    }

    // double the size of the range for the next  map_reduce call.
    i=n+1;
    n+=n;
  } while (result == std::numeric_limits<long>::max());

  return EXIT_SUCCESS;
}
