/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <fstream>
#include <stapl/views/repeated_view.hpp>
#include <stapl/stream.hpp>
#include <stapl/runtime.hpp>
#include <stapl/algorithm.hpp>
#include <stapl/skeletons/serial.hpp>
#include <stapl/utility/do_once.hpp>
#include <stapl/vector.hpp>

using namespace std;

typedef stapl::vector<double> vec_type;
typedef stapl::vector_view<vec_type> vec_view_type;

template<typename Value>
struct msg_val
{
  private:
  const char* m_txt;
  Value m_val;
  public:
    msg_val(const char* text,Value val)
      : m_txt(text),m_val(val)
    { }

  typedef void result_type;
  result_type operator()()
  {
    cout << m_txt<< " " <<m_val << endl;
  }
  void define_type(stapl::typer& t)
  {
    t.member(m_txt);
  }
};

struct get_val_wf
{
  private:
    stapl::stream<ifstream> m_zin;
  public:
    get_val_wf(stapl::stream<ifstream> const& zin)
      :m_zin(zin)
    {}

    typedef void result_type;
    template <typename Ref>
    result_type operator()(Ref val)
    {
      m_zin>>val;
    }

    void define_type(stapl::typer& t)
    {
      t.member(m_zin);
    }
};

struct exp_wf
{
  typedef double result_type;
  template <typename View, typename Mean>
  result_type operator()(View v, Mean m)
  {
    return pow(v-m,2);
  }
};

stapl::exit_code stapl_main(int argc, char **argv)
{
  vec_type vec(boost::lexical_cast<size_t>(argv[1]));
  vec_view_type vec_view(vec);
  stapl::stream<ifstream> zin;
  zin.open(argv[2]);
  stapl::serial_io(get_val_wf(zin),vec_view);
  stapl::generate( vec_view, stapl::sequence<double>(0,1));
  double n = vec.size();
  double mean = stapl::accumulate(vec_view,0) / n;
  double variance=stapl::map_reduce(exp_wf(), stapl::plus<double>(),
    vec_view, stapl::make_repeat_view(mean));
  variance /= n-1;
  double s_dev = sqrt(variance);
  stapl::do_once( msg_val<double>( "Mean:", mean ) );
  stapl::do_once( msg_val<double>( "Variance:", variance ) );
  stapl::do_once( msg_val<double>( "Standard deviation:", s_dev ) );
  return EXIT_SUCCESS;
}

