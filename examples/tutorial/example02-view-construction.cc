/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#include <algorithm>
#include <utility>
#include <iostream>
#include <stapl/vector.hpp>
#include <stapl/array.hpp>
#include <stapl/utility/do_once.hpp>

stapl::exit_code stapl_main(int argc, char **argv)
{
  // Construction of one STAPL vector of strings with 5 elements and
  // one STAPL array of integers with 10 elements.
  stapl::vector<std::string> vec_strings(5, "Howdy");
  stapl::array<int> array_ints(10, 7);

  // Construction of one STAPL vector view and one STAPL array view.
  // Note that we need the container and its type to declare a view.
  // Once in a view, we can access the information of the container
  // through the view.
  stapl::vector_view<stapl::vector<std::string> > vector_view(vec_strings);
  stapl::array_view<stapl::array<int> > array_view(array_ints);

  // Print container data. The primary use of stapl::do_once() is to
  // allow formatted output from a single location in an SPMD section.
  // This function should not be used regularly in applications,
  // because all locations wait until its conclusion, i.e., no parallel
  // computation is occurring.
  stapl::do_once(

    // Use lambda function to print in stapl::do_once().
    [](stapl::vector_view<stapl::vector<std::string> > vector_view,
       stapl::array_view<stapl::array<int> > array_view){

       // Pop back one "Howdy" and push back "Gig 'em" in the vector view.
       vector_view.pop_back();
       vector_view.push_back("Gig 'em");

       // Change the first 7 for one 11 in the array view.
       array_view[0] = 11;

       // Get container sizes through their views.
       std::size_t v_sz = vector_view.size();
       std::size_t a_sz = array_view.size();

       // Print data through their views.
       for (std::size_t i = 0; i < v_sz; ++i)
       {
          std::cout << "Vector[" << i << "] = " << vector_view[i] << std::endl;
       }

       for (std::size_t i = 0; i < a_sz; ++i)
       {
          std::cout << "Array[" << i << "] = " << array_view[i] << std::endl;
       }

    }

  // stapl::do_once() parameters
  , vector_view, array_view);

  return EXIT_SUCCESS;
}
