/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/runtime/communicator/collective.hpp>
#include <stapl/runtime/gang_md.hpp>
#include <stapl/runtime/gang_md_registry.hpp>
#include <functional>
#include <mutex>
#include <unordered_map>
#include <utility>
#include <boost/functional/hash.hpp>

namespace stapl {

namespace runtime {

/// @ref collective object registry.
static std::unordered_map<
         collective::id, collective, boost::hash<collective::id>
       > collective_registry;

/// @ref collective object registry mutex.
static std::mutex collective_registry_mtx;


// Creates a new collective handle or returns an already existing one
collective& collective::get(collective::id const& id, topology const& t)
{
  std::lock_guard<std::mutex> lock{collective_registry_mtx};
  return collective_registry.emplace(
           std::piecewise_construct,
           std::forward_as_tuple(std::cref(id)),
           std::forward_as_tuple(std::cref(id), std::cref(t))).first->second;
}


// Creates a new collective handle or returns an already existing one
collective& collective::get(collective::id const& id)
{
  std::lock_guard<std::mutex> lock{collective_registry_mtx};
  auto it = collective_registry.find(id);
  if (it!=collective_registry.end())
    return it->second;

  // collective does not exist, create a new one
  auto const& t = gang_md_registry::get(id.first).get_topology();
  return collective_registry.emplace(
           std::piecewise_construct,
           std::forward_as_tuple(std::cref(id)),
           std::forward_as_tuple(std::cref(id), std::cref(t))).first->second;
}


// Attempts to destroy the handle
void collective::try_destroy(void) noexcept
{
  std::lock_guard<std::mutex> lock1{collective_registry_mtx};
  STAPL_RUNTIME_ASSERT(collective_registry.count(m_id)!=0);

  {
    std::lock_guard<std::mutex> lock2{m_mtx};
    STAPL_RUNTIME_ASSERT(!bool(m_notify_arrived));
    // if not empty, a new collective started with the same id
    if (!m_arrived.empty())
      return;
  }

  // remove the object from the registry
  collective_registry.erase(m_id);
}

} // namespace runtime

} // namespace stapl
