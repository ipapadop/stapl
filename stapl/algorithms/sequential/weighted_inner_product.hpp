/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_ALGORITHMS_SEQUENTIAL_WEIGHTED_INNER_PRODUCT_HPP
#define STAPL_ALGORITHMS_SEQUENTIAL_WEIGHTED_INNER_PRODUCT_HPP


namespace stapl {
namespace sequential {

template<class Iter1, class Iter2, class Iter3, class T>
T weighted_inner_product(Iter1 first1, Iter1 last1, Iter2 first2, Iter3 first3,
                         T value)
{
    for(; first1 != last1; ++first1, ++first2, ++first3) {
         value += *first1 * *first2 * *first3;
    }
    return value;
}

template<class Iter1, class Iter2, class Iter3, class T, class Op1, class Op2>
T weighted_inner_product(Iter1 first1, Iter1 last1, Iter2 first2, Iter3 first3,
                         T value, Op1 op1, Op2 op2)
{
    for(; first1 != last1; ++first1, ++first2, ++first3) {
         value = op1(value, op2( op2(*first1, *first2) , *first3));
    }
    return value;
}

} } // namespace stapl::sequential

#endif

