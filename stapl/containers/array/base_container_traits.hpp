/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_ARRAY_BASE_CONTAINER_TRAITS_HPP
#define STAPL_ARRAY_BASE_CONTAINER_TRAITS_HPP

#include <vector>

#include <stapl/containers/type_traits/define_value_type.hpp>
#include <stapl/skeletons/utility/lightweight_multiarray_storage.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Default traits for the array base container. Specifies customizable
///   type parameters that could be changed on a per-container basis.
/// @ingroup parrayTraits
///
/// @tparam T Type of the stored elements in the base container.
/// @tparam Domain Domain for the base container.
/// @see array_base_container
////////////////////////////////////////////////////////////////////////
template <typename T, typename Domain>
struct array_base_container_traits
{
  typedef typename define_value_type<T>::type   stored_type;
  typedef std::vector<stored_type>              container_type;
  typedef container_type                        container_constructor;
  typedef T                                     value_type;
  typedef Domain                                domain_type;
};


//////////////////////////////////////////////////////////////////////
/// @brief Traits for the array base container that use an underlying
/// container_type (@ref lightweight_multiarray_storage), that avoids
/// default initialization of values when possible (for now, when the
/// stored datatype is fundamental).
///
/// @todo Consider expanding use of non initialized storage beyond
/// fundamentals.  Need to better understand effects of avoiding default
/// initialization.
//////////////////////////////////////////////////////////////////////
template <typename T, typename Domain>
struct no_init_array_base_container_traits
{
  typedef typename define_value_type<T>::type   stored_type;

  typedef typename std::conditional<
     std::is_fundamental<stored_type>::value,
     lightweight_multiarray_storage<stored_type>,
     std::vector<stored_type>
  >::type                                       container_type;
  typedef container_type                        container_constructor;
  typedef T                                     value_type;
  typedef Domain                                domain_type;
};

} // namespace stapl

#endif
