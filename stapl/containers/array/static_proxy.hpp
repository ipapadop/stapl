/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_STATIC_ARRAY_PROXY_HPP
#define STAPL_CONTAINERS_STATIC_ARRAY_PROXY_HPP

#include "static_array_fwd.hpp"

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Specialization used when the type is a static_array. Refer to @ref
/// static_array for proper use of the container itself.
/// @ingroup parrayDistObj
///
/// @see static_array proxy
////////////////////////////////////////////////////////////////////////
template <typename T, typename Accessor>
class proxy<stapl::static_array<T>, Accessor>
  : public Accessor
{
private:
  friend class proxy_core_access;

  typedef stapl::static_array<T>               target_t;
  typedef typename target_t::iterator          iter_t;
  typedef typename target_t::const_iterator    const_iter_t;

public:
  typedef typename target_t::size_type         size_type;
  typedef typename target_t::gid_type          gid_type;
  typedef typename target_t::gid_type          index_type;
  typedef typename target_t::value_type        value_type;
  typedef typename target_t::reference         reference;
  typedef typename target_t::const_reference   const_reference;
  typedef typename target_t::domain_type       domain_type;
  typedef iter_t                               iterator;
  typedef const_iter_t                         const_iterator;

  explicit proxy(Accessor const& acc)
    : Accessor(acc)
  { }

  operator target_t(void) const
  {
    return Accessor::read();
  }

  proxy const& operator=(proxy const& rhs)
  {
    Accessor::write(rhs);
    return *this;
  }

  proxy const& operator=(target_t const& rhs)
  {
    Accessor::write(rhs);
    return *this;
  }

  size_type size(void) const
  {
    return Accessor::const_invoke(&target_t::size);
  }

  void resize(size_t size)
  {
    Accessor::invoke(&target_t::resize, size);
  }

  bool empty(void) const
  {
    return Accessor::const_invoke(&target_t::empty);
  }

  reference operator[](size_type gid)
  {
    return Accessor::invoke(&target_t::operator[], gid);
  }

  const_reference operator[](size_type gid) const
  {
    return Accessor::const_invoke(&target_t::operator[], gid);
  }

  domain_type domain(void) const
  {
    return Accessor::const_invoke(&target_t::domain);
  }

  iterator make_iterator(gid_type const& gid)
  {
    return Accessor::invoke(&target_t::make_iterator, gid);
  }

  const_iterator make_iterator(gid_type const& gid) const
  {
    return Accessor::const_invoke(&target_t::make_iterator, gid);
  }

  void set_element(gid_type const& gid, value_type const& value)
  {
    Accessor::invoke(&target_t::set_element, gid, value);
  }

  template<typename View>
  void set_elements(gid_type const& gid, View const& view)
  {
    Accessor::invoke(&target_t::template set_elements<View const&>, gid, view);
  }

  void define_type(typer& t)
  {
    t.base<Accessor>(*this);
  }

}; // struct proxy

} // namespace stapl

#endif // STAPL_CONTAINERS_ARRAY_PROXY_HPP
