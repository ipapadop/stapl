/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_DISTRIBUTION_BASE_CONTAINER_METADATA_HPP
#define STAPL_CONTAINERS_DISTRIBUTION_BASE_CONTAINER_METADATA_HPP

#include <stapl/views/metadata/container/base_container_wrapper.hpp>

namespace stapl {


//////////////////////////////////////////////////////////////////////
/// @brief Class for computing the metadata of base-containers.
/// This class contains information needed to determine the locality metadata
/// information for a base container.
/// @tparam BC Type of the base-container.
//////////////////////////////////////////////////////////////////////
template<typename BC>
struct base_container_metadata
{
  using index_type = typename BC::domain_type::index_type;
  using md_cont_type = metadata::base_container_wrapper<BC>;
  using return_type = std::pair<bool, md_cont_type*>;

  //////////////////////////////////////////////////////////////////////
  /// @brief Return the metadata of the specified base-container.
  /// @param bc A pointer to the base-container.
  ///
  /// @return a pair indicating if the metadata container is static and
  ///   balance distributed and a pointer to the metadata container.
  //////////////////////////////////////////////////////////////////////
  return_type operator()(BC* bc)
  {
    return std::make_pair(false, new md_cont_type(bc));
  }
};

} // namespace stapl

#endif /* STAPL_CONTAINERS_DISTRIBUTION_BASE_CONTAINER_METADATA_HPP */
