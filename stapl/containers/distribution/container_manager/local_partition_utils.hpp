/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_LOCAL_PARTITION_UTILS_HPP
#define STAPL_CONTAINERS_LOCAL_PARTITION_UTILS_HPP

namespace stapl {

namespace cm_impl {

//////////////////////////////////////////////////////////////////////
/// @brief Comparator used to sort partial domain information in ascending
/// order of location id and then partition id.
//////////////////////////////////////////////////////////////////////
template <typename PartitionInfo>
struct loc_and_part_id_less
{
  typedef bool result_type;

  //////////////////////////////////////////////////////////////////////
  /// @brief Returns whether the location id of the first parameter is less
  /// than the location id of the second parameter, and if they are equal
  /// whether the partition id of the first is less than the second.
  ///
  /// @param lh tuple of partial domain, partition id, and location id
  /// @param rh tuple of partial domain, partition id, and location id
  /// @return true if the partition id of lh is less than the
  /// partition id of rh
  //////////////////////////////////////////////////////////////////////
  template <typename PartitionInfoRef0, typename PartitionInfoRef1>
  result_type operator()(PartitionInfoRef0 lh, PartitionInfoRef1 rh)
  {
    return get<2>(lh) != get<2>(rh) ?
             get<2>(lh) < get<2>(rh) :
             get<1>(lh) < get<1>(rh);
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Comparator used to identify adjacent elements in the vector
/// of partition information that belong to different locations.
//////////////////////////////////////////////////////////////////////
struct part_loc_neq
{
  typedef bool result_type;

  //////////////////////////////////////////////////////////////////////
  /// @brief Returns whether the location id of the first parameter is
  /// not equal to the location id of the second parameter.
  /// @param lh tuple of partial domain, partition id, and location id
  /// @param rh tuple of partial domain, partition id, and location id
  /// @return true if the location id of lh is not equal the location id of rh
  //////////////////////////////////////////////////////////////////////
  template <typename PartitionInfoRef>
  result_type operator()(PartitionInfoRef lh, PartitionInfoRef rh)
  {
    return get<2>(lh) != get<2>(rh);
  }
};

} // namespace cm_impl

} // namespace stapl

#endif
