/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_DISTRIBUTION_IS_DISTRIBUTION_VIEW_FWD_HPP
#define STAPL_CONTAINERS_DISTRIBUTION_IS_DISTRIBUTION_VIEW_FWD_HPP

#include <stapl/views/type_traits/is_view.hpp>

namespace stapl {

template <typename V, bool is_vw = is_view<V>::value>
struct is_distribution_view;

template <typename DistributionView, typename PartitionInfoContainer>
struct view_based_partition;

template <typename DistributionView>
struct view_based_mapper;

template <typename T>
struct is_view_based;

template <typename T>
struct is_arbitrary_view_based;

namespace detail {

BOOST_MPL_HAS_XXX_TRAIT_DEF(is_composed_dist_spec)

}

} // namespace stapl

#endif // STAPL_CONTAINERS_DISTRIBUTION_IS_DISTRIBUTION_VIEW_FWD_HPP
