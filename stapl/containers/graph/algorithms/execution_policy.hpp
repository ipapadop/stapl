/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_EXECUTION_POLICY_HPP
#define STAPL_CONTAINERS_EXECUTION_POLICY_HPP

#include <boost/variant/variant.hpp>

namespace stapl {

namespace sgl {

//////////////////////////////////////////////////////////////////////
/// @brief Base class for all SGL execution policies
//////////////////////////////////////////////////////////////////////
class execution_policy_base
{
  double m_active_vertex_ratio;

public:
  execution_policy_base(void)
    : m_active_vertex_ratio(0.5)
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief Set the active vertex ratio, which is a rough estimate of how
  /// many vertices will be active in a given superstep during the traversal.
  //////////////////////////////////////////////////////////////////////
  void active_ratio(double ratio)
  {
    m_active_vertex_ratio = ratio;
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Retrieve the active vertex ratio
  //////////////////////////////////////////////////////////////////////
  double active_ratio() const
  {
    return m_active_vertex_ratio;
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Graph execution policy to execute a traversal using the
///        k-level-asynchronous strategy.
//////////////////////////////////////////////////////////////////////
class kla_policy
  : public execution_policy_base
{
  size_t m_k;

public:
  kla_policy(size_t k = 0)
    : m_k(k)
  { }

  size_t k() const
  {
    return m_k;
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Graph execution policy to execute a traversal level-synchronously.
//////////////////////////////////////////////////////////////////////
class level_sync_policy
  : public kla_policy
{
  using base_type = kla_policy;
public:
  level_sync_policy() = default;
};

//////////////////////////////////////////////////////////////////////
/// @brief Graph execution policy to execute a traversal asynchronously.
//////////////////////////////////////////////////////////////////////
class async_policy
  : public kla_policy
{
  using base_type = kla_policy;
public:
  async_policy()
    : base_type(std::numeric_limits<size_t>::max()-1)
  { }
};



//////////////////////////////////////////////////////////////////////
/// @brief An execution policy for a graph algorithm's traversal.
///
/// @tparam View The input graph view
//////////////////////////////////////////////////////////////////////
template<typename View>
using execution_policy = boost::variant<
  level_sync_policy,
  kla_policy,
  async_policy
>;


//////////////////////////////////////////////////////////////////////
/// @brief A helper to create execution policies for a given graph.
///
/// @param vw The input graph view
/// @param paradigm The paradigm to execute the graph algorithm
/// (lsync,async,kla)
/// @param tunable_param A tunable parameter for use by the paradigm selected
/// e.g. 'k' in KLA
//////////////////////////////////////////////////////////////////////
template<typename View>
execution_policy<View> make_execution_policy(
    const std::string& paradigm, View& vw, size_t tunable_param = 0)
{
  if (paradigm == "lsync") {
    return level_sync_policy{};
  } else if (paradigm == "async") {
    return async_policy{};
  } else if (paradigm == "kla") {
    const size_t k = tunable_param;
    return kla_policy{k};
  } else {
    stapl::abort("Unknown paradigm type " + paradigm + "\n");
    return level_sync_policy{};
  }
}

} // namespace sgl

} // stapl namespace

#endif
