/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_CSR_UTILS_HPP
#define STAPL_CONTAINERS_CSR_UTILS_HPP

#include <stapl/containers/graph/csr_graph.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Metafunction to determine whether a graph is a CSR graph.
//////////////////////////////////////////////////////////////////////
template<typename Graph>
struct is_csr
  : std::false_type
{ };

//////////////////////////////////////////////////////////////////////
/// @brief Specialization for CSR graphs
//////////////////////////////////////////////////////////////////////
template<graph_attributes D, graph_attributes M,
         typename VP, typename EP,
         typename PS, typename Map,
         typename Traits>
struct is_csr<csr_graph<D, M, VP, EP, PS, Map, Traits>>
  : std::true_type
{ };


namespace detail {

//////////////////////////////////////////////////////////////////////
/// @brief Helper function to commit/uncommit a graph if it is CSR
//////////////////////////////////////////////////////////////////////
template<typename Graph, bool = is_csr<Graph>::value>
struct try_commit_impl
{
  static void commit(Graph& g)
  {
    g.commit();
  }

  static void uncommit(Graph& g)
  {
    g.uncommit();
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Helper function to do nothing if a graph is not CSR
//////////////////////////////////////////////////////////////////////
template<typename Graph>
struct try_commit_impl<Graph, false>
{
  static void commit(Graph& g)
  { }

  static void uncommit(Graph& g)
  { }
};

} // detail namespace

//////////////////////////////////////////////////////////////////////
/// @brief Commit a graph if it is a CSR representation. If not, do nothing.
/// @param g The graph to commit
//////////////////////////////////////////////////////////////////////
template<typename Graph>
void try_commit(Graph& g)
{
  detail::try_commit_impl<Graph>::commit(g);
}

//////////////////////////////////////////////////////////////////////
/// @brief Uncommit a graph if it is a CSR representation. If not, do nothing.
/// @param g The graph to uncommit
//////////////////////////////////////////////////////////////////////
template<typename Graph>
void try_uncommit(Graph& g)
{
  detail::try_commit_impl<Graph>::uncommit(g);
}

} // stapl namespace

#endif
