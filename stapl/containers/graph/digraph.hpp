/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_DIGRAPH_HPP
#define STAPL_CONTAINERS_DIGRAPH_HPP

#include <stapl/containers/graph/graph.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Creates a DIRECTED, NONMULTIEDGES graph.
/// @ingroup pgraphSpecial
///
/// Static graphs do not allow addition or deletion of vertices. The number of
/// vertices must be known at construction. Edges may be added/deleted.
/// @tparam VertexP type of property for the vertex. Default is no_property.
/// Must be default assignable, copyable and assignable.
/// @tparam EdgeP type of property for the edge. Default is no_property.
/// Must be default assignable, copyable and assignable.
/// @tparam PS Partition strategy that defines how to partition
/// the original domain into subdomains. The default partition is
/// @ref balanced_partition.
/// @tparam Map Mapper that defines how to map the subdomains produced
/// by the partition to locations. The default mapper is @ref mapper.
/// @tparam Traits A traits class that defines customizable components
/// of graph, such as the domain type, base container type, storage, etc. The
/// default traits class is @ref static_graph_traits.
//////////////////////////////////////////////////////////////////////
template <typename ...OptionalParams>
class digraph
  : public graph<DIRECTED, NONMULTIEDGES, OptionalParams...>
{
  typedef graph<DIRECTED, NONMULTIEDGES, OptionalParams...> base_type;
  typedef typename graph_directedness_container_selector<
    DIRECTED, NONMULTIEDGES, OptionalParams...>::type       directed_base_type;

public:

  /// @name Constructors
  /// @{

  //////////////////////////////////////////////////////////////////////
  /// @copydoc graph::graph(size_t const&, vertex_property const&)
  //////////////////////////////////////////////////////////////////////
  digraph(size_t const& n=0,
          typename base_type::vertex_property const& default_value
            =typename base_type::vertex_property())
    : base_type(n, default_value)
  { }

  //////////////////////////////////////////////////////////////////////
  /// @copydoc graph::graph(partition_type const&, vertex_property const&)
  //////////////////////////////////////////////////////////////////////
  digraph(typename base_type::partition_type const& ps,
          typename base_type::vertex_property const& default_value
            =typename base_type::vertex_property())
    : base_type(ps, default_value)
  { }

  boost::shared_ptr<stapl::digraph<OptionalParams...>> shared_from_this()
  {
    return boost::static_pointer_cast<stapl::digraph<OptionalParams...>>(
             boost::enable_shared_from_this<detail::container_impl<
               directed_base_type>>::shared_from_this());
  }
  /// @}
};

} // stapl namespace


#endif /* STAPL_CONTAINERS_DIGRAPH_HPP */
