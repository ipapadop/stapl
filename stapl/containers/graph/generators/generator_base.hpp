/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_GRAPH_GENERATOR_BASE_HPP
#define STAPL_CONTAINERS_GRAPH_GENERATOR_BASE_HPP

#include <boost/type_traits/is_base_of.hpp>

#include <stapl/utility/random.hpp>
#include <boost/random/uniform_int_distribution.hpp>

namespace stapl {

namespace generators {

namespace detail {

//////////////////////////////////////////////////////////////////////
/// @brief Functor which adds vertices to a generated graph with the default
///   property.
///
/// This is used when the user does not provide a vertex generation functor.
//////////////////////////////////////////////////////////////////////
struct populate_vertices
{
  typedef void result_type;

  //////////////////////////////////////////////////////////////////////
  /// @param x Id of the vertex to add.
  /// @param view View over the graph to which the vertex is added.
  //////////////////////////////////////////////////////////////////////
  template<typename T, typename V>
  void operator()(T x, V& view)
  {
    view.add_vertex(x, typename V::vertex_property());
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Functor used to call the provided vertex addition functor.
/// @tparam WF The vertex addition functor to call.
/// @tparam bool Flag to select correct specialization based on if the target
///   graph is dynamic (true) or not.
//////////////////////////////////////////////////////////////////////
template<typename WF, bool>
struct av_helper
{ };

//////////////////////////////////////////////////////////////////////
/// @brief @copybrief av_helper
/// @tparam WF The vertex addition functor to call.
///
/// This is the specialization called for stapl::dynamic_graph. It adds N
/// vertices to the given graph in parallel using the provided functor.
//////////////////////////////////////////////////////////////////////
template<typename WF>
struct av_helper<WF, true>
{
  WF m_wf;
  size_t m_num_vertices;

  //////////////////////////////////////////////////////////////////////
  /// @param wf The vertex addition functor.
  /// @param num_vertices The number of vertices to add.
  //////////////////////////////////////////////////////////////////////
  av_helper(WF const& wf, size_t num_vertices)
    : m_wf(wf), m_num_vertices(num_vertices)
  { }

  template<typename G>
  void operator()(G& g)
  {
    map_func(m_wf, counting_view<size_t>(m_num_vertices), make_repeat_view(g));
  }

  void define_type(typer& t)
  {
    t.member(m_wf);
    t.member(m_num_vertices);
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief @copybrief av_helper
/// @tparam WF The vertex addition functor to call.
///
/// This is the specialization called when the input is not a
/// stapl::dynamic_graph. It performs a no-op.
//////////////////////////////////////////////////////////////////////
template<typename WF>
struct av_helper<WF, false>
{
  av_helper(WF const&, size_t)
  { }

  template<typename G>
  void operator()(G& g)
  { }
};


//////////////////////////////////////////////////////////////////////
/// @brief Type-traits metafunction used to determine if a given graph is
///   a stapl::dynamic_graph.
/// @tparam Graph The type of the graph in question.
//////////////////////////////////////////////////////////////////////
template<typename Graph>
struct is_dynamic
{
  static const bool value = false;
};

//////////////////////////////////////////////////////////////////////
/// @brief Function which adds vertices to the graph using the correct helper
///   class.
/// @param g View over the graph to which the vertices are added.
/// @param wf Functor which is used to add the vertices.
/// @param n The number of vertices to add.
///
/// @see av_helper
//////////////////////////////////////////////////////////////////////
template<typename GraphView, typename WF>
void add_verts_helper(GraphView& g, WF wf, size_t n)
{
  typedef typename GraphView::view_container_type graph_cont_t;
  av_helper<WF, is_dynamic<graph_cont_t>::value>(wf, n)(g);
}


} // namespace detail


//////////////////////////////////////////////////////////////////////
/// @brief Metafunction which extracts the type of the generated graph from
///   a graph generator type.
//////////////////////////////////////////////////////////////////////
template<typename T>
struct extract_graph_type;

//////////////////////////////////////////////////////////////////////
/// @brief @copybrief extract_graph_type
/// @tparam Gen Type of the generator.
/// @tparam G Type of the graph.
///
/// Specialization for generators which only accept G as a parameter.
//////////////////////////////////////////////////////////////////////
template<template<typename> class Gen, typename G>
struct extract_graph_type<Gen<G> >
{
  typedef G type;
};

//////////////////////////////////////////////////////////////////////
/// @brief @copybrief extract_graph_type
/// @tparam Gen Type of the generator.
/// @tparam G Type of the graph.
/// @tparam EF Type of the edge addition functor.
/// @tparam VF Type of the vertex addition functor.
///
/// Specialization for generators which accept G, EF, and VF as parameters.
//////////////////////////////////////////////////////////////////////
template<template<typename, typename, typename> class Gen,
         typename G, typename EF, typename VF>
struct extract_graph_type<Gen<G, EF, VF> >
{
  typedef G type;
};


//////////////////////////////////////////////////////////////////////
/// @brief Base class for all graph generators.
/// @tparam Derived Type of the derived class, used to implement static
///   polymorphism via the CRTP idiom.
//////////////////////////////////////////////////////////////////////
template<typename Derived>
class generator_base
{
  typedef typename extract_graph_type<Derived>::type graph_type;
  typedef typename graph_type::view_container_type   graph_cont_type;
  typedef Derived                                    derived_type;

  graph_type m_graph;
  size_t     m_num_vertices;

  //////////////////////////////////////////////////////////////////////
  /// @brief Cast this object to the derived type.
  /// @return Reference to this class interpreted as the derived type.
  //////////////////////////////////////////////////////////////////////
  derived_type& derived()
  {
    return static_cast<derived_type&>(*this);
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Cast this object to the derived type.
  /// @return Reference to this class interpreted as the derived type.
  //////////////////////////////////////////////////////////////////////
  derived_type const& derived() const
  {
    return static_cast<derived_type const&>(*this);
  }

protected:
  graph_type& graph()
  {
    return m_graph;
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Function which is called to add vertices to the graph using the
  ///   default functor.
  //////////////////////////////////////////////////////////////////////
  void add_vertices()
  {
    detail::add_verts_helper(this->graph(), detail::populate_vertices(),
                             m_num_vertices);
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Function which is called to add vertices to the graph using the
  ///   provided vertex addition functor.
  /// @param vf Functor which is used to add vertices.
  //////////////////////////////////////////////////////////////////////
  template<typename VF>
  void add_vertices(VF const& vf)
  {
    detail::add_verts_helper(this->graph(), vf, m_num_vertices);
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Function which is called to add edges to the graph using the
  ///   provided edge addition functor.
  /// @param ef Functor which is used to add edges.
  //////////////////////////////////////////////////////////////////////
  template<typename EF>
  void add_edges(EF const& ef)
  {
    map_func(ef, this->graph(), make_repeat_view(this->graph()));
  }

public:
  // if no graph is provided, we generate default vertices from [0, N)
  // with default properties.
  //////////////////////////////////////////////////////////////////////
  /// @brief Constructs a generator, as well as a new graph of size n.
  /// @param n Size of the new graph.
  //////////////////////////////////////////////////////////////////////
  generator_base(size_t n)
    : m_graph(new graph_cont_type(n)),
      m_num_vertices(n)
  { }

  //////////////////////////////////////////////////////////////////////
  /// @param g View over the generated graph.
  /// @param n Size of the graph.
  //////////////////////////////////////////////////////////////////////
  generator_base(graph_type& g, size_t n)
    : m_graph(g),
      m_num_vertices(n)
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief Calls @ref add_vertices and @ref add_edges on the derived class.
  /// @return A view over the generated graph.
  //////////////////////////////////////////////////////////////////////
  graph_type operator()()
  {
    size_t graph_sz = m_graph.size();
    rmi_fence();  // Needed because size is non-collective.
    if (graph_sz == 0) {
      derived().add_vertices();
      rmi_fence();  // Needed because of comm. generated during vertex-addition.
    }

    stapl_assert(m_graph.size() == m_num_vertices,
                 "Graph does not have required number of vertices");

    if (m_graph.version() != m_graph.container().version())
      m_graph = graph_type(this->graph().container());
    derived().add_edges();
    rmi_fence();    // Needed because of comm. generated during edge-addition.
    return this->graph();
  }

  void define_type(typer& t)
  {
    t.member(m_graph);
    t.member(m_num_vertices);
  }

}; // class generator_base


//////////////////////////////////////////////////////////////////////
/// @brief Class for generating random numbers.
///
/// Generators that need to produce random numbers in a thread-safe
/// way should inherit from this class and call this->rand(...) to
/// get random numbers.
//////////////////////////////////////////////////////////////////////
struct rand_gen
{
  boost::random::mt19937 m_rng;
  typedef boost::random::uniform_int_distribution<size_t> rng_dist_t;

  //////////////////////////////////////////////////////////////////////
  /// @param seed The seed for random-number generation.
  //////////////////////////////////////////////////////////////////////
  rand_gen(unsigned int seed = get_location_id())
    : m_rng(seed)
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief Generates a random number in the range
  /// [0, numeric_limits<size_t>::max()).
  //////////////////////////////////////////////////////////////////////
  size_t rand(void)
  { return rng_dist_t()(m_rng); }

  //////////////////////////////////////////////////////////////////////
  /// @brief Generates a random number in the range
  /// [0, max).
  /// @param max The maximum value of the output random number (exclusive).
  //////////////////////////////////////////////////////////////////////
  size_t rand(size_t max)
  { return rng_dist_t(0, max-1)(m_rng); }

  //////////////////////////////////////////////////////////////////////
  /// @brief Generates a random number in the range
  /// [min, max).
  /// @param min The minimum value of the output random number.
  /// @param max The maximum value of the output random number (exclusive).
  //////////////////////////////////////////////////////////////////////
  size_t rand(size_t min, size_t max)
  { return rng_dist_t(min, max-1)(m_rng); }

  void define_type(typer& t)
  { t.member(m_rng); }
};

} // namespace generators

} // namespace stapl

#endif
