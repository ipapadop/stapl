/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_GRAPH_GENERATORS_GRID_HPP
#define STAPL_CONTAINERS_GRAPH_GENERATORS_GRID_HPP

#include <stapl/containers/graph/generators/generator.hpp>
#include <stapl/views/metadata/projection/geometry.hpp>

namespace stapl {

namespace generators {

namespace detail {

//////////////////////////////////////////////////////////////////////
/// @brief Functor which adds edges to form a grid of the given dimensions.
/// @see make_grid
//////////////////////////////////////////////////////////////////////
template<int D>
struct grid_neighbors
{
  geometry_impl::grid_generator<D> m_generator;
  bool                             m_bidirectional;

  typedef void result_type;

  //////////////////////////////////////////////////////////////////////
  /// @param x,y Dimensions of the grid.
  /// @param bidirectional True to add back-edges in a directed graph.
  //////////////////////////////////////////////////////////////////////
  grid_neighbors(std::array<std::size_t, D> dims, bool bidirectional)
    : m_generator(dims), m_bidirectional(bidirectional)
  { }

  template<typename Vertex, typename Graph>
  void operator()(Vertex v, Graph& view)
  {
    typedef typename Vertex::vertex_descriptor descriptor_type;

    m_generator.edges_for(v.descriptor(),
      [&](descriptor_type const& s, descriptor_type const& t) {

      view.add_edge_async(s, t);

       if (m_bidirectional && view.is_directed())
        view.add_edge_async(t, s);
    });
  }

  void define_type(typer& t)
  {
    t.member(m_generator);
    t.member(m_bidirectional);
  }
};

}


//////////////////////////////////////////////////////////////////////
/// @brief Generates a multidimensional grid graph.
///
/// The generated graph will be a hyperrectangular grid in n-dimensions.
/// In 2D, it will create a 2D mesh. In 3D, it will create a cube and
/// so on.
///
/// This function mutates the input graph.
///
/// @param g A view over the graph to generate.
/// @param dims An array of sizes in each dimension
/// @param bidirectional True to add back-edges in a directed graph, false
///   for forward edges only.
/// @return The original view, now containing the generated graph.
//////////////////////////////////////////////////////////////////////
template <typename GraphView, unsigned long D>
GraphView make_grid(GraphView& g, std::array<std::size_t, D> dims,
                    bool bidirectional=true)
{
  typedef detail::grid_neighbors<D> ef_t;
  const std::size_t size =
    std::accumulate(
      dims.begin(), dims.end(), 1, std::multiplies<std::size_t>()
    );

  return make_generator<GraphView, ef_t>(g, size,
                                         ef_t(dims, bidirectional))();
}

//////////////////////////////////////////////////////////////////////
/// @brief Generates a multidimensional grid graph.
///
/// The generated graph will be a hyperrectangular grid in n-dimensions.
/// In 2D, it will create a 2D mesh. In 3D, it will create a cube and
/// so on.
/// The returned view owns its underlying container.
///
/// @param dims An array of sizes in each dimension
/// @param bidirectional True to add back-edges in a directed graph, false
///   for forward edges only.
/// @return A view over the generated graph.
///
/// @b Example
/// @snippet grid.cc Example
//////////////////////////////////////////////////////////////////////
template <typename GraphView, unsigned long D>
GraphView make_grid(std::array<std::size_t, D> dims, bool bidirectional=true)
{
  typedef detail::grid_neighbors<D> ef_t;
  const std::size_t size =
    std::accumulate(
      dims.begin(), dims.end(), 1, std::multiplies<std::size_t>()
    );

  return make_generator<GraphView, ef_t>(size, ef_t(dims, bidirectional))();
}


} // namespace generators

} // namespace stapl

#endif
