/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_GRAPH_GENERATORS_MESH_HPP
#define STAPL_CONTAINERS_GRAPH_GENERATORS_MESH_HPP

#include <stapl/containers/graph/generators/generator.hpp>

namespace stapl {

namespace generators {

namespace detail {

//////////////////////////////////////////////////////////////////////
/// @brief Functor which adds edges to form a mesh of the given dimensions.
/// @see make_mesh
//////////////////////////////////////////////////////////////////////
struct mesh_neighbors
{
  size_t m_x, m_y;
  bool   m_bidirectional;

  typedef void result_type;

  //////////////////////////////////////////////////////////////////////
  /// @param x,y Dimensions of the mesh.
  /// @param bidirectional True to add back-edges in a directed graph.
  //////////////////////////////////////////////////////////////////////
  mesh_neighbors(size_t x, size_t y, bool bidirectional)
    : m_x(x), m_y(y), m_bidirectional(bidirectional)
  { }

  template<typename Vertex, typename Graph>
  void operator()(Vertex v, Graph& view)
  {
    size_t row_st = (v.descriptor()/m_x)*m_x;
    size_t x_neighbour = ((v.descriptor()-(row_st)+1) + row_st);
    size_t y_neighbour = (v.descriptor()+m_x);

    if (x_neighbour < row_st+m_x)
      view.add_edge_async(v.descriptor(), x_neighbour);
    if (y_neighbour < m_x*m_y)
      view.add_edge_async(v.descriptor(), y_neighbour);

    if (view.is_directed() && m_bidirectional) {
      if (x_neighbour < row_st+m_x)
        view.add_edge_async(x_neighbour, v.descriptor());
      if (y_neighbour < m_x*m_y)
        view.add_edge_async(y_neighbour, v.descriptor());
    }
  }

  void define_type(typer& t)
  {
    t.member(m_x);
    t.member(m_y);
    t.member(m_bidirectional);
  }
};

}


//////////////////////////////////////////////////////////////////////
/// @brief Generates an x-by-y mesh.
///
/// The generated mesh will contain x vertices in the horizontal direction and
/// y vertices in the vertical direction. This is distributed n/p in the
/// y-direction.
///
/// This function mutates the input graph.
///
/// @param g A view over the graph to generate.
/// @param nx Size of the x-dimension of the mesh.
/// @param ny Size of the y-dimension of the mesh.
/// @param bidirectional True to add back-edges in a directed graph, false
///   for forward edges only.
/// @return The original view, now containing the generated graph.
//////////////////////////////////////////////////////////////////////
template <typename GraphView>
GraphView make_mesh(GraphView& g, size_t nx, size_t ny,
                    bool bidirectional=true)
{
  typedef typename detail::mesh_neighbors ef_t;
  return make_generator<GraphView, ef_t>(g, nx*ny,
                                         ef_t(nx, ny, bidirectional))();
}

//////////////////////////////////////////////////////////////////////
/// @brief Generates an x-by-y mesh.
///
/// The generated mesh will contain x vertices in the horizontal direction and
/// y vertices in the vertical direction. This is distributed n/p in the
/// y-direction.
///
/// The returned view owns its underlying container.
/// @param nx Size of the x-dimension of the mesh.
/// @param ny Size of the y-dimension of the mesh.
/// @param bidirectional True to add back-edges in a directed graph, false
///   for forward edges only.
/// @return A view over the generated graph.
///
/// @b Example
/// @snippet mesh.cc Example
//////////////////////////////////////////////////////////////////////
template <typename GraphView>
GraphView make_mesh(size_t nx, size_t ny, bool bidirectional=true)
{
  typedef typename detail::mesh_neighbors ef_t;
  return make_generator<GraphView, ef_t>(nx*ny, ef_t(nx, ny, bidirectional))();
}


} // namespace generators

} // namespace stapl

#endif
