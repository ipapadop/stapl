/*
// Copyright (c) 2000-2009, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// The information and source code contained herein is the exclusive
// property of TEES and may not be disclosed, examined or reproduced
// in whole or in part without explicit written authorization from TEES.
*/

#ifndef STAPL_CONTAINERS_SEQUENTIAL_BITSET_HPP
#define STAPL_CONTAINERS_SEQUENTIAL_BITSET_HPP

#include <algorithm>

namespace stapl {

namespace sequential {

class bitset;

namespace detail {

//////////////////////////////////////////////////////////////////////
/// @brief Iterator over only the active indices that are set to 1 in
/// a @ref bitset.
//////////////////////////////////////////////////////////////////////
class const_bitset_set_index_iterator
  : public boost::iterator_facade<
           const_bitset_set_index_iterator,
           std::size_t,
           boost::forward_traversal_tag,
           std::size_t>
{
private:
  friend class boost::iterator_core_access;

  std::uint64_t const* m_words;
  std::size_t    m_num_words;
  std::uint64_t m_current_word;
  std::size_t    m_current_set_index;
  std::size_t    m_current_word_index;

public:
  const_bitset_set_index_iterator(std::uint64_t const *words,
                                  std::size_t num_words, std::size_t index)
    : m_words(words), m_num_words(num_words), m_current_word(0),
      m_current_set_index(std::numeric_limits<std::size_t>::max()),
      m_current_word_index(index / 64)
  {
    if (m_current_word_index < m_num_words) {
      m_current_word = this->m_words[m_current_word_index];

      // Unsetting low bits of word to advance to index within the word
      const std::size_t mask = std::numeric_limits<std::size_t>::max()
        << (index % 64);
      m_current_word &= mask;


      this->increment();
    }
  }

  const_bitset_set_index_iterator()
    : m_words(nullptr), m_num_words(0), m_current_word(0),
      m_current_set_index(std::numeric_limits<std::size_t>::max()),
      m_current_word_index(std::numeric_limits<std::size_t>::max())
  { }

private:
  std::size_t dereference(void) const
  {
    return m_current_set_index;
  }

  bool equal(const_bitset_set_index_iterator const& other) const noexcept
  {
    return m_current_set_index == other.m_current_set_index;
  }

  void increment(void) noexcept
  {
    while (m_current_word == 0) {
      // We've reached the end
      if (m_current_word_index == m_num_words-1) {
        m_current_set_index = std::numeric_limits<std::size_t>::max();
        return;
      }

      m_current_word = this->m_words[++m_current_word_index];
    }

    // Now the current word has at least one bit set
    std::uint64_t t = m_current_word & -m_current_word;
    int r = __builtin_ctzl(m_current_word);
    m_current_set_index = m_current_word_index * 64 + r;
    m_current_word ^= t;
  }
};

} // namespace detail

//////////////////////////////////////////////////////////////////////
/// @brief A sequential compressed set of bits, where each bit can
/// be individually accessed.
//////////////////////////////////////////////////////////////////////
class bitset
{
  using word_type = std::uint64_t;
  static constexpr std::size_t bits_per_word = 64;

  std::size_t m_num_bits;
  std::size_t m_num_words;
  std::unique_ptr<word_type[]> m_words;
  bool m_empty{true};

public:
  using const_iterator = detail::const_bitset_set_index_iterator;

  //////////////////////////////////////////////////////////////////////
  /// @brief Create the bitset with @p n elements.
  /// @param The number of elements (not bits) in the set
  //////////////////////////////////////////////////////////////////////
  bitset(std::size_t n)
    : m_num_bits(n)
    , m_num_words((n + bits_per_word - 1) / bits_per_word)
    , m_words(new word_type[m_num_words])
    , m_empty(true)
  {
    std::fill(m_words.get(), m_words.get() + m_num_words, 0);
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Retrive the value of bit @p i
  /// @param The index of the bit to retrieve
  //////////////////////////////////////////////////////////////////////
  bool get(std::size_t i) const
  {
    const std::size_t word_index = i / bits_per_word;
    const std::size_t bit_index = i & (bits_per_word-1);

    return m_words[word_index] >> bit_index & 1ul;
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Set the value of bit at index @p i
  /// @param The index of the bit to set
  //////////////////////////////////////////////////////////////////////
  __attribute__((always_inline)) inline void set(std::size_t i)
  {
    const std::size_t word_index = i / bits_per_word;
    const std::size_t bit_index = i & (bits_per_word-1);

    m_words[word_index] |= 1ul << bit_index;
    m_empty = false;
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Begin iterator of only the set bits in the set
  //////////////////////////////////////////////////////////////////////
  const_iterator begin() const
  {
    return const_iterator{m_words.get(), m_num_words, 0};
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief End iterator of only the set bits in the set
  //////////////////////////////////////////////////////////////////////
  const_iterator end() const
  {
    return const_iterator{m_words.get(), m_num_words, this->size()};
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief The total number of elemnts in the set
  //////////////////////////////////////////////////////////////////////
  std::size_t size() const
  {
    return m_num_bits;
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Swap the storage of another bitset
  //////////////////////////////////////////////////////////////////////
  void swap(bitset& other)
  {
    std::swap(this->m_num_bits, other.m_num_bits);
    std::swap(this->m_num_words, other.m_num_words);
    std::swap(this->m_empty, other.m_empty);
    this->m_words.swap(other.m_words);
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Whether or not any bits are set
  //////////////////////////////////////////////////////////////////////
  bool empty() const
  {
    return m_empty;
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Unset all of the bits
  //////////////////////////////////////////////////////////////////////
  void clear(void)
  {
    std::fill(m_words.get(), m_words.get() + m_num_words, 0);
  }
};

} // namespace sequential

} // namespace stapl

#endif
