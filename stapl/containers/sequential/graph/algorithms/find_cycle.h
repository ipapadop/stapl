/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_SEQUENTIAL_GRAPH_FIND_CYCLE_HPP
#define STAPL_CONTAINERS_SEQUENTIAL_GRAPH_FIND_CYCLE_HPP

#include <deque>
#include "depth_first_search.h"
#include "breadth_first_search.h"

namespace stapl{
namespace sequential{

//////////////////////////////////////////////////////////////////////
/// @brief Checks for cycles in a directed graph. It does so
/// by returning EARLY_QUIT if a visitor is visiting a node that has
/// already been visited.
/// @todo Move to a detail namespace to clean up stapl::sequential
/// @ingroup seqGraphAlgoWf
//////////////////////////////////////////////////////////////////////
template <class GRAPH>
class visitor_cycle
 : public visitor_base<GRAPH>
{
  //////////////////////////////////////////////////////////////////////
  /// @brief The visitor class function for a gray target, which will
  /// return EARLY_QUIT.
  //////////////////////////////////////////////////////////////////////
  public:
  visitor_return gray_target (typename GRAPH::vertex_iterator ,
                              typename GRAPH::adj_edge_iterator )
  {
    return EARLY_QUIT;
  }
  /*
  visitor_return black_target (typename GRAPH::vertex_iterator ,
                               typename GRAPH::adj_edge_iterator ){
    return EARLY_QUIT;
  }
  */
};

//////////////////////////////////////////////////////////////////////
/// @brief Checks for cycles in a directed graph by running a
/// Depth-First Search with the visitor_cycle class as the visitor class.
/// @param _g The input graph.
/// @param _color_map A colormap for the depth first search, which maps
/// each vertex in the graph to a color.
/// @ingroup seqGraphAlgo
//////////////////////////////////////////////////////////////////////
template<class Graph, class ColorMap>
bool is_cycle (Graph& _g, ColorMap& _color_map)
{
  visitor_cycle<Graph> vis;
  visitor_return res = depth_first_search_early_quit(_g, vis, _color_map);
  if (res == EARLY_QUIT) return true;
  else return false;
}

//////////////////////////////////////////////////////////////////////
/// @brief Finds the back edges in a graph.
/// @tparam GRAPH The type of input graph.
/// @tparam Container The type of container to hold the back edges.
/// @todo Move to a detail namespace to clean up stapl::sequential
/// @ingroup seqGraphAlgoWf
//////////////////////////////////////////////////////////////////////
 template <class GRAPH, class Container>
class visitor_back_edges
 : public visitor_base<GRAPH>
{
  Container& m_back_edges;
  public:

  visitor_back_edges(Container& _v)
   : m_back_edges(_v){};

  //////////////////////////////////////////////////////////////////////
  /// @brief Used for a black target, which pushes
  /// a back edge into the container.
  /// @param vi The vertex iterator.
  /// @param ei The edge iterator.
  //////////////////////////////////////////////////////////////////////
  visitor_return black_target (typename GRAPH::vertex_iterator vi,
                                      typename GRAPH::adj_edge_iterator ei)
  {
    typedef typename GRAPH::vertex_descriptor VD;
    m_back_edges.push_back(std::pair<VD,VD>((*ei).target(),(*vi).descriptor()));
    return CONTINUE;
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Uses a Breadth-First Search with the
/// visitor_back_edges visitor class to find all of the back edges in
/// and input graph. Return is the number of back edges.
/// @param _g The input graph.
/// @param _color_map The color map to be used, which maps each vertex in
/// the graph to a color.
/// @param _V A container to hold the back edges of the graph.
/// @ingroup seqGraphAlgo
//////////////////////////////////////////////////////////////////////
template<class Graph, class ColorMap, class Container>
size_t get_back_edges(Graph& _g, ColorMap& _color_map, Container& _V)
{
   visitor_back_edges<Graph,Container> vis(_V);
   breadth_first_search(_g , vis, _color_map);
   return _V.size();
}

}//namespace sequential
}//namespace stapl

#endif
