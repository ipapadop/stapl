/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_CONTAINER_WRAPPER_REF_HPP
#define STAPL_CONTAINERS_CONTAINER_WRAPPER_REF_HPP

#include <memory>
#include <boost/type_traits/integral_constant.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief A wrapper for storing containers as the value_type of another
/// container. This is needed because we want to store a reference to the
/// container as a p_object in a base container.
///
/// This class is similar in spirit to boost::ref.
///
/// @tparam T The container to wrap.
//////////////////////////////////////////////////////////////////////
template<typename T>
class container_wrapper_ref
{
private:
  rmi_handle::reference  m_handle_ref;
  /// A pointer to the wrapped value
  T* m_ptr;

public:
  /// The type that this wrapper is holding
  typedef T type;

  //////////////////////////////////////////////////////////////////////
  /// @brief Create an invalid reference, setting the pointer to NULL
  //////////////////////////////////////////////////////////////////////
  container_wrapper_ref(void)
    : m_ptr(nullptr)
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief Initialize the reference with the value to wrap.
  /// @param t The value to wrap
  //////////////////////////////////////////////////////////////////////
  explicit
  container_wrapper_ref(T const& t)
    : m_handle_ref(const_cast<T&>(t).get_rmi_handle()),
      m_ptr(std::addressof(const_cast<T&>(t)))
  { }

  container_wrapper_ref& operator=(T const& t)
  {
    m_handle_ref = const_cast<T&>(t).get_rmi_handle();
    m_ptr = std::addressof(const_cast<T&>(t));
    return *this;
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Cast the value of the wrapper to its original reference type.
  //////////////////////////////////////////////////////////////////////
  operator T const&(void) const
  {
    return get();
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Cast the value of the wrapper to its original reference type.
  //////////////////////////////////////////////////////////////////////
  operator T&(void)
  {
    return get();
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Retrieve the original reference from the wrapper.
  //////////////////////////////////////////////////////////////////////
  T& get(void)
  {
    stapl_assert(m_ptr, "Wrapper not initialized");
    return *m_ptr;
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Retrieve the original reference as a const reference from the
  /// wrapper.
  //////////////////////////////////////////////////////////////////////
  T const& get(void) const
  {
    stapl_assert(m_ptr, "Wrapper not initialized");
    return *m_ptr;
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Retrieve the pointer to the wrapped value.
  //////////////////////////////////////////////////////////////////////
  T* get_pointer(void) const
  {
    stapl_assert(m_ptr, "Wrapper not initialized");
    return m_ptr;
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Serialization of the reference
  //////////////////////////////////////////////////////////////////////
  void define_type(typer& t)
  {
    t.member(m_handle_ref);
    t.transient(m_ptr, resolve_handle<T>(m_handle_ref));
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Retrieve the pointer from a wrapped reference.
/// @param t The reference
/// @return The pointer that is within the reference
//////////////////////////////////////////////////////////////////////
template <typename T>
T* get_pointer(container_wrapper_ref<T> const& t)
{
  return t.get_pointer();
}


//////////////////////////////////////////////////////////////////////
/// @brief Unwraps @p t from @ref container_wrapper_ref.
//////////////////////////////////////////////////////////////////////
template<typename T>
T& unwrap_container_wrapper(T& t)
{
  return t;
}


//////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref unwrap_container_wrapper for
///        @ref container_wrapper_ref.
//////////////////////////////////////////////////////////////////////
template<typename T>
T& unwrap_container_wrapper(container_wrapper_ref<T> t)
{
  return t.get();
}



//////////////////////////////////////////////////////////////////////
/// @brief Metafunction to determine if a given type is a
/// @ref container_wrapper_ref.
///
/// @tparam T The type in question
//////////////////////////////////////////////////////////////////////
template<typename T>
struct is_container_wrapper_ref
  : boost::false_type
{ };

template<typename C>
struct is_container_wrapper_ref<container_wrapper_ref<C> >
  : boost::true_type
{ };

} // namespace stapl

#endif // STAPL_CONTAINERS_CONTAINER_WRAPPER_REF_HPP
