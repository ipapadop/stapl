/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_CONCURRENCY_BOOST_THREAD_LOCAL_STORAGE_HPP
#define STAPL_RUNTIME_CONCURRENCY_BOOST_THREAD_LOCAL_STORAGE_HPP

#include <boost/thread/tss.hpp>

namespace stapl {

namespace runtime {

namespace boost_impl {

//////////////////////////////////////////////////////////////////////
/// @brief Implements thread-local storage using @c boost::thread_specific_ptr.
///
/// @warning This is often a slower performing thread-local storage than
///          @c thread_local, but is required for compilers and platforms that
///          do not support the latter.
///
/// @ingroup concurrency
//////////////////////////////////////////////////////////////////////
template<typename T>
class thread_local_storage
{
private:
  boost::thread_specific_ptr<T> m_t;

public:
  thread_local_storage(void) = default;

  thread_local_storage(thread_local_storage const&) = delete;
  thread_local_storage& operator=(thread_local_storage const&) = delete;

  ~thread_local_storage(void)
  { m_t.reset(); }

  T& get(void)
  {
    T* p = m_t.get();
    if (!p) {
      p = new T();
      m_t.reset(p);
    }
    return *p;
  }
};

} // namespace boost_impl

} // namespace runtime

} // namespace stapl


//////////////////////////////////////////////////////////////////////
/// @brief Thread-local storage variable definition helper macro for
///        @ref stapl::runtime::boost_impl::thread_local_storage.
///
/// @ingroup concurrency
//////////////////////////////////////////////////////////////////////
#define STAPL_RUNTIME_THREAD_LOCAL(t, x) \
 stapl::runtime::boost_impl::thread_local_storage<t> x;

#endif
