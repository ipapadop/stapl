/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_CONCURRENCY_CONFIG_HPP
#define STAPL_RUNTIME_CONCURRENCY_CONFIG_HPP

#include "../config/find_user_config.hpp"
#include "../config/platform.hpp"
#include "../config/compiler.hpp"
#include "../config/suffix.hpp"

#if defined(STAPL_RUNTIME_USE_THREAD)
// native threads backend is used
# if defined(STAPL_RUNTIME_THREADING_BACKEND_DEFINED)
#  error "Multiple multi-threading backends defined."
# endif
# define STAPL_RUNTIME_THREADING_BACKEND_DEFINED 1
#endif

#if defined(STAPL_RUNTIME_USE_TBB)
// Intel Threading Building Blocks backend is used
# if defined(STAPL_RUNTIME_THREADING_BACKEND_DEFINED)
#  error "Multiple multi-threading backends defined."
# endif
# include <tbb/tbb_stddef.h>
# if TBB_INTERFACE_VERSION<4002
#  error "Intel TBB version is too old. Please use TBB 3.0 or newer."
# endif
# define STAPL_RUNTIME_THREADING_BACKEND_DEFINED 1
#endif

#if defined(STAPL_RUNTIME_USE_OMP)
// OpenMP backend is used
# if defined(STAPL_RUNTIME_THREADING_BACKEND_DEFINED)
#  error "Multiple multi-threading backends defined."
# endif
# if _OPENMP<200805
#  error "OpenMP version is too old. Please use OpenMP 3.0 or newer."
# endif
# define STAPL_RUNTIME_THREADING_BACKEND_DEFINED 1
#endif


#if !defined(STAPL_RUNTIME_THREADING_BACKEND_DEFINED)
// multithreaded backend not defined, default to C++11 threading support
# define STAPL_RUNTIME_USE_THREAD
# define STAPL_RUNTIME_THREADING_BACKEND_DEFINED 1
#endif

#if !defined(STAPL_RUNTIME_TBB_AVAILABLE) && defined(BOOST_INTEL)
// Intel Threading Building Blocks is available, use its containers
# define STAPL_RUNTIME_TBB_AVAILABLE
#endif

#endif
