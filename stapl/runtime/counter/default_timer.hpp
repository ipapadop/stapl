/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_COUNTER_DEFAULT_TIMER_HPP
#define STAPL_RUNTIME_COUNTER_DEFAULT_TIMER_HPP

#include "config.hpp"

#if STAPL_USE_TIMER==STAPL_PAPI_TIMER
# include "papi/papi_timer.hpp"

namespace stapl {

typedef papi_timer default_timer;

} // namespace stapl

#elif STAPL_USE_TIMER==STAPL_MPI_WTIME_TIMER
# include "mpi/mpi_wtime_timer.hpp"

namespace stapl {

typedef mpi_wtime_timer default_timer;

} // namespace stapl

#elif STAPL_USE_TIMER==STAPL_CLOCK_GETTIME_TIMER
# include "posix/clock_gettime_timer.hpp"

namespace stapl {

typedef clock_gettime_timer default_timer;

} // namespace stapl

#elif STAPL_USE_TIMER==STAPL_GETTIMEOFDAY_TIMER
# include "posix/gettimeofday_timer.hpp"

namespace stapl {

typedef gettimeofday_timer default_timer;

} // namespace stapl

#elif STAPL_USE_TIMER==STAPL_GETRUSAGE_TIMER
# include "posix/getrusage_timer.hpp"

namespace stapl {

typedef getrusage_timer default_timer;

} // namespace stapl

#elif STAPL_USE_TIMER==STAPL_GETTICKCOUNT_TIMER
# include "windows/gettickcount_timer.hpp"

namespace stapl {

typedef gettickcount_timer default_timer;

} // namespace stapl

#endif


#endif
