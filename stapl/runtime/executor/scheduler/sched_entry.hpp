/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_RUNTIME_EXECUTOR_SCHEDULER_SCHED_ENTRY_HPP
#define STAPL_RUNTIME_EXECUTOR_SCHEDULER_SCHED_ENTRY_HPP

#include <utility>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Base scheduler entry class for entries that can know their scheduling
///        information.
///
/// @tparam SchedInfo Scheduling information object type.
/// @tparam Hook      Hook for storing the entry in an intrusive container.
///
/// For example, tasks created by a @ref paragraph can have scheduling
/// information, whereas @ref gang_executor objects cannot.
///
/// @see executor
/// @ingroup scheduling
//////////////////////////////////////////////////////////////////////
template<typename SchedInfo, typename Hook>
class sched_entry
  : public Hook,
    private SchedInfo
{
public:
  typedef SchedInfo sched_info_type;

  //////////////////////////////////////////////////////////////////////
  /// @brief Creates a new @ref sched_entry.
  ///
  /// @param sched_info Scheduling information.
  //////////////////////////////////////////////////////////////////////
  explicit sched_entry(sched_info_type sched_info)
    : SchedInfo(std::move(sched_info))
  { }

  sched_entry(sched_entry const&) = delete;
  sched_entry& operator=(sched_entry const&) = delete;

protected:
  ~sched_entry(void) = default;

public:
  //////////////////////////////////////////////////////////////////////
  /// @brief Returns the scheduling information associated with this entry.
  //////////////////////////////////////////////////////////////////////
  sched_info_type const& sched_info(void) const noexcept
  { return *this; }

  //////////////////////////////////////////////////////////////////////
  /// @copydoc sched_info(void) const
  //////////////////////////////////////////////////////////////////////
  sched_info_type& sched_info(void) noexcept
  { return *this; }
};

} // namespace stapl

#endif
