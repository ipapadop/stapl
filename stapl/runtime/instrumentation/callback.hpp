/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_INSTRUMENTATION_CALLBACK_HPP
#define STAPL_RUNTIME_INSTRUMENTATION_CALLBACK_HPP

#include "../exception.hpp"
#include "../this_context.hpp"
#include <algorithm>
#include <functional>
#include <list>
#include <mutex>
#include <unordered_map>
#include <utility>

namespace stapl {

////////////////////////////////////////////////////////////////////
/// @brief Registers the given function as a callback to be called when a
///        primitive is called in the registering context.
///
/// The registered functions have to have the following signature
/// @code
/// void f(const char* s);
/// @endcode
/// where @p s is the name of the primitive that called it.
///
/// @ingroup instrumentation
////////////////////////////////////////////////////////////////////
class callback
{
public:
  typedef runtime::full_location                  key_type;
private:
  typedef std::function<void(const char*)>        function_type;
  typedef std::list<function_type>                list_type;
  typedef std::unordered_map<key_type, list_type> container_type;

  ////////////////////////////////////////////////////////////////////
  /// @brief Returns the container of registered objects and the container's
  ///        mutex.
  ////////////////////////////////////////////////////////////////////
  static std::pair<container_type, std::mutex>& get_container(void)
  {
    static std::pair<container_type, std::mutex> map;
    return map;
  }

  const key_type      m_key;
  list_type::iterator m_it;

public:
  ////////////////////////////////////////////////////////////////////
  /// @brief Call all registered callback functions for the given context id
  ///        and passes the given string.
  ////////////////////////////////////////////////////////////////////
  static void call(runtime::context_id const& id, const char* s)
  {
    const key_type key = id.current;
    auto& r            = get_container();
    auto& c            = r.first;

    std::lock_guard<std::mutex> lock{r.second};
    auto it = c.find(key);
    if (it!=c.end()) {
      for (function_type& f : it->second)
        f(s);
    }
  }

  ////////////////////////////////////////////////////////////////////
  /// @brief Creates a new @ref callback object that is registered in the
  ///        current context.
  ////////////////////////////////////////////////////////////////////
  template<typename Function>
  explicit callback(Function&& f)
  : m_key(runtime::this_context::get_id().current)
  {
    auto& r = get_container();
    auto& c = r.first;

    std::lock_guard<std::mutex> lock{r.second};
    auto& l = c[m_key];
    m_it = l.emplace_front(std::forward<Function>(f));
  }

  callback(callback const&) = delete;
  callback& operator=(callback const&) = delete;

  ~callback(void)
  {
    auto& r = get_container();
    auto& c = r.first;

    std::lock_guard<std::mutex> lock{r.second};
    auto it = c.find(m_key);
    STAPL_RUNTIME_ASSERT(it!=c.end());
    auto& l = it->second;
    l.erase(m_it);
    if (l.empty())
      c.erase(it);
  }
};


////////////////////////////////////////////////////////////////////
/// @brief Calls the registered @ref stapl::callback objects in the current
///        context with the given arguments.
///
/// @ingroup instrumentation
////////////////////////////////////////////////////////////////////
#define STAPL_RUNTIME_CALL_CALLBACKS(s)                                   \
{                                                                         \
  stapl::runtime::context* ctx = stapl::runtime::this_context::try_get(); \
  if (ctx)                                                                \
    stapl::callback::call(ctx->get_id(), s);                              \
}

} // namespace stapl

#endif
