/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_LAZY_REF_HPP
#define STAPL_RUNTIME_LAZY_REF_HPP

#include "rmi_handle.hpp"
#include "serialization_fwd.hpp"
#include "type_traits/is_p_object.hpp"
#include <functional>
#include <memory>
#include <type_traits>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief @c std::reference_wrapper -like class for distributed objects.
///
/// This reference wrapper will only try to retrieve the distributed object
/// when the @ref lazy_reference_wrapper::get() or the implicit conversion
/// operator to @p T are called.
///
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
template<typename T>
class lazy_reference_wrapper
{
public:
  using type = T;
private:
  using non_cv_type = typename std::remove_cv<T>::type;

  rmi_handle::light_reference m_handle;

public:
  lazy_reference_wrapper(T& t) noexcept
  : m_handle(const_cast<non_cv_type&>(t).get_rmi_handle())
  { }

  lazy_reference_wrapper(T&&) = delete;

  //////////////////////////////////////////////////////////////////////
  /// @brief Returns a reference to the object.
  /// @todo Decide whether lazy_ref should be used inter-gang.  If not,
  ///  remove resolve_handle usage here and directly use in place where
  ///  lazy_ref is called in inter-gang contexts.
  //////////////////////////////////////////////////////////////////////
  T& get(void) const noexcept
  {
    T* const t = resolve_handle<non_cv_type>(m_handle);
    STAPL_RUNTIME_ASSERT(t);
    return *t;
  }

  operator T&(void) const noexcept
  { return get(); }

  void define_type(typer& t)
  { t.member(m_handle); }
};


//////////////////////////////////////////////////////////////////////
/// @brief Returns a pointer to the distributed object referenced by @p r.
///
/// This function is required for interoperability with @c boost::bind().
///
/// @related lazy_reference_wrapper
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
template<typename T>
T* get_pointer(lazy_reference_wrapper<T> const& r)
{
  return std::addressof(r.get());
}


//////////////////////////////////////////////////////////////////////
/// @brief Helper functions that creates an object of type
///        @ref lazy_reference_wrapper if @p T is a distributed object,
///        otherwise an object of type @c std::reference_wrapper.
///
/// @related lazy_reference_wrapper
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
template<typename T>
typename std::conditional<
           is_p_object<T>::value,
           lazy_reference_wrapper<T>,
           std::reference_wrapper<T>
         >::type lazy_ref(T& t)
{
  return t;
}


//////////////////////////////////////////////////////////////////////
/// @brief Helper function that creates an object of type
///        @ref lazy_reference_wrapper if @p T is a distributed object,
///        otherwise an object of type @c std::reference_wrapper.
///
/// @related lazy_reference_wrapper
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
template<typename T>
typename std::conditional<
           is_p_object<T>::value,
           lazy_reference_wrapper<const T>,
           std::reference_wrapper<const T>
         >::type lazy_cref(T const& t)
{
  return t;
}

} // namespace stapl

#endif
