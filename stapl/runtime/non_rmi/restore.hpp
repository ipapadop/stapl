/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_NON_RMI_RESTORE_HPP
#define STAPL_RUNTIME_NON_RMI_RESTORE_HPP

#include "../context.hpp"
#include "../future.hpp"
#include "../rmi_handle.hpp"
#include "../runqueue.hpp"
#include "../value_handle.hpp"
#include "../request/arguments.hpp"
#include "../request/location_rpc_request.hpp"
#include "../type_traits/callable_traits.hpp"
#include <memory>
#include <type_traits>
#include <utility>

namespace stapl {

namespace runtime {

//////////////////////////////////////////////////////////////////////
/// @brief Request for restoring an SPMD section.
///
/// @tparam ObjectHandle Distributed object handle type.
/// @tparam MemFun       Member function pointer type.
/// @tparam T            Argument types.
///
/// This restores the SPMD execution for a gang that its locations are not
/// on the top of the tread-local stack.
///
/// Restoring the SPMD section is a procedure that requires multiple tries,
/// since another gang might be calling @ref restore() as well. Therefore, it is
/// quite costly as an operation.
///
/// @ingroup requestBuildingBlock
//////////////////////////////////////////////////////////////////////
template<typename ObjectHandle, typename MemFun, typename... T>
class restore_request final
: public location_rpc_request,
  private arguments_t<MemFun, T...>
{
private:
  using args_type          = arguments_t<MemFun, T...>;
  using seq_type           = make_index_sequence<sizeof...(T)>;
  using object_type        = typename callable_traits<MemFun>::object_type;
  using pmf_result_type    = typename callable_traits<MemFun>::result_type;
public:
  using return_handle_type = value_handle<pmf_result_type>;

private:
  ObjectHandle              m_handle;
  const MemFun              m_pmf;
  return_handle_type* const m_rhandle;

  //////////////////////////////////////////////////////////////////////
  /// @brief Executes the request when the return type is @c void.
  //////////////////////////////////////////////////////////////////////
  void apply(object_type& t, std::true_type)
  {
    invoke(m_pmf, t, static_cast<args_type&>(*this),
           static_cast<void*>(this), seq_type{});
    m_rhandle->set_value();
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Executes the request when the return type is not @c void.
  //////////////////////////////////////////////////////////////////////
  void apply(object_type& t, std::false_type)
  {
    m_rhandle->set_value(
      invoke(m_pmf, t, static_cast<args_type&>(*this),
             static_cast<void*>(this), seq_type{}));
  }

public:
  template<typename... U>
  static std::size_t expected_size(U&&... u) noexcept
  {
    return (sizeof(restore_request) +
            dynamic_size<args_type>(seq_type{}, std::forward<U>(u)...));
  }

  template<typename RetHandle, typename Handle, typename F, typename... U>
  restore_request(RetHandle&& rh, Handle&& h, F&& f, U&&... u) noexcept
  : location_rpc_request(sizeof(*this)),
    args_type(std::forward_as_tuple(std::forward<U>(u),
                                    static_cast<void*>(this),
                                    this->size())...),
    m_handle(std::forward<Handle>(h)),
    m_pmf(std::forward<F>(f)),
    m_rhandle(std::forward<RetHandle>(rh))
  { }

  bool operator()(location_md& l, message_shared_ptr&) final
  {
    if (!this_context::can_restore(l)) {
      // cannot execute right now, try again later
      return false;
    }

    context ctx{l};

    auto* const t = m_handle.template get<object_type>(l);
    if (!t)
      STAPL_RUNTIME_ERROR("p_object does not exist.");

    if (l.is_leader()) {
      apply(*t, typename std::is_void<pmf_result_type>::type{});
    }
    else {
      invoke(m_pmf, *t, static_cast<args_type&>(*this),
             static_cast<void*>(this), seq_type{});
    }

    this->~restore_request();
    return true;
  }
};

} // namespace runtime


//////////////////////////////////////////////////////////////////////
/// @brief Restores the SPMD execution of the gang of the given object, using as
///        the entry point the given member function.
///
/// @param h   Handle to the target object.
/// @param pmf Member function to invoke.
/// @param t   Arguments to pass to the member function.
///
/// @return A @ref future object with the return value of the invoked member
///         function from location 0.
///
/// @ingroup distributedObjects
///
/// @todo It does not work for gangs that are on more that one process.
//////////////////////////////////////////////////////////////////////
template<typename Handle, typename MemFun, typename... T>
future<typename callable_traits<MemFun>::result_type>
restore(Handle const& h, MemFun const& pmf, T&&... t)
{
  using namespace stapl::runtime;

  auto& ctx = this_context::get();

  static_assert(is_appropriate_handle<Handle, MemFun>::value,
                "Incompatible qualifiers between handle and member function");
  STAPL_RUNTIME_ASSERT_MSG(h.valid(), "Invalid handle");

  using request_type = restore_request<
                         packed_handle_type,
                         MemFun,
                         typename std::remove_reference<T>::type...>;
  using return_handle_type = typename request_type::return_handle_type;

  std::unique_ptr<return_handle_type> p{new return_handle_type};
  {
    STAPL_RUNTIME_PROFILE("restore()", (primitive_traits::non_blocking |
                                        primitive_traits::p2m          |
                                        primitive_traits::comm));

    gang_md* const g = gang_md_registry::try_get(h.get_gang_id());
    if (!g)
      STAPL_RUNTIME_ERROR("Could not restore gang.");
    if (!g->get_description().is_on_shmem())
      STAPL_RUNTIME_ERROR("Not implemented for gangs that span multiple "
                          "processes.");

    auto const size = request_type::expected_size(std::forward<T>(t)...);
    all_locations_rpc_aggregator a{*g, h.get_epoch()};
    new(a.allocate(size)) request_type{p.get(), h, pmf, std::forward<T>(t)...};
  }

  scheduling_point(ctx);
  return future<typename callable_traits<MemFun>::result_type>{std::move(p)};
}

} // namespace stapl

#endif
