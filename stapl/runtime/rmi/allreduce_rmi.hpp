/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_RMI_ALLREDUCE_RMI_HPP
#define STAPL_RUNTIME_RMI_ALLREDUCE_RMI_HPP

#include "../aggregator.hpp"
#include "../context.hpp"
#include "../exception.hpp"
#include "../future.hpp"
#include "../instrumentation.hpp"
#include "../primitive_traits.hpp"
#include "../yield.hpp"
#include "../collective/allreduce_object.hpp"
#include "../non_rmi/response.hpp"
#include "../request/sync_rmi_request.hpp"
#include "../type_traits/callable_traits.hpp"
#include "../type_traits/transport_qualifier.hpp"
#include <memory>
#include <utility>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Allreduce RMI primitive.
///
/// The given member function is called on all locations the object is defined
/// on and the result of the local invocations of the member function are
/// combined using @p op and the result is distributed to all locations that
/// made the call. This result can be retrieved through the returned @ref future
/// object.
///
/// @param op  Reduction operator.
/// @param h   Handle to the target object.
/// @param pmf Member function to invoke.
/// @param t   Arguments to pass to the member function.
///
/// @return A @ref future object with the combined return values.
///
/// @ingroup ARMICollectives
//////////////////////////////////////////////////////////////////////
template<typename BinaryOperation,
         typename Handle,
         typename MemFun,
         typename... T>
future<
    runtime::binary_operation_result_t<
      typename callable_traits<MemFun>::result_type,
      BinaryOperation>
>
allreduce_rmi(BinaryOperation op, Handle const& h, MemFun const& pmf, T&&... t)
{
  using namespace stapl::runtime;

  auto& ctx = this_context::get();

  static_assert(is_appropriate_handle<Handle, MemFun>::value,
                "Incompatible qualifiers between handle and member function");
  STAPL_RUNTIME_ASSERT_MSG(h.valid(), "Invalid handle");
  STAPL_RUNTIME_ASSERT_MSG(ctx.is_base(), "Only allowed in SPMD");
  STAPL_RUNTIME_ASSERT_MSG(ctx.get_gang_id()==h.get_gang_id(),
                           "Intergang collectives not supported");

  using result_type =
    binary_operation_result_t<typename callable_traits<MemFun>::result_type,
                              BinaryOperation>;
  using return_handle_type = allreduce_object<result_type, BinaryOperation>;
  using response_type =
    active_handle_response<packed_handle_type, return_handle_type>;

  std::unique_ptr<return_handle_type>
    p{new return_handle_type{ctx, std::move(op)}};
  {
    STAPL_RUNTIME_PROFILE("allreduce_rmi()", (primitive_traits::non_blocking |
                                              primitive_traits::ordered      |
                                              primitive_traits::coll         |
                                              primitive_traits::comm));

    using request_type =
      sync_rmi_request<response_type,
                       packed_handle_type,
                       MemFun,
                       typename transport_qualifier<decltype(t)>::type...>;

    auto const size = request_type::expected_size(std::forward<T>(t)...);
    aggregator a{ctx, h, ctx.get_location_id(), no_implicit_flush};
    new(a.allocate(size)) request_type{*p, h, pmf, std::forward<T>(t)...};
  }

  scheduling_point(ctx);
  return future<result_type>{std::move(p)};
}

} // namespace stapl

#endif
