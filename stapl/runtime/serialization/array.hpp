/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_SERIALIZATION_ARRAY_HPP
#define STAPL_RUNTIME_SERIALIZATION_ARRAY_HPP

#include "typer_fwd.hpp"
#include "../type_traits/is_basic.hpp"
#include <cstring>
#include <type_traits>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref typer_traits for C arrays of non-basic @p T.
///
/// @tparam T    Array element type.
/// @tparam Size Size of the array.
///
/// @see is_basic
/// @ingroup serialization
//////////////////////////////////////////////////////////////////////
template<typename T, std::size_t Size>
class typer_traits<T[Size],
                   typename std::enable_if<!is_basic<T>::value>::type>
{
public:
  using value_type = T[Size];

  static std::size_t packed_size(value_type const& t) noexcept
  {
    typer ct{typer::SIZE};
    for (std::size_t i=0; i<Size; ++i)
      ct.member(t[i]);
    return ct.offset();
  }

  static std::pair<bool, std::size_t>
  meets_requirements(const typer::pass_type p, value_type const& t) noexcept
  {
    typer ct{p};
    for (std::size_t i=0; i<Size; ++i)
      ct.member(t[i]);
    return ct.meets_requirements();
  }

  static void prepack(value_type* dest,
                      value_type const* src,
                      const std::size_t num = 1) noexcept
  {
    std::memcpy(static_cast<void*>(dest),
                static_cast<void const*>(src),
                (sizeof(value_type) * num));
  }

  static std::size_t pack(value_type& dest,
                          void* base,
                          const std::size_t offset,
                          value_type const& src) noexcept
  {
    typer ct{dest, src, base, offset};
    for (std::size_t i=0; i<Size; ++i)
      ct.member(dest[i]);
    return (ct.offset() - offset); // actual packed size is needed, not total
  }

  static std::size_t unpack(value_type& t, void* base)
  {
    typer ct{base};
    for (std::size_t i=0; i<Size; ++i)
      ct.member(t[i]);
    return ct.offset();
  }

  static void destroy(value_type& t) noexcept
  {
    typer ct{typer::DESTROY};
    for (std::size_t i=0; i<Size; ++i)
      ct.member(t[i]);
  }
};


//////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref typer_traits_specialization for C arrays.
///
/// @ingroup serialization
//////////////////////////////////////////////////////////////////////
template<typename T, std::size_t Size>
struct typer_traits_specialization<T[Size],
                                   typename std::enable_if<
                                     !is_basic<T>::value
                                   >::type>
: public std::true_type
{ };

} // namespace stapl

#endif
