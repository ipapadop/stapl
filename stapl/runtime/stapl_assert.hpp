/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_ASSERT_HPP
#define STAPL_RUNTIME_ASSERT_HPP

#if !defined(STAPL_NDEBUG)


# ifdef _STAPL

#  include <exception>
#  include <boost/current_function.hpp>

namespace stapl {

namespace runtime {

void assert_fail(const char* s,
                 const char* file, unsigned int line, const char* function);

void warning(const char* s,
             const char* file, unsigned int line, const char* function);

} // namespace runtime

} // namespace stapl


//////////////////////////////////////////////////////////////////////
/// @brief Ensures the given input condition is true.
///
/// If @p E is @c true, nothing happens, otherwise the error message @p M is
/// printed and the program aborts.
///
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
#  define stapl_assert(E, M)                            \
  ( (std::uncaught_exception() || (E)) ?                \
    (static_cast<void>(0)) :                            \
    (stapl::runtime::assert_fail("STAPL ASSERTION: " M, \
                                 __FILE__, __LINE__, BOOST_CURRENT_FUNCTION)) )

//////////////////////////////////////////////////////////////////////
/// @brief Ensures the given input condition is true.
///
/// If @p E is @c true, nothing happens, otherwise the warning message @p M is
/// printed.
///
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
#  define stapl_warning(E, M)                     \
  ( (std::uncaught_exception() || (E)) ?          \
    (static_cast<void>(0)) :                      \
    (stapl::runtime::warning("STAPL WARNING: " M, \
                             __FILE__, __LINE__, BOOST_CURRENT_FUNCTION)) )

# else // _STAPL

#  include <cstdio>
#  include <cstdlib>
#  include <exception>

//////////////////////////////////////////////////////////////////////
/// @brief Ensures the given input condition is true.
///
/// If @p E is @c true, nothing happens, otherwise the error message @p M is
/// printed and the program aborts.
///
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
#  define stapl_assert(E, M) do {                                        \
    if ( !std::uncaught_exception() && !(E) ) {                          \
      std::fprintf(stderr, "STAPL ASSERTION: %s (file: %s, line: %d)\n", \
                   M, __FILE__, __LINE__);                               \
      std::abort();                                                      \
    }                                                                    \
  } while (false)

//////////////////////////////////////////////////////////////////////
/// @brief Ensures the given input condition is true.
///
/// If @p E is @c true, nothing happens, otherwise the warning message @p M is
/// printed.
///
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
#  define stapl_warning(E, M)                                         \
  ( (std::uncaught_exception() || (E)) ?                              \
    (static_cast<void>(0)) :                                          \
    (std::fprintf(stderr, "STAPL WARNING: %s (file: %s, line: %d)\n", \
                  M, __FILE__, __LINE__)) )

#  endif // _STAPL


#else // STAPL_NDEBUG


//////////////////////////////////////////////////////////////////////
/// @brief Ensures the given input condition is true.
///
/// If @p E is true, nothing happens, otherwise the error message @p M is
/// printed and the program aborts.
///
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
# define stapl_assert(E, M) static_cast<void>(0)

//////////////////////////////////////////////////////////////////////
/// @brief Ensures the given input condition is true.
///
/// If @p E is @c true, nothing happens, otherwise the warning message @p M is
/// printed.
///
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
# define stapl_warning(E, M) static_cast<void>(0)


#endif // STAPL_NDEBUG

#endif
