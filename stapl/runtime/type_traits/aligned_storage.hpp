/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_TYPE_TRAITS_ALIGNED_STORAGE_HPP
#define STAPL_RUNTIME_TYPE_TRAITS_ALIGNED_STORAGE_HPP

#include "../config/platform.hpp"
#include <type_traits>

namespace stapl {

namespace runtime {

////////////////////////////////////////////////////////////////////
/// @brief Provides the member typedef type, which is a POD type suitable for
///        use as uninitialized storage for any object whose size is at most
///        @p Len and whose alignment requirement is a divisor of
///        @ref STAPL_RUNTIME_DEFAULT_ALIGNMENT.
///
/// @ingroup runtimeTypeTraits
////////////////////////////////////////////////////////////////////
template<std::size_t Len>
using aligned_storage_t =
  typename std::aligned_storage<Len, STAPL_RUNTIME_DEFAULT_ALIGNMENT>::type;


////////////////////////////////////////////////////////////////////
/// @brief Returns @p n adjusted with extra padding bytes to satisfy alignment
///        of @p alignment.
///
/// @ingroup runtimeTypeTraits
////////////////////////////////////////////////////////////////////
constexpr std::size_t
aligned_size(const std::size_t n,
             const std::size_t alignment =
               STAPL_RUNTIME_DEFAULT_ALIGNMENT) noexcept
{
  return (n + /* padding */ (alignment - ((n - 1) % alignment + 1)));
}

} // namespace runtime

} // namespace stapl

#endif
