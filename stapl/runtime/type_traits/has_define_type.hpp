/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_TYPE_TRAITS_HAS_DEFINE_TYPE_HPP
#define STAPL_RUNTIME_TYPE_TRAITS_HAS_DEFINE_TYPE_HPP

#include "../serialization/typer_fwd.hpp"
#include <type_traits>
#include <utility>

namespace stapl {

namespace runtime {

////////////////////////////////////////////////////////////////////
/// @brief Returns @c std::false_type if @c T::define_type(stapl::typer&) does
///        not exist or is not accessible.
///
/// @ingroup runtimeTypeTraitsImpl
////////////////////////////////////////////////////////////////////
constexpr std::false_type
define_type_exists(...)
{ return std::false_type{}; }


////////////////////////////////////////////////////////////////////
/// @brief Returns @c std::true_type if @c T::define_type(stapl::typer&) exists.
///
/// @ingroup runtimeTypeTraitsImpl
////////////////////////////////////////////////////////////////////
template<typename T>
constexpr std::true_type
define_type_exists(T* t,
                   decltype(
                     runtime::define_type_cast(*t).define_type(
                       std::declval<typer&>())
                   )* = nullptr)
{ return std::true_type{}; }


////////////////////////////////////////////////////////////////////
/// @brief Detects if @p T has a function @c T::define_type(stapl::typer&).
///
/// @ingroup runtimeTypeTraitsImpl
////////////////////////////////////////////////////////////////////
template<typename T,
         bool = (std::is_scalar<T>::value || std::is_array<T>::value)>
struct has_define_type_impl
: public decltype(define_type_exists(static_cast<T*>(nullptr)))
{ };


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref has_define_type_impl for scalars and arrays.
///
/// @ingroup runtimeTypeTraitsImpl
////////////////////////////////////////////////////////////////////
template<typename T>
struct has_define_type_impl<T, true>
: public std::false_type
{ };


////////////////////////////////////////////////////////////////////
/// @brief Detects if @p T has a function @c T::define_type(stapl::typer&).
///
/// @ingroup runtimeTypeTraits
////////////////////////////////////////////////////////////////////
template<typename T>
struct has_define_type
: public has_define_type_impl<typename std::remove_cv<T>::type>
{ };


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref has_define_type for references.
///
/// @ingroup runtimeTypeTraits
////////////////////////////////////////////////////////////////////
template<typename T>
struct has_define_type<T&>
: public std::false_type
{ };


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref has_define_type for pointers.
///
/// @ingroup runtimeTypeTraits
////////////////////////////////////////////////////////////////////
template<typename T>
struct has_define_type<T*>
: public std::false_type
{ };


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref has_define_type for @c const pointers.
///
/// @ingroup runtimeTypeTraits
////////////////////////////////////////////////////////////////////
template<typename T>
struct has_define_type<T* const>
: public std::false_type
{ };


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref has_define_type for @c volatile pointers.
///
/// @ingroup runtimeTypeTraits
////////////////////////////////////////////////////////////////////
template<typename T>
struct has_define_type<T* volatile>
: public std::false_type
{ };


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref has_define_type for @c const @c volatile
///        pointers.
///
/// @ingroup runtimeTypeTraits
////////////////////////////////////////////////////////////////////
template<typename T>
struct has_define_type<T* const volatile>
: public std::false_type
{ };

} // namespace runtime

} // namespace stapl

#endif
