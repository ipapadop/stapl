/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_TYPE_TRAITS_LAZY_STORAGE_HPP
#define STAPL_RUNTIME_TYPE_TRAITS_LAZY_STORAGE_HPP

#include <type_traits>
#include <utility>

namespace stapl {

////////////////////////////////////////////////////////////////////
/// @brief Provides lazy construction of objects with automatic storage.
///
/// @tparam T     Object type.
/// @tparam Align Object alignment.
///
/// This struct is a POD and while it reserves stack space for the object, the
/// object is initialized only when it is explicitly required to.
///
/// @ingroup runtimeTypeTraits
////////////////////////////////////////////////////////////////////
template<typename T, std::size_t Align = std::alignment_of<T>::value>
struct lazy_storage
{
private:
  ////////////////////////////////////////////////////////////////////
  /// @brief Calls the destructor of the stored object.
  ////////////////////////////////////////////////////////////////////
  struct destructor
  {
    T& m_t;

    constexpr explicit destructor(T& t) noexcept
    : m_t(t)
    { }

    ~destructor(void)
    { m_t.~T(); }
  };

  typename std::aligned_storage<sizeof(T), Align>::type m_storage;

public:
  template<typename... U>
  void construct(U&&... u)
  { new(&m_storage) T(std::forward<U>(u)...); }

  void destroy(void) noexcept
  { reinterpret_cast<T*>(&m_storage)->~T(); }

  T const& get(void) const noexcept
  { return *reinterpret_cast<const T*>(&m_storage); }

  T& get(void) noexcept
  { return *reinterpret_cast<T*>(&m_storage); }

  T moveout(void)
  {
    destructor d{get()};
    return std::move(get());
  }
};

} // namespace stapl

#endif
