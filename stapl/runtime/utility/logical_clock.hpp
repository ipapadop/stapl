/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_UTILITY_LOGICAL_CLOCK_HPP
#define STAPL_RUNTIME_UTILITY_LOGICAL_CLOCK_HPP

#include "../exception.hpp"
#include <limits>
#include <tuple>

namespace stapl {

namespace runtime {

//////////////////////////////////////////////////////////////////////
/// @brief Implements a logical clock for managing epochs.
///
/// @ingroup runtimeUtility
//////////////////////////////////////////////////////////////////////
class logical_clock
{
public:
  typedef unsigned int          time_type;
  typedef std::tuple<time_type> member_types;

  static const time_type no_time    = 0;
  static const time_type start_time = 1;

private:
  time_type m_time;

public:
  constexpr explicit logical_clock(const time_type t = start_time) noexcept
  : m_time(t)
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief Increases the time.
  //////////////////////////////////////////////////////////////////////
  time_type tick(void) noexcept
  {
    const time_type t = ++m_time;
    if (t == std::numeric_limits<time_type>::max())
      STAPL_RUNTIME_ERROR("Logical clock overflowed.");
    return t;
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Returns the current time.
  //////////////////////////////////////////////////////////////////////
  constexpr time_type time(void) const noexcept
  { return m_time; }

  void reset(const time_type t = start_time) noexcept
  { m_time = t; }

  friend constexpr bool operator==(logical_clock const& x,
                                   logical_clock const& y) noexcept
  {
    return (x.m_time==y.m_time);
  }

  friend constexpr bool operator!=(logical_clock const& x,
                                   logical_clock const& y) noexcept
  {
    return !(x==y);
  }
};

} // namespace runtime

} // namespace stapl

#endif
