/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_YIELD_HPP
#define STAPL_RUNTIME_YIELD_HPP

#include "context.hpp"
#include "runqueue.hpp"
#include "tags.hpp"
#include <utility>

namespace stapl {

namespace runtime {

//////////////////////////////////////////////////////////////////////
/// @brief Yields the location to execute some requests.
///
/// @ingroup requestBuildingBlock
//////////////////////////////////////////////////////////////////////
void scheduling_point(context&);


//////////////////////////////////////////////////////////////////////
/// @brief Yields until @p pred returns @c true.
///
/// This function will flush aggregated requests if @p pred returns @c false.
///
/// @return @c true if it yielded, otherwise @c false.
///
/// @ingroup requestBuildingBlock
//////////////////////////////////////////////////////////////////////
template<typename Predicate>
bool yield_until(context& ctx, Predicate&& pred)
{
  if (pred())
    return false;
  ctx.flush_requests();
  runqueue::wait(std::forward<Predicate>(pred));
  return true;
}


//////////////////////////////////////////////////////////////////////
/// @brief Yields until @p pred returns @c true.
///
/// This function does not flush aggregated requests.
///
/// @return @c true if it yielded, otherwise @c false.
///
/// @ingroup requestBuildingBlock
//////////////////////////////////////////////////////////////////////
template<typename Predicate>
bool yield_until(const no_context_t, Predicate&& pred)
{
  if (pred())
    return false;
  runqueue::wait(std::forward<Predicate>(pred));
  return true;
}


//////////////////////////////////////////////////////////////////////
/// @brief Yields until @p pred returns @c true.
///
/// This function will flush aggregated requests if a @ref context exists and
/// @p pred returns @c false.
///
/// @return @c true if it yielded, otherwise @c false.
///
/// @ingroup requestBuildingBlock
//////////////////////////////////////////////////////////////////////
template<typename Predicate>
bool yield_until(Predicate&& pred)
{
  if (pred())
    return false;
  context* const ctx = this_context::try_get();
  if (ctx)
    ctx->flush_requests();
  runqueue::wait(std::forward<Predicate>(pred));
  return true;
}


//////////////////////////////////////////////////////////////////////
/// @brief Yields if @p pred does not return @c true.
///
/// This function will flush aggregated requests if @p pred returns @c false.
///
/// @return The result of @p pred.
///
/// @ingroup requestBuildingBlock
//////////////////////////////////////////////////////////////////////
template<typename Predicate>
bool yield_if_not(context& ctx, Predicate&& pred)
{
  if (pred())
    return true;
  ctx.flush_requests();
  runqueue::yield();
  return pred();
}


//////////////////////////////////////////////////////////////////////
/// @brief Yields if @p pred does not return @c true.
///
/// This function does not flush aggregated requests.
///
/// @return The result of @p pred.
///
/// @ingroup requestBuildingBlock
//////////////////////////////////////////////////////////////////////
template<typename Predicate>
bool yield_if_not(const no_context_t, Predicate&& pred)
{
  if (pred())
    return true;
  runqueue::yield();
  return pred();
}


//////////////////////////////////////////////////////////////////////
/// @brief Yields if @p pred does not return @c true.
///
/// This function will flush aggregated requests if a @ref context exists and
/// @p pred returns @c false.
///
/// @return The result of @p pred.
///
/// @ingroup requestBuildingBlock
//////////////////////////////////////////////////////////////////////
template<typename Predicate>
bool yield_if_not(Predicate&& pred)
{
  if (pred())
    return true;
  context* const ctx = this_context::try_get();
  if (ctx)
    ctx->flush_requests();
  runqueue::yield();
  return pred();
}

} // namespace runtime

} // namespace stapl

#endif
