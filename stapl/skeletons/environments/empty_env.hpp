/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_ENVIRONMENTS_EMPTY_ENV_HPP
#define STAPL_SKELETONS_ENVIRONMENTS_EMPTY_ENV_HPP

namespace stapl {
namespace skeletons {

//////////////////////////////////////////////////////////////////////
/// @brief An @c empty_env is a dummy environment which ignores all
/// the requests sent to it.
///
/// This environment is used to simplify the default cases in the
/// @c skeleton_manager.
///
/// @ingroup skeletonsEnvironments
//////////////////////////////////////////////////////////////////////
class empty_env
{
  std::size_t                      m_num_PEs;
  runtime::location_id             m_PE_id;
public:
  std::size_t get_num_PEs() const
  {
    return m_num_PEs;
  }

  std::size_t get_PE_id() const
  {
    return m_PE_id;
  }

  template <typename... Args>
  void pre_spawn(Args&&... args) const
  { }

  template <typename... Args>
  void post_spawn(Args&&... args) const
  { }

  void init_location_info(std::size_t num_PEs, runtime::location_id PE_id)
  {
    m_num_PEs = num_PEs;
    m_PE_id = PE_id;
  }

  template <bool isResult, typename... Args>
  void spawn_element(Args&&... args) const
  { }

  template <typename... Args>
  void set_num_succs(Args&&... args) const
  { }

  void define_type(typer& t)
  {
    t.member(m_num_PEs);
    t.member(m_PE_id);
  }
};

} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_ENVIRONMENTS_EMPTY_ENV_HPP
