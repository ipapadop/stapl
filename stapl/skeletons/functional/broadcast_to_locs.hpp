/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_FUNCTIONAL_BROADCAST_TO_LOCS_HPP
#define STAPL_SKELETONS_FUNCTIONAL_BROADCAST_TO_LOCS_HPP

#include <stapl/skeletons/utility/utility.hpp>
#include <stapl/skeletons/utility/tags.hpp>
#include <stapl/skeletons/utility/skeleton.hpp>
#include <stapl/skeletons/spans/per_location.hpp>
#include "broadcast.hpp"

namespace stapl {
namespace skeletons {
namespace skeletons_impl {


template <typename Flows, typename Tag, bool SetResult>
struct broadcast_to_locs
  : public skeletons_impl::broadcast<
             stapl::identity_op, Flows, spans::per_location, Tag, SetResult>


{
  using skeleton_tag_type = tags::broadcast_to_locs;
  using op_type          = stapl::identity_op;
  using base_type =
    skeletons_impl::broadcast<
      stapl::identity_op, Flows, spans::per_location, Tag, SetResult>;

  broadcast_to_locs(void)
    : base_type(stapl::identity_op())
  { }

  op_type get_op(void) const
  {
    return stapl::identity_op();
  }

  void define_type(typer& t)
  {
    t.base<base_type>(*this);
  }
};

} // namespace skeletons_impl


namespace result_of {

template <typename Tag,
          typename Flows,
          bool SetResult>
using broadcast_to_locs = skeletons_impl::
                            broadcast_to_locs<Flows, Tag, SetResult>;

} // namespace result_of


//////////////////////////////////////////////////////////////////////
/// @brief This broadcast skeleton is used when the result of the
/// broadcast should have a representative on each location.
/// Therefore, a @c span::per_location is used in this skeleton.
/// @c Flows are still allowed to be customized for this skeleton.
///
/// @tparam SetResult whether the skeleton should set the task
///                   results on the pg edge container or not
/// @tparam Flows     the customized flow to be used in the
///                   @c reverse_tree
/// @tparam Tag       determines the type of the broadcast skeleton
/// @return a broadcast skeleton that broadcasts one element to each
///         location with custom flows
///
/// @see broadcast
/// @see spans::per_location
///
/// @ingroup skeletonsFunctionalBroadcast
//////////////////////////////////////////////////////////////////////
template <bool SetResult  = false,
          typename Tag    = stapl::use_default,
          typename Flows  = stapl::use_default>
inline result_of::broadcast_to_locs<Tag, Flows, SetResult>
broadcast_to_locs(void)
{
  return result_of::broadcast_to_locs<
           Tag, Flows, SetResult>();
}

} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_FUNCTIONAL_BROADCAST_TO_LOCS_HPP
