/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_FUNCTIONAL_PRE_BROADCAST_HPP
#define STAPL_SKELETONS_FUNCTIONAL_PRE_BROADCAST_HPP

#include <type_traits>
#include <utility>
#include <stapl/skeletons/utility/utility.hpp>
#include <stapl/skeletons/utility/skeleton.hpp>
#include <stapl/skeletons/utility/tags.hpp>
#include <stapl/skeletons/operators/elem.hpp>
#include <stapl/skeletons/param_deps/pre_broadcast_pd.hpp>
#include <stapl/skeletons/spans/balanced.hpp>

namespace stapl {
namespace skeletons {
namespace skeletons_impl {


template <std::size_t Arity, typename Op, typename SkeletonTraits>
struct pre_broadcast
  : public decltype(
      skeletons::elem<
        default_type<typename SkeletonTraits::span_type, spans::balanced<1>>>(
        skeletons::pre_broadcast_pd<
          Arity,
          default_type<typename SkeletonTraits::span_type, spans::balanced<1>>,
          SkeletonTraits::set_result>(
          std::declval<Op>())))
{
  using skeleton_tag_type = tags::pre_broadcast;
  using op_type     = Op;
  static constexpr bool setting_result = SkeletonTraits::set_result;
  using span_t =
    default_type<typename SkeletonTraits::span_type, spans::balanced<1>>;
  using base_type = decltype(
                      skeletons::elem<span_t>(
                        skeletons::pre_broadcast_pd<
                          Arity, span_t, setting_result>(
                            std::declval<Op>())));

  pre_broadcast(Op const& op, SkeletonTraits const& traits)
    : base_type(skeletons::elem<span_t>(
        skeletons::pre_broadcast_pd<Arity, span_t, setting_result>(op)))
  { }

  Op get_op(void) const
  {
    return base_type::nested_skeleton().get_op();
  }

  void define_type(typer& t)
  {
    t.base<base_type>(*this);
  }
};

} // namespace skeletons_impl

namespace result_of {

template <std::size_t Arity, typename Op, typename SkeletonTraits>
using pre_broadcast =
  skeletons_impl::pre_broadcast<Arity,
                                typename std::decay<Op>::type,
                                typename std::decay<SkeletonTraits>::type>;

} // namespace result_of


//////////////////////////////////////////////////////////////////////
/// @brief  This skeleton chooses an element from the domain of the input
///         for broadcast when the input domain is non-scalar and we want to
///         broadcast one element from the domain (e.g. last one).
///
/// @tparam Arity   number of inputs to the @c op workfunction
/// @param  op      the workfunction to be used in each pre_broadcast
///                 parametric dependency
/// @param  traits  the traits to be used (default = default_skeleton_traits)
///
/// @ingroup skeletonsFunctional
/// @todo currently this skeleton chooses the last element of the domain
///       for broadcast, in future should be extended to arbitrary element
///       of domain
//////////////////////////////////////////////////////////////////////
template <std::size_t Arity = 1,
          typename Op,
          typename SkeletonTraits = skeletons_impl::default_skeleton_traits>
result_of::pre_broadcast<Arity, Op, SkeletonTraits>
pre_broadcast(Op&& op, SkeletonTraits&& traits = SkeletonTraits())
{
  return result_of::pre_broadcast<Arity, Op, SkeletonTraits>(
    std::forward<Op>(op), std::forward<SkeletonTraits>(traits));
}

} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_FUNCTIONAL_PRE_BROADCAST_HPP
