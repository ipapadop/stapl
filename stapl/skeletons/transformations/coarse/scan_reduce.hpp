/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_TRANSFORMATIONS_COARSE_SCAN_REDUCE_HPP
#define STAPL_SKELETONS_TRANSFORMATIONS_COARSE_SCAN_REDUCE_HPP

#include <type_traits>
#include <stapl/utility/utility.hpp>
#include <stapl/skeletons/utility/tags.hpp>
#include <stapl/skeletons/transformations/wrapped_skeleton.hpp>
#include <stapl/skeletons/transformations/transform.hpp>

#include <stapl/skeletons/utility/skeleton.hpp>
#include <stapl/skeletons/transformations/coarse/coarse.hpp>
#include <stapl/skeletons/transformations/optimizers/scan.hpp>
#include <stapl/skeletons/transformations/optimizers/reduce.hpp>

#include <stapl/algorithms/identity_value.hpp>
#include <stapl/skeletons/transformations/wrapped_skeleton.hpp>
#include <stapl/skeletons/transformations/coarse/scan.hpp>
#include <stapl/skeletons/operators/elem.hpp>
#include <stapl/skeletons/functional/reduce.hpp>
#include <stapl/skeletons/functional/map.hpp>
#include <stapl/skeletons/functional/scan.hpp>
#include <stapl/skeletons/functional/scan_reduce.hpp>
#include <stapl/skeletons/functional/pre_broadcast.cpp>
#include <stapl/skeletons/param_deps/shifted_first_pd.hpp>
#include <stapl/skeletons/param_deps/zip_pd.hpp>
#include <stapl/skeletons/flows/inline_flows.hpp>
#include <stapl/skeletons/spans/only_nearest_pow_two.hpp>

namespace stapl {
namespace skeletons {
namespace transformations {


template <typename S, typename SkeletonTag, typename CoarseTag>
struct transform;

namespace ph = stapl::skeletons::flows::inline_flows::placeholders;

//////////////////////////////////////////////////////////////////////
/// @brief Coarsened version of exclusive scan_reduce skeleton
///
/// @tparam S            the scan skeleton
/// @tparam Tag          type of exclusive scan to use for the conquering phase
/// @tparam CoarseTag    a tag to specify the required specialization for
///                      coarsening
/// @tparam ExecutionTag a tag to specify the execution method used for
///                      the coarsened chunks
///
/// @see wrapped_skeleton
///
/// @ingroup skeletonsTransformationsCoarse
//////////////////////////////////////////////////////////////////////
template <typename S, typename Tag,
          typename CoarseTag, typename ExecutionTag>
struct transform<S, tags::scan_reduce<Tag, tags::exclusive>,
                 tags::coarse<CoarseTag, ExecutionTag>>
{
  using value_t = typename S::value_t;

  static auto call(S const& skeleton)
    STAPL_AUTO_RETURN((compose<skeletons::tags::inline_flow>(
      ph::x<0>() << map(
        skeletons::wrap<ExecutionTag>(skeletons::reduce(skeleton.get_op()))) |
        ph::input<0>(),
      ph::x<1>() << skeletons::scan<Tag>(skeleton.get_op(),
                                         skeleton.get_init_value()) |
        ph::x<0>(),
      ph::x<2>() << skeletons::elem(
        skeletons::scan_helpers::find_scan_update_phase_pd::
          call<tags::exclusive, stapl::use_default>(skeleton.get_op())) |
        (ph::x<1>(), ph::input<0>(), ph::input<1>()),
      ph::x<3>() << skeletons::pre_broadcast<2>(
        skeleton.get_op(), skeleton_traits<spans::only_nearest_pow_two>()) |
        (ph::x<0>(), ph::x<1>()),
      ph::x<4>() << skeletons::broadcast_to_locs<true, tags::right_aligned>() |
        ph::x<3>())))
};

//////////////////////////////////////////////////////////////////////
/// @brief Coarsened version of inclusive scan_reduce skeleton
///
/// @tparam S            the scan skeleton
/// @tparam Tag          type of inclusive scan to use for the conquering phase
/// @tparam CoarseTag    a tag to specify the required specialization for
///                      coarsening
/// @tparam ExecutionTag a tag to specify the execution method used for
///                      the coarsened chunks
///
/// @see wrapped_skeleton
///
/// @ingroup skeletonsTransformationsCoarse
//////////////////////////////////////////////////////////////////////
template <typename S, typename Tag,
          typename CoarseTag, typename ExecutionTag>
struct transform<S, tags::scan_reduce<Tag, tags::inclusive>,
                 tags::coarse<CoarseTag, ExecutionTag>>
{
  using value_t = typename S::value_t;

  static auto call(S const& skeleton)
    STAPL_AUTO_RETURN((compose<skeletons::tags::inline_flow>(
      ph::x<0>() << map(
        skeletons::wrap<ExecutionTag>(skeletons::reduce(skeleton.get_op()))) |
        ph::input<0>(),
      ph::x<1>() << skeletons::scan<Tag>(skeleton.get_op()) | ph::x<0>(),
      ph::x<2>() << skeletons::elem(
        skeletons::scan_helpers::find_scan_update_phase_pd::
          call<tags::inclusive, CoarseTag>(skeleton.get_op())) |
        (ph::x<1>(), ph::input<0>(), ph::input<1>()),
      ph::x<3>() << skeletons::pre_broadcast<1>(
        stapl::identity<value_t>(),
        skeleton_traits<spans::only_nearest_pow_two>()) |
        ph::x<1>(),
      ph::x<4>() << skeletons::broadcast_to_locs<true, tags::right_aligned>() |
        ph::x<3>())))
};


} // namespace transformations
} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_TRANSFORMATIONS_COARSE_SCAN_REDUCE_HPP
