/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_UTILITY_BITREVERSE_HPP
#define STAPL_SKELETONS_UTILITY_BITREVERSE_HPP

#include <cmath>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Given a value this functor computes its bitreversed value by
/// considering the least signficant bits specified by ceil(log2(max))
//////////////////////////////////////////////////////////////////////
template <typename T>
struct bitreverse
{
  unsigned int m_number_of_bits;

  //////////////////////////////////////////////////////////////////////
  /// @brief Set the number of bits to use by determining the number
  ///        of bits needed to represent the value provided.
  /// @param max - ceil(log_2(max)) determines the number of bits in
  ///            bitreverse
  //////////////////////////////////////////////////////////////////////
  bitreverse(T max)
    : m_number_of_bits(std::ceil(std::log2(max)))
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief Convert a number to its bitreverse value. For example, if the
  ///        struct is constructed with a max of 32(100000) and the
  ///        operator receives 2(000010) the operator will return
  ///        010000.
  //////////////////////////////////////////////////////////////////////
  T operator()(T input) const
  {
    unsigned long bitreverse = 0;

    for (unsigned int i = 0; i < m_number_of_bits; ++i)
    {
      bitreverse <<= 1;

      if (input & 1)
        bitreverse += 1;

      input >>= 1;
    }

    return bitreverse;
  }


  void define_type(typer& t)
  {
    t.member(m_number_of_bits);
  }
};

} // namespace stapl

#endif //STAPL_SKELETONS_UTILITY_BITREVERSE_HPP
