/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_UTILITY_MAPPERS_HPP
#define STAPL_SKELETONS_UTILITY_MAPPERS_HPP

#include <stapl/skeletons/param_deps/wavefront_utils.hpp>
#include <stapl/skeletons/utility/should_flow.hpp>

namespace stapl {
namespace skeletons {

/////////////////////////////////////////////////////////////////////////
/// @brief default mapper that maps the linearized index
///        the consumer task to the corresponding linearized index
///        of the producer task, basically just linearization.
///
/// @tparam Dim dimension of skeleton
/////////////////////////////////////////////////////////////////////////
template <int Dim>
class default_output_to_input_mapper
{
  using domain_type = indexed_domain<std::size_t, Dim>;
  using index_type = typename domain_type::index_type;
  using traversal_t = typename domain_type::traversal_type;
  using linearize_t = nd_linearize<index_type, traversal_t>;

  linearize_t m_linear_mf;

public:
  void set_direction(direction dir)
  { }

  void set_dimensions(index_type const& dimension)
  {
    m_linear_mf = linearize_t(dimension);
  }

  std::size_t operator()(index_type coord) const
  {
    return m_linear_mf(coord);
  }

  void define_type(typer& t)
  {
    t.member(m_linear_mf);
  }
};

/////////////////////////////////////////////////////////////////////////
/// @brief A mapper for mapping the result ids from the child skeleton
///        to parent skeleton, for default case given the coordination
///        of producer task, it will map to the new coordination
///        in parent dimension
/// @tparam Dim dimension of skeleton
/////////////////////////////////////////////////////////////////////////
template <int Dim>
class default_output_to_output_mapper
{
  using should_flow_t = recursive_should_flow<Dim>;
  using corner_t = skeletons::position;
  using corner_type = std::array<corner_t, Dim>;
  using dir_t = direction;

  using domain_type = indexed_domain<std::size_t, Dim>;
  using index_type = typename domain_type::index_type;
  using traversal_t = typename domain_type::traversal_type;
  using linearizer_t = nd_linearize<index_type, traversal_t>;
  using rev_linearizer_t = nd_reverse_linearize<index_type, traversal_t>;

  corner_type m_corner;
  index_type m_cur_coord;
  index_type m_cur_dimension;
  index_type m_parent_coord;
  index_type m_parent_dimension;
  index_type m_task_dimension;
  should_flow_t m_should_flow;

  std::array<dir_t, Dim>  m_sides;
  std::vector<std::size_t> m_res_ids;

public:
  void set_direction(dir_t dir)
  { }

  void set_dimensions(index_type const& cur_coord,
                      index_type const& cur_dimension,
                      index_type const& parent_dimension,
                      index_type const& task_dimension)
  {
    m_cur_coord = cur_coord;
    m_cur_dimension = cur_dimension;
    m_parent_dimension = parent_dimension;
    m_task_dimension = task_dimension;
    m_parent_coord = tuple_ops::transform(
      m_cur_coord, m_task_dimension, stapl::multiplies<std::size_t>());
    m_should_flow.set_dimension(task_dimension);

    for (std::size_t dir = 0; dir < Dim; ++dir)
      m_sides[dir] = (dir_t) dir;

  }

  std::size_t operator()(std::size_t idx) const
  {
    using namespace tuple_ops;

    index_type child_coord = rev_linearizer_t(m_task_dimension)(idx);
    index_type parent_coord = tuple_ops::transform(
      m_parent_coord, child_coord, stapl::plus<std::size_t>());
    return linearizer_t(m_parent_dimension)(parent_coord);
  }

  template <typename Op, typename LevelDims>
  void compute_result_ids(Op&& op, LevelDims&& level_dims)
  {
    m_res_ids.clear();
    for (auto dir : m_sides) {
      m_should_flow.set_direction(dir);
      m_should_flow.compute_result_ids(std::forward<Op>(op),
                                       std::forward<LevelDims>(level_dims));
      for (auto res_ids : m_should_flow.get_result_ids()) {
        if (std::end(m_res_ids) ==
            std::find(m_res_ids.begin(), m_res_ids.end(), res_ids))
          m_res_ids.push_back(res_ids);
      }
    }
  }

  bool should_flow(std::size_t result_id) const
  {
    return std::end(m_res_ids) !=
           std::find(m_res_ids.begin(), m_res_ids.end(), result_id);
  }

  should_flow_t get_should_flow() const
  {
    return m_should_flow;
  }

  std::vector<std::size_t> get_result_ids() const
  {
    return m_res_ids;
  }

  void define_type(typer& t)
  {
    t.member(m_corner);
    t.member(m_cur_coord);
    t.member(m_parent_coord);
    t.member(m_cur_dimension);
    t.member(m_parent_dimension);
    t.member(m_task_dimension);
    t.member(m_should_flow);
    t.member(m_sides);
    t.member(m_res_ids);
  }
};


/////////////////////////////////////////////////////////////////////////
/// @brief  Specialization for 1D dimension
/////////////////////////////////////////////////////////////////////////
template <>
class default_output_to_output_mapper<1>
{

  using should_flow_t = recursive_should_flow<1>;
  using corner_t = skeletons::position;
  using corner_type = std::array<corner_t, 1>;
  using dir_t = direction;

  using domain_type = indexed_domain<std::size_t, 1>;
  using index_type = typename domain_type::index_type;
  using traversal_t = typename domain_type::traversal_type;
  using linearizer_t = nd_linearize<index_type, traversal_t>;
  using rev_linearizer_t = nd_reverse_linearize<index_type, traversal_t>;

  corner_type m_corner;
  index_type m_cur_coord;
  index_type m_cur_dimension;
  index_type m_parent_coord;
  index_type m_parent_dimension;
  index_type m_task_dimension;
  should_flow_t m_should_flow;

  std::array<dir_t, 1>  m_sides;
  std::vector<std::size_t> m_res_ids;

public:
  void set_direction(dir_t)
  { }

  void set_dimensions(index_type const& cur_coord,
                      index_type const& cur_dimension,
                      index_type const& parent_dimension,
                      index_type const& task_dimension)
  {
    m_cur_coord = cur_coord;
    m_cur_dimension = cur_dimension;
    m_parent_dimension = parent_dimension;
    m_task_dimension = task_dimension;
    m_parent_coord = m_cur_coord * m_task_dimension;
    m_should_flow.set_dimension(task_dimension);

    for (std::size_t dir = 0; dir < 1; ++dir)
      m_sides[dir] = (dir_t) dir;

  }

  std::size_t operator()(std::size_t idx) const
  {
    using namespace tuple_ops;

    index_type child_coord  = rev_linearizer_t(m_task_dimension)(idx);
    index_type parent_coord = m_parent_coord + child_coord;
    return linearizer_t(m_parent_dimension)(parent_coord);
  }

  template <typename Op, typename LevelDims>
  void compute_result_ids(Op&& op, LevelDims&& level_dims)
  {
    m_res_ids.clear();
    for (auto dir : m_sides) {
      m_should_flow.set_direction(dir);
      m_should_flow.compute_result_ids(std::forward<Op>(op),
                                       std::forward<LevelDims>(level_dims));
      for (auto res_ids : m_should_flow.get_result_ids()) {
        if (std::end(m_res_ids) ==
            std::find(m_res_ids.begin(), m_res_ids.end(), res_ids))
          m_res_ids.push_back(res_ids);
      }
    }
  }

  bool should_flow(std::size_t result_id) const
  {
    return std::end(m_res_ids) !=
           std::find(m_res_ids.begin(), m_res_ids.end(), result_id);
  }

  should_flow_t get_should_flow() const
  {
    return m_should_flow;
  }

  std::vector<std::size_t> get_result_ids() const
  {
    return m_res_ids;
  }

  void define_type(typer& t)
  {
    t.member(m_corner);
    t.member(m_cur_coord);
    t.member(m_parent_coord);
    t.member(m_cur_dimension);
    t.member(m_parent_dimension);
    t.member(m_task_dimension);
    t.member(m_should_flow);
    t.member(m_sides);
    t.member(m_res_ids);
  }
};


/////////////////////////////////////////////////////////////////////////
/// @brief  default mapper when skeleton is consuming from the
///         the parent paragraph results. it maps the
///         result ids requested by the child, from the parent dimension
///         (upper level) to child dimension.
/// @tparam Dim dimension of skeleton
/////////////////////////////////////////////////////////////////////////
template <int Dim>
class default_input_to_input_mapper
{
  using should_flow_t = recursive_should_flow<Dim>;
  using corner_t = skeletons::position;
  using corner_type = std::array<corner_t, Dim>;
  using dir_t = direction;
  using domain_type = indexed_domain<std::size_t, Dim>;
  using index_type = typename domain_type::index_type;
  using traversal_t = typename domain_type::traversal_type;
  using linearizer_t = nd_linearize<index_type, traversal_t>;
  using rev_linearizer_t = nd_reverse_linearize<index_type, traversal_t>;

  should_flow_t m_should_flow;
  index_type m_consumer_coord;
  index_type m_consumer_dimension;
  index_type m_total_dimension;
  index_type m_task_dimension;
  dir_t m_dir;

  std::vector<std::size_t> m_res_ids;
  std::map<std::size_t, std::size_t> m_result_map;

public:
  void set_direction(dir_t dir)
  {
    // stapl_assert((int)dir >= 0 && (int)dir < Dim, "wrong mapper direction");
    m_dir = dir;
    m_res_ids.clear();
    m_result_map.clear();
    m_should_flow.set_direction(dir);
  }

  void set_dimensions(index_type const& consumer_coord,
                      index_type const& consumer_dimension,
                      index_type const& total_dimension,
                      index_type const& task_dimension)
  {
    using namespace tuple_ops;

    m_consumer_coord = consumer_coord;
    m_consumer_dimension = consumer_dimension;
    m_total_dimension = total_dimension;
    m_task_dimension = task_dimension;
    m_should_flow.set_dimension(task_dimension);
  }

  template <typename Op, typename LevelDims>
  void compute_result_ids(Op&& op, LevelDims&& level_dims)
  {
    rev_linearizer_t rev_linearizer(m_task_dimension);
    linearizer_t linearizer(m_total_dimension);

    m_should_flow.compute_result_ids(std::forward<Op>(op),
                                     std::forward<LevelDims>(level_dims));

    for (auto res_id : m_should_flow.get_result_ids()) {
      index_type res_child_coord = rev_linearizer(res_id);

      index_type parent_res_coord = tuple_ops::transform(
        m_consumer_coord, m_task_dimension, stapl::multiplies<std::size_t>());

      parent_res_coord = tuple_ops::transform(
        parent_res_coord, res_child_coord, stapl::plus<std::size_t>());

      std::size_t upper_level_idx = linearizer(parent_res_coord);
      m_res_ids.push_back(upper_level_idx);
      m_result_map[upper_level_idx] = res_id;
    }
  }

  std::size_t operator()(const std::size_t idx) const
  {
    return const_cast<default_input_to_input_mapper*>(this)->m_result_map[idx];
  }

  should_flow_t get_should_flow(void) const
  {
    return m_should_flow;
  }

  std::vector<size_t> get_result_ids() const
  {
    return m_res_ids;
  }

  void define_type(typer& t)
  {
    t.member(m_should_flow);
    t.member(m_consumer_coord);
    t.member(m_consumer_dimension);
    t.member(m_total_dimension);
    t.member(m_task_dimension);
    t.member(m_dir);
    t.member(m_res_ids);
    t.member(m_result_map);
  }
};

/////////////////////////////////////////////////////////////////////////
/// @brief  Specialization for 1D dimension
/////////////////////////////////////////////////////////////////////////
template <>
class default_input_to_input_mapper<1>
{
  using should_flow_t = recursive_should_flow<1>;
  using corner_t = skeletons::position;
  using corner_type = std::array<corner_t, 1>;
  using dir_t = direction;
  using domain_type = indexed_domain<std::size_t, 1>;
  using index_type = typename domain_type::index_type;
  using traversal_t = typename domain_type::traversal_type;
  using linearizer_t = nd_linearize<index_type, traversal_t>;
  using rev_linearizer_t = nd_reverse_linearize<index_type, traversal_t>;

  should_flow_t m_should_flow;
  index_type m_consumer_coord;
  index_type m_consumer_dimension;
  index_type m_total_dimension;
  index_type m_task_dimension;
  dir_t m_dir;

  std::vector<std::size_t> m_res_ids;
  std::map<std::size_t, std::size_t> m_result_map;

public:
  void set_direction(dir_t dir)
  {
    m_dir = dir;
    m_res_ids.clear();
    m_result_map.clear();
    m_should_flow.set_direction(dir);
  }

  void set_dimensions(index_type const& consumer_coord,
                      index_type const& consumer_dimension,
                      index_type const& total_dimension,
                      index_type const& task_dimension)
  {
    using namespace tuple_ops;

    m_consumer_coord = consumer_coord;
    m_consumer_dimension = consumer_dimension;
    m_total_dimension = total_dimension;
    m_task_dimension = task_dimension;
    m_should_flow.set_dimension(task_dimension);
  }

  template <typename Op, typename LevelDims>
  void compute_result_ids(Op&& op, LevelDims&& level_dims)
  {
    rev_linearizer_t rev_linearizer(m_task_dimension);
    linearizer_t linearizer(m_total_dimension);

    m_should_flow.compute_result_ids(std::forward<Op>(op),
                                     std::forward<LevelDims>(level_dims));
    for (auto res_id : m_should_flow.get_result_ids()) {

      index_type res_child_coord = rev_linearizer(res_id);
      index_type parent_res_coord = m_consumer_coord * m_task_dimension;
      parent_res_coord += res_child_coord;

      std::size_t upper_level_idx = linearizer(parent_res_coord);
      m_res_ids.push_back(upper_level_idx);
      m_result_map[upper_level_idx] = res_id;
    }
  }

  std::size_t operator()(const std::size_t idx) const
  {
    return const_cast<default_input_to_input_mapper*>(this)->m_result_map[idx];
  }

  should_flow_t get_should_flow(void) const
  {
    return m_should_flow;
  }

  std::vector<size_t> get_result_ids() const
  {
    return m_res_ids;
  }

  void define_type(typer& t)
  {
    t.member(m_should_flow);
    t.member(m_consumer_coord);
    t.member(m_consumer_dimension);
    t.member(m_total_dimension);
    t.member(m_task_dimension);
    t.member(m_dir);
    t.member(m_res_ids);
    t.member(m_result_map);
  }
};

} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_UTILITY_MAPPERS_HPP
