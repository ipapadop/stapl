/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_UTILITY_HASH_HPP
#define STAPL_UTILITY_HASH_HPP

#include "hash_fwd.hpp"
#include <stapl/utility/tuple.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Default hash function for sequential hash containers.
/// @ingroup utility
//////////////////////////////////////////////////////////////////////
template<typename T>
struct hash
  : public boost::hash<T>
{ };


namespace detail {

//////////////////////////////////////////////////////////////////////
/// @brief Recursive, static function to call @p boost::hash_combine()
///  on each member of the input tuple and update @p seed parameter
///  accordingly.
//////////////////////////////////////////////////////////////////////
template<typename Tuple, int N = tuple_size<Tuple>::value-1>
struct tuple_hash_impl
{
  void static apply(std::size_t& seed, Tuple const& t)
  {
    tuple_hash_impl<Tuple, N-1>::apply(seed, t);

    boost::hash_combine(seed, get<N>(t));
  }
};


template<typename Tuple>
struct tuple_hash_impl<Tuple, 0>
{
  void static apply(std::size_t& seed, Tuple const& t)
  {
    boost::hash_combine(seed, get<0>(t));
  }
};

} // namespace detail


//////////////////////////////////////////////////////////////////////
/// @brief Specialization of hash for tuple. Use @p boost::hash_combine()
///   to incorporate all elements of tuple into hash value.
//////////////////////////////////////////////////////////////////////
template<typename ...Args>
struct hash<tuple<Args...>>
{
  std::size_t operator()(tuple<Args...> const& t) const
  {
    std::size_t seed = 0;

    detail::tuple_hash_impl<tuple<Args...>>::apply(seed, t);

    return seed;
  }
};

} // namespace stapl

#endif // STAPL_UTILITY_HASH_HPP

