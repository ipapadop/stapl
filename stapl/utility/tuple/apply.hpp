/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_UTILITY_TUPLE_APPLY_HPP
#define STAPL_UTILITY_TUPLE_APPLY_HPP

#include <stapl/runtime/type_traits/callable_traits.hpp>
#include <stapl/utility/integer_sequence.hpp>
#include "tuple_size.hpp"
#include <type_traits>
#include <utility>
#include <stapl/utility/utility.hpp>

namespace stapl {
namespace tuple_ops {

//////////////////////////////////////////////////////////////////////
/// @brief Calls @p f with arguments unpacked from tuple @p t in the order given
///        by the @ref index_sequence.
//////////////////////////////////////////////////////////////////////
template<typename F, typename Tuple, std::size_t... Is>
constexpr auto
apply_impl(F&& f, Tuple&& t, index_sequence<Is...>)
STAPL_AUTO_RETURN(
  std::forward<F>(f)(get<Is>(std::forward<Tuple>(t))...)
)

//////////////////////////////////////////////////////////////////////
/// @brief Invokes pointer member function @p pmf on object @p obj with
///        arguments unpacked from tuple @p t in the order given by the
///        @ref index_sequence.
//////////////////////////////////////////////////////////////////////
template<typename Obj, typename PMF, typename Tuple, std::size_t... Is>
constexpr typename callable_traits<PMF>::result_type
apply_impl(Obj&& obj, PMF const& pmf, Tuple&& t, index_sequence<Is...>)
{
  return (std::forward<Obj>(obj).*pmf)(get<Is>(std::forward<Tuple>(t))...);
}


//////////////////////////////////////////////////////////////////////
/// @brief Calls @p f with arguments unpacked from tuple @p t.
//////////////////////////////////////////////////////////////////////
template<typename F, typename Tuple>
constexpr auto apply(F&& f, Tuple&& t)
STAPL_AUTO_RETURN(
  apply_impl(
    std::forward<F>(f),
    std::forward<Tuple>(t),
    make_index_sequence<tuple_size<typename std::decay<Tuple>::type>::value>{})
)


//////////////////////////////////////////////////////////////////////
/// @brief Invokes pointer member function @p pmf on object @p obj with
///        arguments unpacked from tuple @p t.
//////////////////////////////////////////////////////////////////////
template<typename Obj, typename PMF, typename Tuple>
constexpr typename callable_traits<PMF>::result_type
apply(Obj&& obj, PMF const& pmf, Tuple&& t)
{
  return apply_impl(
           std::forward<Obj>(obj),
           pmf,
           std::forward<Tuple>(t),
           make_index_sequence<
             tuple_size<typename std::decay<Tuple>::type>::value
           >{});
}

} // namespace tuple_ops
} // namespace stapl

#endif // STAPL_UTILITY_TUPLE_APPLY_HPP
