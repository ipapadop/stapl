/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_UTILITY_TUPLE_ENSURE_TUPLE_HPP
#define STAPL_UTILITY_TUPLE_ENSURE_TUPLE_HPP

#include <type_traits>

#include <stapl/utility/utility.hpp>
#include "tuple.hpp"

namespace stapl {
namespace tuple_ops {
namespace result_of {

//////////////////////////////////////////////////////////////////////
/// @brief Metafunction that wraps a non-tuple type into a tuple of
///        size one. If the type is already a tuple, it acts as an
///        identity.
///
///        For example, ensure_tuple<char> would return tuple<char> but
///        ensure_tuple<tuple<char,float>> would return tuple<char,float>.
///
/// @tparam T The type to obtain the tuple from.
//////////////////////////////////////////////////////////////////////
template<typename T>
struct ensure_tuple
{
  using type = stapl::tuple<T>;

  template<typename U>
  static type apply(U&& u)
  {
    return std::forward_as_tuple(std::forward<U>(u));
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Specialization when T is already a tuple.
//////////////////////////////////////////////////////////////////////
template<typename... Args>
struct ensure_tuple<stapl::tuple<Args...>>
{
  using type = stapl::tuple<Args...>;

  static type apply(type const& t)
  {
    return t;
  }
};

} // namespace result_of

//////////////////////////////////////////////////////////////////////
/// @brief Always return a tuple representation of given argument.
///        If it is already a tuple, return it unchanged.
///
/// @param tup Object to obtain the tuple from.
//////////////////////////////////////////////////////////////////////
template<typename T>
auto ensure_tuple(T&& tup)
STAPL_AUTO_RETURN (
  result_of::ensure_tuple<typename std::decay<T>::type>::apply(
    std::forward<T>(tup))
)

template<typename T>
using ensure_tuple_t = typename tuple_ops::result_of::ensure_tuple<T>::type;

} // namespace tuple_ops

} // namespace stapl

#endif // STAPL_UTILITY_TUPLE_ENSURE_TUPLE_HPP
