/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_UTILITY_TUPLE_EXTRACT_1D_HPP
#define STAPL_UTILITY_TUPLE_EXTRACT_1D_HPP

#include <type_traits>

#include <stapl/utility/utility.hpp>
#include "tuple.hpp"

namespace stapl {
namespace tuple_ops {

template <typename T, std::size_t sz = tuple_size<T>::value>
struct extract_1D_impl
{
  T static apply(T const& t)
  {
    return t;
  }
};

template <typename T>
struct extract_1D_impl<T, 1>
{
  typename tuple_element<0, T>::type
  static apply(T const& t)
  {
    return get<0>(t);
  }
};

template <typename T>
auto extract_1D(T&& t) -> decltype(
  extract_1D_impl<typename std::decay<T>::type>::apply(std::forward<T>(t)))
{
  return extract_1D_impl<typename std::decay<T>::type>::apply(
    std::forward<T>(t));
}

} // namespace tuple_ops

} // namespace stapl

#endif // STAPL_UTILITY_TUPLE_EXTRACT_1D_HPP
