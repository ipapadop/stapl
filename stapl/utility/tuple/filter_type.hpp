/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_UTILITY_TUPLE_FILTER_TYPE_HPP
#define STAPL_UTILITY_TUPLE_FILTER_TYPE_HPP

#include "tuple.hpp"
#include "push_front.hpp"

namespace stapl {
namespace tuple_ops {

//////////////////////////////////////////////////////////////////////
/// @brief Filter the types in a tuple based on a predicate metafunction.
//////////////////////////////////////////////////////////////////////
template<class Tuple, template<class...> class F, class... Args>
struct filter_types;

template<template<class...> class F, class... Args>
struct filter_types<tuple<>, F, Args...>
{
  using type = tuple<>;

  static type call(type const& t)
  {
    return t;
  }

  template<size_t I, class T>
  static type apply(T const&)
  {
    return {};
  }
};

namespace detail {

template<bool ShouldPush, typename Tuple, typename>
struct push_type_if
{
  using type = Tuple;

  template<class T>
  static type call(Tuple const& tuple, T const&)
  {
    return tuple;
  }
};

template<typename Tuple, typename T>
struct push_type_if<true, Tuple, T>
  : result_of::push_front<Tuple, T>
{ };

} // namespace detail

template<class T, class... Ts, template<class...> class F,
  class... Args>
struct filter_types<tuple<T, Ts...>, F, Args...>
{
private:
  using recursive = filter_types<tuple<Ts...>, F, Args...>;
  static constexpr bool keep = F<T, Args...>::value;
  using push = detail::push_type_if<keep, typename recursive::type, T>;

public:
  using type = typename push::type;

  template<class Tuple>
  inline static type call(Tuple const& t)
  {
    return apply<0>(t);
  }

  template<size_t I, class Tuple>
  inline static type apply(Tuple const& tuple)
  {
    return push::call(recursive::template apply<I+1>(tuple), get<I>(tuple));
  }
};

} // namespace tuple_ops
} // namespace stapl

#endif // STAPL_UTILITY_TUPLE_FILTER_TYPE_HPP
