/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_UTILITY_TUPLE_INSERT_SORTED_HPP
#define STAPL_UTILITY_TUPLE_INSERT_SORTED_HPP


#include "tuple.hpp"
#include <stapl/utility/type_less.hpp>
#include <stapl/utility/type_identity.hpp>
namespace stapl {
namespace tuple_ops {

namespace detail {

template<typename Seen, template<typename,typename> class Pred,
  typename Items, typename T>
struct insert_sorted_impl;

template<typename... Seen, template<typename,typename> class Pred,
  typename Head, typename... Tail, typename T>
struct insert_sorted_impl<tuple<Seen...>, Pred, tuple<Head, Tail...>, T>
  : std::conditional<Pred<T, Head>::value,
      type_identity<tuple<Seen..., T, Head, Tail...>>,
      insert_sorted_impl<tuple<Seen..., Head>, Pred, tuple<Tail...>, T>>::type
{ };

template<typename... Seen, template<typename, typename> class Pred, typename T>
struct insert_sorted_impl<tuple<Seen...>, Pred, tuple<>, T>
{
  using type = tuple<Seen..., T>;
};

} // namespace detail

template<typename Items, typename T, template<typename, typename> class Pred>
using insert_sorted =
  typename detail::insert_sorted_impl<tuple<>, Pred, Items, T>;

template<typename Items, typename T>
using insert_sorted_less = insert_sorted<Items, T, type_less>;

} // namespace tuple_ops
} // namespace stapl
#endif // STAPL_UTILITY_TUPLE_INSERT_SORTED_HPP
