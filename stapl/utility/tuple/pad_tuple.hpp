/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_UTILITY_TUPLE_PAD_TUPLE_HPP
#define STAPL_UTILITY_TUPLE_PAD_TUPLE_HPP

#include <type_traits>

#include <stapl/utility/utility.hpp>
#include "tuple.hpp"

namespace stapl {
namespace tuple_ops {

template<std::size_t N, bool is_size_t>
struct apply_pad_tuple
{
  template<typename T>
  static auto apply(T&& t, size_t val)
    -> decltype(
        stapl::tuple_cat(homogeneous_tuple<N -1>(val), stapl::make_tuple(t)))
  {
    return stapl::tuple_cat(homogeneous_tuple<N -1>(val), stapl::make_tuple(t));
  }
};

template<std::size_t N>
struct apply_pad_tuple<N, false>
{
  template<typename T>
  static auto apply(T&& t, size_t val)
    -> decltype(
        stapl::tuple_cat(
          homogeneous_tuple<
            N - stapl::tuple_size<typename std::decay<T>::type>::value>(val),t))
  {
    return stapl::tuple_cat(
             homogeneous_tuple<
               N - stapl::tuple_size<typename std::decay<T>::type>::value>(val),
             t);
  }
};

template <std::size_t N, typename T>
auto
pad_tuple(T&& t, std::size_t val)
 -> decltype(
      apply_pad_tuple<
        N,
        std::is_same<
          std::size_t,
          typename std::decay<T>::type>::value>::apply(std::forward<T>(t), val))
{
  return apply_pad_tuple<
    N,
    std::is_same<std::size_t, typename std::decay<T>::type>::value
  >::apply(std::forward<T>(t), val);
}

} // namespace tuple_ops
} // namespace stapl

#endif // STAPL_UTILITY_TUPLE_PAD_TUPLE_HPP
