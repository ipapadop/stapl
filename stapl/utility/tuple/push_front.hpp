/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_UTILITY_TUPLE_PUSH_FRONT_HPP
#define STAPL_UTILITY_TUPLE_PUSH_FRONT_HPP

#include <stapl/utility/tuple/tuple.hpp>
#include <stapl/utility/integer_sequence.hpp>
#include <stapl/utility/tuple/tuple_element.hpp>
#include <stapl/utility/utility.hpp>

namespace stapl {
namespace tuple_ops {

namespace result_of {

template<typename Tuple, typename T>
struct push_front;

template <typename T>
struct push_front<tuple<>, T>
{
  using type = tuple<T>;

  static type call(tuple<> const&, T const& val)
  {
    return type(val);
  }
};

template <typename ...Elements, typename T>
struct push_front<tuple<Elements...>, T>
{
  using type = tuple<T, Elements...>;

private:
  template<size_t... Indices>
  static type apply(tuple<Elements...> const& elements, T const& val,
      index_sequence<Indices...>)
  {
    return type(val, get<Indices>(elements)...);
  }

public:
  static type call(tuple<Elements...> const& elements, T const& val)
  {
    return apply(elements, val, make_index_sequence<sizeof...(Elements)>{});
  }
};


} // namespace result_of

//////////////////////////////////////////////////////////////////////
/// @brief Returns a new tuple with @c val added at the beginning.
///
/// @param t   a tuple
/// @param val the new element to be added at the beginning
//////////////////////////////////////////////////////////////////////
template<typename ...Elements, typename T>
auto push_front(tuple<Elements...> const& t, T const& val)
STAPL_AUTO_RETURN((
  result_of::push_front<tuple<Elements...>, T>::call(t, val)
))

} // namespace tuple_ops
} // namespace stapl

#endif // STAPL_UTILITY_TUPLE_PUSH_FRONT_HPP
