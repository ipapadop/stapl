/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_UTILITY_TYPE_PRINTER_HPP
#define STAPL_UTILITY_TYPE_PRINTER_HPP

#include <stapl/runtime/system.hpp>
#include <iosfwd>
#include <string>
#include <typeinfo>

namespace stapl {

////////////////////////////////////////////////////////////////////
/// @brief Prints the given type through the @ref apply(std::ostream&) function.
///
/// @ingroup utility
////////////////////////////////////////////////////////////////////
template<typename T>
struct type_printer
{
  static std::ostream& apply(std::ostream& os)
  { return os << runtime::demangle(typeid(T).name()); }
};


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref type_printer for @c const types.
///
/// @ingroup utility
////////////////////////////////////////////////////////////////////
template<typename T>
struct type_printer<const T>
{
  static std::ostream& apply(std::ostream& os)
  { return type_printer<T>::apply(os) << " const"; }
};


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref type_printer for @c volatile types.
///
/// @ingroup utility
////////////////////////////////////////////////////////////////////
template<typename T>
struct type_printer<volatile T>
{
  static std::ostream& apply(std::ostream& os)
  { return type_printer<T>::apply(os) << " volatile"; }
};


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref type_printer for @c const @c volatile types.
///
/// @ingroup utility
////////////////////////////////////////////////////////////////////
template<typename T>
struct type_printer<const volatile T>
{
  static std::ostream& apply(std::ostream& os)
  { return type_printer<T>::apply(os) << " const volatile"; }
};


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref type_printer for references.
///
/// @ingroup utility
////////////////////////////////////////////////////////////////////
template<typename T>
struct type_printer<T&>
{
  static std::ostream& apply(std::ostream& os)
  { return type_printer<T>::apply(os) << '&'; }
};


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref type_printer for pointers.
///
/// @ingroup utility
////////////////////////////////////////////////////////////////////
template<typename T>
struct type_printer<T*>
{
  static std::ostream& apply(std::ostream& os)
  { return type_printer<T>::apply(os) << '*'; }
};


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref type_printer for rvalue references.
///
/// @ingroup utility
////////////////////////////////////////////////////////////////////
template<typename T>
struct type_printer<T&&>
{
  static std::ostream& apply(std::ostream& os)
  { return type_printer<T>::apply(os) << "&&"; }
};


template<typename T>
std::ostream& operator<<(std::ostream& os, type_printer<T> const&)
{
  return type_printer<T>::apply(os);
}



////////////////////////////////////////////////////////////////////
/// @brief Prints the given list of types to the @c std::ostream.
///
/// A delimiter between the types can be specified at the constructor.
///
/// @ingroup utility
////////////////////////////////////////////////////////////////////
template<typename... T>
struct typelist_printer;


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref typelist_printer for two or more types.
///
/// @ingroup utility
////////////////////////////////////////////////////////////////////
template<typename H, typename... T>
struct typelist_printer<H, T...>
{
  std::string delimiter;

  explicit typelist_printer(const char* delim = ", ")
  : delimiter(delim)
  { }

  explicit typelist_printer(std::string const& delim)
  : delimiter(delim)
  { }

  std::ostream& apply(std::ostream& os) const
  {
    return os << type_printer<H>() << delimiter
              << typelist_printer<T...>(delimiter);
  }
};


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref typelist_printer for one type.
///
/// @ingroup utility
////////////////////////////////////////////////////////////////////
template<typename T>
struct typelist_printer<T>
{
  explicit typelist_printer(const char* = ", ")
  { }

  explicit typelist_printer(std::string const&)
  { }

  static std::ostream& apply(std::ostream& os)
  { return os << type_printer<T>(); }
};


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref typelist_printer for no types.
///
/// @ingroup utility
////////////////////////////////////////////////////////////////////
template<>
struct typelist_printer<>
{
  explicit typelist_printer(const char* = ", ")
  { }

  explicit typelist_printer(std::string const&)
  { }

  static std::ostream& apply(std::ostream& os)
  { return os; }
};


template<typename... T>
std::ostream& operator<<(std::ostream& os, typelist_printer<T...> const& p)
{
  return p.apply(os);
}



////////////////////////////////////////////////////////////////////
/// @brief Creates a @ref typelist_printer from the list of objects.
///
/// @related typelist_printer
/// @ingroup utility
////////////////////////////////////////////////////////////////////
template<typename... U>
typelist_printer<U...> object_type_printer(U...)
{
  return typelist_printer<U...>();
}

} // namespace stapl

#endif
