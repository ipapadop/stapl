/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_VIEWS_ADAPTORS_TBB_RANGE_ADAPTOR_HPP
#define STAPL_VIEWS_ADAPTORS_TBB_RANGE_ADAPTOR_HPP

#include <tbb/tbb_stddef.h>
#include <stapl/containers/partitions/balanced.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Adaptor that allows creation of a TBB compatible range from a view.
//////////////////////////////////////////////////////////////////////
template<typename View>
class tbb_range_adaptor
{
public:
  typedef typename View::const_iterator const_iterator;
  typedef typename View::iterator       iterator;

private:
  typedef indexed_domain<size_t> offset_domain_type;

  /// View being adapted
  View const*        m_vw;

  /// Offsets of view elements an instance of the adaptor represents
  offset_domain_type m_dom;

public:
  explicit tbb_range_adaptor(View const& view)
    : m_vw(&view), m_dom(0, m_vw->domain().size()-1)
  { }

  tbb_range_adaptor(tbb_range_adaptor const& other)
    : m_vw(other.m_vw), m_dom(other.m_dom)
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief Constructor that splits the elements referenced by @p other
  ///        into two views.  The first half is assigned to @p other and the
  ///        second half is assigned to the newly constructed view.
  //////////////////////////////////////////////////////////////////////
  tbb_range_adaptor(tbb_range_adaptor& other, tbb::split)
    : m_vw(other.m_vw)
  {
    balanced_partition<offset_domain_type> part(other.m_dom, 2);
    other.m_dom = part[0];
    m_dom = part[1];
  }

  bool is_divisible(void) const
  {
    return (m_dom.size()>1);
  }

  bool empty(void) const
  {
    return m_dom.empty();
  }

  const_iterator begin(void) const
  {
    return const_iterator(m_vw->begin()+m_dom.first());
  }

  const_iterator end(void) const
  {
    return const_iterator(m_vw->begin()+m_dom.last()+1);
  }

  iterator begin(void)
  {
    return iterator(m_vw->begin()+m_dom.first());
  }

  iterator end(void)
  {
    return iterator(m_vw->begin()+m_dom.last()+1);
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Creates a new @ref tbb_range_adaptor from the given view.
//////////////////////////////////////////////////////////////////////
template<typename View>
tbb_range_adaptor<View> make_tbb_range(View const& vw)
{
  return tbb_range_adaptor<View>(vw);
}

} // namespace stapl

#endif
