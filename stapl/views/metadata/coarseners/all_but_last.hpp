/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_VIEWS_METADATA_COARSEN_ALL_BUT_LAST_HPP
#define STAPL_VIEWS_METADATA_COARSEN_ALL_BUT_LAST_HPP

#include <stapl/utility/tuple.hpp>
#include <type_traits>
#include <boost/utility/result_of.hpp>
#include <boost/mpl/eval_if.hpp>
#include "null.hpp"

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Functor to coarsen a set of given views, where the last
///        view in the given set of views is coarsened using the
///        null_coarsener.
//////////////////////////////////////////////////////////////////////
template<typename Coarsener>
struct coarsen_all_but_last
{
  template<typename>
  struct result;

  template<typename F, typename Views>
  struct result<F(Views)>
  {
    typedef const typename tuple_ops::result_of::pop_back<Views>::type views1_t;

    typedef typename stapl::tuple<
              typename std::remove_reference<
                typename tuple_ops::result_of::back<Views>::type>::type
            > views2_t;

    typedef typename boost::mpl::eval_if_c<
      stapl::tuple_size<Views>::value == 1,
      typename std::result_of<null_coarsener(Views)>,
      typename result_of::tuple_cat<
        typename std::result_of<Coarsener(views1_t)>::type,
        typename std::result_of<null_coarsener(views2_t)>::type
      >
      >::type type;
  };

  template <typename Views>
  auto apply(Views const& views, integral_constant<std::size_t, 1>) const
  STAPL_AUTO_RETURN(null_coarsener()(views))

  template <typename Views, typename VSize>
  auto apply(Views const& views, VSize) const
  STAPL_AUTO_RETURN(
    tuple_cat(
      Coarsener()(tuple_ops::pop_back(views)),
      null_coarsener()(stapl::make_tuple(tuple_ops::back(views)))))

  template <typename Views>
  typename boost::result_of<coarsen_all_but_last(Views)>::type
  operator()(Views const& views) const
  {
    return apply(views, stapl::tuple_size<Views>());
  }
};

} // namespace stapl

#endif // STAPL_VIEWS_METADATA_COARSEN_ALL_BUT_LAST_HPP
