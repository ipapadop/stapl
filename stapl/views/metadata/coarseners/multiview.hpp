/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_VIEWS_COARSEN_MULTIVIEWS_HPP
#define STAPL_VIEWS_COARSEN_MULTIVIEWS_HPP

#include <boost/mpl/int.hpp>
#include <boost/mpl/count_if.hpp>

#include <stapl/runtime.hpp>
#include <stapl/paragraph/paragraph_fwd.h>

#include <stapl/views/metadata/coarseners/default.hpp>
#include <stapl/views/metadata/coarsen_utility.hpp>
#include <stapl/views/metadata/alignment/guided.hpp>

#include <stapl/views/metadata/utility/are_aligned.hpp>
#include <stapl/views/metadata/utility/have_equal_sizes.hpp>

namespace stapl {

namespace metadata {

//////////////////////////////////////////////////////////////////////
/// @brief helper class template for @ref multiview_coarsener, dispatching
///   coarsener implementation based on the number of finite views found
///   in the set of views passed in tuple parameter @p Views.
///
/// Primary template is default implementation, used when multiple finite
/// domain views are detected, requiring alignment checks / enforcement.
//////////////////////////////////////////////////////////////////////
template<typename Views,
         bool Align,
         int FiniteViews= boost::mpl::count_if<
           Views, has_finite_domain<boost::mpl::_>
          >::value>
struct multiview_coarsener_impl
{
private:
  typedef metadata_from_container<Views>                  md_extractor_t;
  typedef typename md_extractor_t::type                   md_t;
  typedef typename first_finite_domain_index<Views>::type guide_idx_t;

public:
  static
  auto apply(Views const& views)
    ->decltype(coarsen_views_native<Views>::apply(
                 views, md_extractor_t::apply(views)))
  {
    constexpr int guide = guide_idx_t::value;

    stapl_assert(metadata::have_equal_sizes<guide>(views),
      "Attempting to coarsen views that have unequal sizes");

    // get metadata from the views
    md_t md_conts = md_extractor_t::apply(views);

    // check to see if the metadata containers have the same number of entries
    const bool b_same_num_entries = metadata::have_equal_sizes(md_conts);

    // if the metadata containers have the same number of entries and they
    // are already aligned, then just transform the fine-grained views
    // using the standard metadata
    if (b_same_num_entries && metadata::entries_are_aligned<guide>(md_conts))
      return coarsen_views_native<Views>::apply(views, md_conts);

    // views are not aligned, so invoke the alignment algorithm
    return metadata::guided_alignment<Views>::apply(
      views, md_conts, metadata::have_static_metadata(md_conts), guide_idx_t()
    );
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Specialization when one finite view is found.  No alignment
///   (or corresponding check needed).  Call native coarsener.
//////////////////////////////////////////////////////////////////////
template<typename Views, bool Align>
struct multiview_coarsener_impl<Views, Align, 1>
{
private:
  typedef coarsen_views_native<Views> base_coarsener;

public:
  static auto apply(Views const& views)
  STAPL_AUTO_RETURN(
    base_coarsener::apply(
      views, base_coarsener::metadata_extractor_type::apply(views)))
};

//////////////////////////////////////////////////////////////////////
/// @brief Specialization when no finite views are found.
///   Statically assert as this case does not make sense for this
///   coarsening approach.
//////////////////////////////////////////////////////////////////////
template<typename Views, bool Align>
struct multiview_coarsener_impl<Views, Align, 0>
{
  static_assert(sizeof(Views) == 0,
                "Must provide at least one view with a finite domain.");
};

//////////////////////////////////////////////////////////////////////
/// @brief Specialization for the case when alignment is explicitly disabled.
//////////////////////////////////////////////////////////////////////
template<typename Views, int FiniteViews>
struct multiview_coarsener_impl<Views, false, FiniteViews>
{
private:
  typedef metadata_from_container<Views>                  md_extractor_t;
  typedef typename md_extractor_t::type                   md_t;

public:
  static
  auto apply(Views const& views)
    ->decltype(coarsen_views_native<Views>::apply(
                 views, md_extractor_t::apply(views)))
  {
    using guide = typename first_finite_domain_index<Views>::type;

    stapl_assert(metadata::have_equal_sizes<guide::value>(views),
      "Attempting to coarsen views that have unequal sizes");

    // create metadata containers for each view using extractors
    // and projection algorithms
    md_t md_conts = md_extractor_t::apply(views);

    // create coarsened views from the metadata containers
    return coarsen_views_native<Views>::apply(views, md_conts);
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Specialization for the case when a @ref partitioned_mix_view
///   is present in the input view set.
///
/// Do not process the partitioned_mix_views as they already represent
/// coarsened views.
///
/// @todo We currently assume that the partitioned_mix_view comes first
///   in the view set. This could be generalized by using
///   @ref stapl::find_first_index.
//////////////////////////////////////////////////////////////////////
template<typename View0, typename Part, typename CC,
         bool Align, int FiniteViews, typename... Views>
struct multiview_coarsener_impl<
         tuple<partitioned_mix_view<View0, Part, CC>, Views...>,
         Align, FiniteViews>
{
private:
  using ViewSet = tuple<partitioned_mix_view<View0, Part, CC>, Views...>;

  using md_extractor_t = metadata_from_container<ViewSet>;
  using md_t = typename md_extractor_t::type;

public:
  static
  auto apply(ViewSet const& views)
    ->decltype(coarsen_views_native<ViewSet>::apply(
                 views, md_extractor_t::apply(views)))
  {
    // get metadata from the views
    md_t md_conts = md_extractor_t::apply(views);

    // check to see if the metadata containers have the same number of entries
    // and are properly aligned
    stapl_assert(metadata::have_equal_sizes(md_conts)
      && metadata::entries_are_aligned<0>(md_conts),
      "Attempting to use coarsened views with incompatible metadata containers."
      " When passing multiple pre-coarsened views to a paragraph, make sure"
      " they have been properly aligned.");

    return coarsen_views_native<ViewSet>::apply(views, md_conts);
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Specialization for the single @ref partitioned_mix_view.
///
/// Do not process the partitioned_mix_view as it already represents
/// a coarsened view.
//////////////////////////////////////////////////////////////////////
template<typename View0, typename Part, typename CC, bool Align>
struct multiview_coarsener_impl<
         tuple<partitioned_mix_view<View0, Part, CC>>,
         Align, 1>
{
private:
  using Views = tuple<partitioned_mix_view<View0, Part, CC>>;
  using base_coarsener = coarsen_views_native<Views>;

public:
  static auto apply(Views const& views)
  STAPL_AUTO_RETURN(
    base_coarsener::apply(
      views, base_coarsener::metadata_extractor_type::apply(views)))
};

} // namespace metadata


//////////////////////////////////////////////////////////////////////
/// @brief Functor to coarsen a set of given views.
///
/// If all views are aligned a native coarsener is used.  Otherwise, an
/// aligning coarsener using one of the finite views as the "guide" is
/// employed. Instances of partitioned_mix_view are not coarsened.
///
/// @todo Currently, coarsening acts as an identity on partitioned_mix_view,
///   which is not semantically correct - we should rather produce a
///   partitioned_mix_view<partitioned_mix_view<View, ...>, ...>. See
///   GFORGE #1500.
//////////////////////////////////////////////////////////////////////
template<bool Align>
struct multiview_coarsener
{
  template<typename Views>
  auto operator()(Views const& views) const
  STAPL_AUTO_RETURN(
    STAPL_PROXY_CONCAT(
      metadata::multiview_coarsener_impl<Views, Align>::apply(views)
    )
  )
};

} // namespace stapl

// TODO(mani) remove these from here and add includes to the places that use
// these files.
#include <stapl/views/metadata/container/growable.hpp>
#include <stapl/views/metadata/container/flat.hpp>
#include <stapl/views/metadata/container/projected.hpp>
#include <stapl/views/metadata/container/infinite.hpp>
#include <stapl/views/metadata/container/generator.hpp>
#include <stapl/views/metadata/container/metadata_view.hpp>

#endif // STAPL_VIEWS_COARSEN_MULTIVIEWS_HPP
