/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_VIEWS_METADATA_EXTRACTION_DOMAIN_CONTAINER_HPP
#define STAPL_VIEWS_METADATA_EXTRACTION_DOMAIN_CONTAINER_HPP

#include <stapl/views/metadata/container/growable.hpp>
#include <stapl/views/metadata/metadata_entry.hpp>

namespace stapl {

namespace metadata {

//////////////////////////////////////////////////////////////////////
/// @brief Functor to create metadata for a @see domain_view
///
/// This extractor expects the container to be a
/// @see view_impl::domain_container
///
/// @tparam C container that uses a domain as element storage
//////////////////////////////////////////////////////////////////////
template<typename C>
class domain_container_extractor
{
  using native_part_type =
    typename std::remove_pointer<
      typename coarsen_partition_impl::coarsen_partition<
        typename C::view_type>::return_type::second_type
    >::type;

  using view_md_type  = typename native_part_type::value_type;
  using domain_type   = typename view_md_type::domain_type;
  using md_entry_type = metadata_entry<domain_type, C*, size_t>;

public:
  using md_cont_type = growable_container<md_entry_type>;
  using return_type  = std::pair<bool, md_cont_type*>;

  return_type operator()(C* cont) const
  {
    // get the metadata for the original view
    auto original_view = cont->view();

    native_part_type* md_cont =
      coarsen_partition_impl::
        coarsen_partition<decltype(original_view)>::
          apply(&original_view).second;

    md_cont_type* out_part = new md_cont_type();

    // for each local metadata entry of the original view
    for (size_t i = 0; i < md_cont->local_size(); ++i)
    {
      // get the md entry and transform it to have C's container
      auto md = (*md_cont)[md_cont->get_local_vid(i)];

      md_entry_type new_md(
        md.id(), md.domain(), cont,
        md.location_qualifier(), md.affinity(), md.handle(), md.location()
      );

      // add the entry
      out_part->push_back_here(new_md);
    }

    out_part->update();

    delete md_cont;

    return std::make_pair(false, out_part);
  }
};

} // namespace metadata

} // namespace stapl

#endif
