/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_VIEWS_LOCALITY_DIST_METADATA_HPP
#define STAPL_VIEWS_LOCALITY_DIST_METADATA_HPP

#include <stapl/views/metadata/extraction/generic.hpp>
#include <stapl/views/type_traits/has_locality_metadata.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Helper to determine the metadata extractor type based on
///        the given container (@c C) type.
///
/// Uses the general container locality/distribution metadata
/// extractor.
/// @todo This catch all method for coarsening ignores the locality of
///   the underlying container and just creates a balanced partition of the
///   elements across the locations of the PARAGRAPH.  Containers should
///   explicitly request this behavior instead of a fallback. Otherwise we
///   silently perform badly.  Force all containers to define
///   loc_dist_metadata.
//////////////////////////////////////////////////////////////////////
template<typename C, bool = detail::has_loc_dist_metadata<C>::value>
struct locality_dist_metadata
{
  typedef generic_metadata_extraction<C> type;
};


//////////////////////////////////////////////////////////////////////
/// @brief Helper to determine the metadata extractor type based on
///        the given container (@c C) type.
///
/// The container/distribution customized its extractor and reflects
/// it via typedef.
//////////////////////////////////////////////////////////////////////
template<typename C>
struct locality_dist_metadata<C, true>
{
  typedef typename C::loc_dist_metadata type;
};

} // namespace stapl

#endif // STAPL_VIEWS_LOCALITY_DIST_METADATA_HPP
