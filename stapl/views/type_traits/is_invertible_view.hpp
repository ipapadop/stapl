/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_VIEWS_TYPE_TRAITS_IS_INVERTIBLE_VIEW_HPP
#define STAPL_VIEWS_TYPE_TRAITS_IS_INVERTIBLE_VIEW_HPP

#include <boost/mpl/has_xxx.hpp>
#include <stapl/views/metadata/infinite_helpers.hpp>

namespace stapl {

namespace detail {

//////////////////////////////////////////////////////////////////////
/// @brief Metafunction to determine if an object has a nested trait inverse
//////////////////////////////////////////////////////////////////////
BOOST_MPL_HAS_XXX_TRAIT_DEF(inverse)

//////////////////////////////////////////////////////////////////////
/// @brief Metafunction to check if the domain of the view and its underlying
/// container are the same type.
///
/// The specialization of the class is needed to prevent instantiation of
/// view_traits on std container types.
//////////////////////////////////////////////////////////////////////
template <typename View, typename Container,
          bool has_dom = has_domain_type<Container>::value>
struct has_same_domain
  : public std::false_type
{ };

template <typename View, typename Container>
struct has_same_domain<View, Container, true>
  : public std::is_same<
      typename view_traits<View>::domain_type,
      typename view_traits<Container>::domain_type
    >
{ };

} // namespace detail


//////////////////////////////////////////////////////////////////////
/// @brief Metafunction to determine if a view has an inverse function
///        defined for its mapping function and a finite domain.
//////////////////////////////////////////////////////////////////////
template <typename View>
struct is_invertible_view
  : std::integral_constant<bool,
      // the mapping function has an inverse defined
      detail::has_inverse<typename view_traits<View>::map_function>::value &&
      // the view's domain is finite
      has_finite_domain<View>::value &&
      !std::is_same<typename view_traits<View>::container,
        std::vector<typename view_traits<View>::value_type>>::value &&
      // the view and its container have the same domain
      detail::has_same_domain<View,
        typename view_traits<View>::container>::value
    >
{ };

} //namespace stapl

#endif /* STAPL_VIEWS_TYPE_TRAITS_IS_INVERTIBLE_VIEW_HPP */
