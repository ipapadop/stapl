/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <iostream>

#include <stapl/algorithms/functional.hpp>
#include <stapl/algorithms/algorithm.hpp>

#include <stapl/views/array_view.hpp>
#include <stapl/views/array_ro_view.hpp>
#include <stapl/views/counting_view.hpp>

#include <stapl/containers/array/array.hpp>

#include "../../test_report.hpp"

using namespace stapl;


class custom_gen_cont
  : public p_object
{
private:
  size_t m_size;

public:
  using value_type      = int;
  using reference       = value_type;
  using const_reference = const value_type;
  using domain_type     = indexed_domain<size_t>;
  using gid_type        = size_t;

  custom_gen_cont(size_t size)
    : m_size(size)
  {
    srand(time(NULL) + (get_location_id() + 100) * 3 + get_location_id());
  }

  const_reference operator[](size_t n) const
  {
    return ((n == m_size-1) || (n == m_size-2)) ? 0 : n + 1;
  }

  const_reference get_element(size_t n) const
  {
    return ((n == m_size-1) || (n == m_size-2)) ? 0 : n + 1;
  }

  template<typename Functor>
  const_reference apply_get(size_t idx, Functor const& f) const
  {
    return f(get_element(idx));
  }

  size_t size(void) const
  {
    return m_size;
  }

  domain_type domain(void) const
  {
    return domain_type(0, m_size-1);
  }

  void define_type(typer& t)
  {
    t.member(m_size);
  }

  size_t version(void) const
  {
    return 0;
  }
}; // class custom_gen_cont


stapl::exit_code stapl_main(int argc, char* argv[])
{
  size_t n = 100;

  if (argc == 2)
  {
    n = atoi(argv[1]);

    if (n < 2)
    {
      do_once([](void)
        { std::cout << "Problem size too small, should be greater than 2\n"; });

      exit(0);
    }
  }

  custom_gen_cont                cgc(n);
  array_ro_view<custom_gen_cont> cgenv(cgc);

  //
  // p_array test
  //
  using parray_t = array<int>;
  using view_t   = array_view<parray_t>;
  using ref_t    = view_t::reference;

  parray_t pa(n);
  view_t view(pa);

  //
  // Test match found.
  //
  copy(cgenv, view);
  const ref_t ref1   = find(view, 0);
  const bool result1 = !is_null_reference(ref1);

  //
  // Test NO match found
  //
  copy(counting_view<int>(n), view);
  const ref_t ref2   = find(view, n + 1);
  const bool result2 = is_null_reference(ref2);

  STAPL_TEST_REPORT(result1 && result2, "Testing find over array")

  return EXIT_SUCCESS;
}
