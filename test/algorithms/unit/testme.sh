#!/bin/sh

# Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
# component of the Texas A&M University System.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

run_command=$1
TOTAL_TESTS=14
PASSED_TEST=0

echo "-------- Testing `pwd` -------------"

eval $run_command ./search 1000 10
if test $? -eq 0
then
    PASSED_TEST=`expr $PASSED_TEST + 1`;
else
    echo "ERROR:: while testing search"
fi

eval $run_command ./search_n
if test $? -eq 0
then
    PASSED_TEST=`expr $PASSED_TEST + 1`;
else
    echo "ERROR:: while testing search_n"
fi

eval $run_command ./find 1234
if test $? -eq 0
then
    PASSED_TEST=`expr $PASSED_TEST + 1`;
else
    echo "ERROR:: while testing find"
fi

eval $run_command ./adjacent_difference 1234
if test $? -eq 0
then
    PASSED_TEST=`expr $PASSED_TEST + 1`;
else
    echo "ERROR:: while testing adjacent_difference"
fi

eval $run_command ./adjacent_find 1234
if test $? -eq 0
then
    PASSED_TEST=`expr $PASSED_TEST + 1`;
else
    echo "ERROR:: while testing adjacent_find"
fi

eval $run_command ./lexicographic_compare 1000 2000 1
if test $? -eq 0
then
    PASSED_TEST=`expr $PASSED_TEST + 1`;
else
    echo "ERROR:: while testing lexicographic_compare"
fi

eval $run_command ./generate 1234 1
if test $? -eq 0
then
    PASSED_TEST=`expr $PASSED_TEST + 1`;
else
    echo "ERROR:: while testing generate"
fi

eval $run_command ./mismatch 1234
if test $? -eq 0
then
    PASSED_TEST=`expr $PASSED_TEST + 1`;
else
    echo "ERROR:: while testing mismatch"
fi

eval $run_command ./min_element
if test $? -eq 0
then
    PASSED_TEST=`expr $PASSED_TEST + 1`;
else
    echo "ERROR:: while testing min_element"
fi

# merge two disjoint arrays
eval $run_command ./merge 10000 0
if test $? -eq 0
then
    PASSED_TEST=`expr $PASSED_TEST + 1`;
else
    echo "ERROR:: while testing merge (disjoint)"
fi

# merge two interleaved arrays
eval $run_command ./merge 10000 1
if test $? -eq 0
then
    PASSED_TEST=`expr $PASSED_TEST + 1`;
else
    echo "ERROR:: while testing merge (interleaved)"
fi

# merge two offset arrays
eval $run_command ./merge 10000 2
if test $? -eq 0
then
    PASSED_TEST=`expr $PASSED_TEST + 1`;
else
    echo "ERROR:: while testing merge (offset)"
fi

echo "----------------------------------------------------------------------"
echo "Tests executed: $PASSED_TEST/$TOTAL_TESTS"

