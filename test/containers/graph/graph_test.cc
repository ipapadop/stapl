/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/algorithms/algorithm.hpp>
#include <stapl/algorithms/functional.hpp>
#include <stapl/runtime.hpp>
#include <stapl/skeletons/serial.hpp>
#include <stapl/containers/graph/graph.hpp>
#include <stapl/containers/graph/views/graph_view.hpp>
#include <stapl/utility/do_once.hpp>

using namespace std;
typedef stapl::graph<stapl::DIRECTED, stapl::NONMULTIEDGES, size_t, size_t>
  grph_type;
typedef stapl::graph_view<grph_type> grph_view_type;
int test_func(grph_view_type const& view);


struct get_descriptor_wf
{
  typedef int result_type;
  template<typename View>
  result_type operator() (View v)
    {
      return v.descriptor();
    }
};

struct msg
{
  private:
  const char* m_txt;
  public:
  msg(const char* text)
    : m_txt(text)
      { }

  typedef void result_type;
  result_type operator() () {
    cout << m_txt << endl;
  }
  void define_type(stapl::typer& t) {
    t.member(m_txt);
  }
};


stapl::exit_code stapl_main(int argc, char** argv)
{
  grph_type grph1(5);
  grph_view_type grph_view1(grph1);
  int result1 = test_func(grph_view1);
  int result2 = 10;
  if (result1==result2)  {
    stapl::do_once( msg( "Test for graph const correctness PASSED" ) );
  }  else {
     stapl::do_once( msg( "Test for graph const correctness FAILED" ) );
  }
  return EXIT_SUCCESS;
}


int test_func(grph_view_type const& view)
{
  return stapl::map_reduce(get_descriptor_wf(), stapl::plus<size_t>(), view);
}

