/*
// Copyright (c) 2000-2009, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// The information and source code contained herein is the exclusive
// property of TEES and may not be disclosed, examined or reproduced
// in whole or in part without explicit written authorization from TEES.
*/

#include <stapl/containers/graph/multidigraph.hpp>
#include <stapl/containers/graph/views/graph_view.hpp>
#include <stapl/containers/graph/algorithms/breadth_first_search.hpp>

using namespace stapl;

stapl::exit_code stapl_main(int argc, char** argv)
{

  using page_rank_occupancy = sgl::always_active_superstep_occupancy_type;

  static_assert(sgl::compute_gid_set_type<sgl::frontier_type::bitmap,
                                          sgl::default_ordering,
                                          page_rank_occupancy>::value
                  == sgl::frontier_type::implied,
                "PageRank bitmap -> implied");
  static_assert(sgl::compute_gid_set_type<sgl::frontier_type::vector,
                                          sgl::default_ordering,
                                          page_rank_occupancy>::value
                  == sgl::frontier_type::implied,
                "PageRank vector -> implied");
  static_assert(sgl::compute_gid_set_type<sgl::frontier_type::implied,
                                          sgl::default_ordering,
                                          page_rank_occupancy>::value
                  == sgl::frontier_type::implied,
                "PageRank implied -> implied");

  using bfs_occupancy = sgl::default_superstep_occupancy_type;

  static_assert(sgl::compute_gid_set_type<sgl::frontier_type::vector,
                                          sgl::default_ordering,
                                          bfs_occupancy>::value
                  == sgl::frontier_type::vector,
                "BFS vector -> vector");
  static_assert(sgl::compute_gid_set_type<sgl::frontier_type::bitmap,
                                          sgl::default_ordering,
                                          bfs_occupancy>::value
                  == sgl::frontier_type::bitmap,
                "BFS bitmap -> bitmap");
  static_assert(sgl::compute_gid_set_type<sgl::frontier_type::implied,
                                          sgl::default_ordering,
                                          bfs_occupancy>::value
                  == sgl::frontier_type::implied,
                "BFS implied -> implied");

  return EXIT_SUCCESS;
}
