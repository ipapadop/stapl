/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_PROFILERS_VALUE_TYPE_HPP
#define STAPL_PROFILERS_VALUE_TYPE_HPP

#include <complex>
#include <iostream>
#include <stapl/algorithms/numeric.hpp>
#include <stapl/algorithms/identity_value.hpp>

////////////////////////////////////////////////////////////////////////////////
/// @brief A type that houses a container of elements and necessary operations.
///
/// @tparam The container type
/// @tparam N The size value type; int if unspecified
/// @tparam DL The time delay value type; int if unspecified
////////////////////////////////////////////////////////////////////////////////
template<class T, int N, int DL>
class my_variable
{
public:
  /// The container of elements
  T a[N];
  /// The counter value
  mutable size_t counter;

  //////////////////////////////////////////////////////////////////////////////
  /// @brief Initializes the container's elements to 0.
  //////////////////////////////////////////////////////////////////////////////
  my_variable(void)
  {
    for (size_t i=0; i<N; ++i)
      a[i] = 0;
    counter = 0;
  }

  //////////////////////////////////////////////////////////////////////////////
  /// @brief Initializes the container's elements to a value @p v.
  ///
  /// @param v The value to initialize the container's elements to
  //////////////////////////////////////////////////////////////////////////////
  my_variable(size_t v)
  {
    for (size_t i=0; i<N; ++i)
      a[i] = v;
    counter = 0;
  }

  my_variable& operator=(size_t v)
  {
    for (size_t i=0; i<N; ++i)
      a[i] = v;
    return *this;
  }

  bool operator==(my_variable const& other) const
  {
    bool eql=true;
    for (size_t i=0; i<N; ++i)
      if (a[i] != other.a[i])
        eql = false;
    return eql;
  }

  void operator+=(size_t v)
  {
    for (size_t i=0; i<N; ++i)
      a[i] += v;
  }

  my_variable operator+(size_t v) const
  {
    my_variable temp;
    for (size_t i=0; i<N; ++i)
      temp.a[i] = this->a[i] + v;
    return temp;
  }

  void operator+=(my_variable const& v)
  {
    for (size_t i=0; i<N; ++i)
      a[i] += v.a[i];
  }

  void define_type(stapl::typer& t)
  {
    t.member(a);
  }

  void delay() const
  {
    for (int i=0; i<DL; ++i)
      this->counter += (i / (lrand48()+1));
  }
};


namespace stapl {

////////////////////////////////////////////////////////////////////////////////
/// @brief A proxy for the my_variable type.
////////////////////////////////////////////////////////////////////////////////
template <class T, int N, int DL, typename Accessor>
class proxy<my_variable<T, N, DL>, Accessor>
  : public Accessor
{
private:
  friend class proxy_core_access;
  typedef my_variable<T, N, DL> target_t;

public:

  explicit proxy(Accessor const& acc)
  : Accessor(acc)
  { }

  operator target_t() const
  { return Accessor::read(); }

  proxy const& operator=(proxy const& rhs)
  { Accessor::write(rhs); return *this; }

  proxy const& operator=(target_t const& rhs)
  { Accessor::write(rhs); return *this;}

  void operator+=(size_t v)
  { Accessor::invoke(&target_t::operator+=, v); }

  void operator+=(target_t const& v)
  { Accessor::invoke(&target_t::operator+=, v); }

  target_t operator+(size_t v)
  { return Accessor::const_invoke(&target_t::operator+, v); }

}; //struct proxy
}

template<class T, int N, int DL>
std::ostream& operator<<(std::ostream& out, my_variable<T,N,DL> const& l)
{
  return out;
}

#endif
