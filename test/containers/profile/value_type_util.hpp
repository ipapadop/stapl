/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_PROFILING_VALUE_TYPE_HPP
#define STAPL_PROFILING_VALUE_TYPE_HPP

#include <iostream>
#include <stapl/algorithms/numeric.hpp>
#include <stapl/algorithms/identity_value.hpp>

////////////////////////////////////////////////////////////////////////////////
/// @brief Simple user defined type that houses a fixed-size array and provides
/// basic operations required by the profiling suite.
///
/// @tparam T type of the elements of the member array.
/// @tparam N Size of the member array.
////////////////////////////////////////////////////////////////////////////////
template<class T, size_t N>
class my_variable_type
{
  static_assert(N > 0, "Non-zero size for the member array of the user-defined "
    "class required.");

public:
  T a[N];

  //////////////////////////////////////////////////////////////////////////////
  /// @brief Initializes the elements of the member array to 0.
  //////////////////////////////////////////////////////////////////////////////
  my_variable_type(void)
  {
    for (size_t i=0; i<N; ++i)
      a[i] = 0;
  }

  //////////////////////////////////////////////////////////////////////////////
  /// @brief Initializes the elements of the member array to a value @p v.
  //////////////////////////////////////////////////////////////////////////////
  my_variable_type(size_t v)
  {
    for (size_t i=0; i<N; ++i)
      a[i] = v;
  }

  my_variable_type& operator=(size_t v)
  {
    for (size_t i=0; i<N; ++i)
      a[i] = v;
    return *this;
  }

  bool operator==(my_variable_type const& other) const
  {
    bool eql=true;
    for (size_t i=0; i<N; ++i)
    {
      if (a[i] != other.a[i])
      {
        eql = false;
        break;
      }
    }

    return eql;
  }

  void operator+=(size_t v)
  {
    for (size_t i=0; i<N; ++i)
      a[i] += v;
  }

  my_variable_type operator+(size_t v) const
  {
    my_variable_type temp;
    for (size_t i=0; i<N; ++i)
      temp.a[i] = this->a[i] + v;
    return temp;
  }

  void operator+=(my_variable_type const& v)
  {
    for (size_t i=0; i<N; ++i)
      a[i] += v.a[i];
  }

  void define_type(stapl::typer& t)
  {
    t.member(a);
  }
};

namespace stapl {

////////////////////////////////////////////////////////////////////////////////
/// @brief A proxy for the my_variable_type class.
////////////////////////////////////////////////////////////////////////////////
template <class T, size_t N, typename Accessor>
class proxy<my_variable_type<T, N>, Accessor>
  : public Accessor
{
private:
  friend class proxy_core_access;
  typedef my_variable_type<T, N> target_t;

public:

  explicit proxy(Accessor const& acc)
  : Accessor(acc)
  { }

  operator target_t() const
  { return Accessor::read(); }

  proxy const& operator=(proxy const& rhs)
  { Accessor::write(rhs); return *this; }

  proxy const& operator=(target_t const& rhs)
  { Accessor::write(rhs); return *this;}

  void operator+=(size_t v)
  { Accessor::invoke(&target_t::operator+=, v); }

  void operator+=(target_t const& v)
  { Accessor::invoke(&target_t::operator+=, v); }

  target_t operator+(size_t v)
  { return Accessor::const_invoke(&target_t::operator+, v); }

}; // class proxy

} // namespace stapl

template<class T, size_t N>
std::ostream& operator<<(std::ostream& out, my_variable_type<T,N> const& mv)
{
  out << "a: array of size " << N;

  if (N < 16)
  {
    out << ", { " << mv.a[0];
    for (size_t i = 1; i < N; ++i)
      out << ", " << mv.a[i];
    out << "}";
  }

  return out << std::endl;
}

using MVT = my_variable_type<int,8>;

#endif // STAPL_PROFILING_VALUE_TYPE_HPP
