/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

////////////////////////////////////////////////////////////////////////////////
/// @file
/// Common work functions used by the profilers.
///
/// @todo Add more complicated set/get functors.
////////////////////////////////////////////////////////////////////////////////

#ifndef TEST_CONTAINERS_PROFILE_WORKFUNCTIONS_HPP_
#define TEST_CONTAINERS_PROFILE_WORKFUNCTIONS_HPP_

namespace stapl {

namespace profiling {

////////////////////////////////////////////////////////////////////////////////
/// @brief Sets an element to a given value.
////////////////////////////////////////////////////////////////////////////////
template <class T>
struct set_op
{
  set_op(T val)
    : m_val(std::move(val))
  { }

  template<typename Reference>
  void operator()(Reference&& elem) const
  {
    elem = m_val;
  }

  T const& result() const
  {
    return m_val;
  }

  void define_type(stapl::typer& t)
  {
    t.member(m_val);
  }

private:
  T m_val;
};

////////////////////////////////////////////////////////////////////////////////
/// @brief Returns the element passed in (possibly unwrapping it from
/// its proxy representation).
////////////////////////////////////////////////////////////////////////////////
template <class T>
struct get_op
{
  using result_type = T;

  template<typename Reference>
  T operator()(Reference&& elem) const
  {
    return elem;
  }
};

} // namespace profiling

} // namespace stapl

#endif /* TEST_CONTAINERS_PROFILE_WORKFUNCTIONS_HPP_ */
