/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/runtime.hpp>

#include <cstdlib>
#include <iostream>

#include <stapl/containers/array/array.hpp>
#include <stapl/views/array_view.hpp>
#include <stapl/algorithms/algorithm.hpp>
#include <stapl/skeletons/explicit/new_map.h>

#include <test/algorithms/test_utils.h>

int x = 0;

struct my_ident
{
  typedef int result_type;

  int m_val;

  void define_type(stapl::typer& t)
  {
    t.member(m_val);
  }

  my_ident()
    : m_val(0)
  { }

  template<typename Reference>
  int operator()(Reference elem)
  {
    return elem + (++m_val);
  }
};


stapl::exit_code stapl_main(int argc, char* argv[])
{
  typedef stapl::array<int>                                               ct_t;
  typedef stapl::array_view<ct_t>                                         vw_t;
  typedef stapl::composition::result_of::map_func<my_ident, vw_t>::type   res_t;

  using stapl::fill;

  if (stapl::get_location_id() == 0)
    std::cout << "paragraph composition positive with "
              << stapl::get_num_locations() << " locations... ";

  const size_t nelems = stapl::get_num_locations() * 100;
  const size_t offset = stapl::get_location_id()   * 100;

  // Container and View Construction
  ct_t ct1(nelems);
  vw_t vw1(ct1);

  fill(vw1, stapl::get_location_id());

  res_t ret_vw1 = stapl::composition::map_func(my_ident(), vw1);

  res_t ret_vw2 = stapl::composition::map_func(my_ident(), ret_vw1);

  res_t ret_vw3 = stapl::composition::map_func(my_ident(), ret_vw2);

  bool error = ret_vw3.size() != nelems;

  for (int i=0; i < 100; ++i)
    if (ret_vw3[i + offset] != (int) ((i+1)*3 + stapl::get_location_id()))
      error = true;

  stapl::stapl_bool test_result(error);
  test_result.reduce();

  if (stapl::get_location_id() == 0)
  {
    if (test_result.value() == false)
      std::cout << "Passed\n";
    else
      std:: cout << "Failed\n";
  }

  return EXIT_SUCCESS;
}
