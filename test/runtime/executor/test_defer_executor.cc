/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


//////////////////////////////////////////////////////////////////////
/// @file
/// Test for avoiding executor creation / binding to parent.
//////////////////////////////////////////////////////////////////////

#include <stapl/runtime.hpp>
#include <stapl/runtime/executor/scheduler/sched.hpp>
#include <iostream>
#include "simple_executor.hpp"
#include "../test_utils.h"

using namespace stapl;

void dummy_task(void)
{ }

class p_test
: public p_test_object
{
public:
  p_test(void)
  {
    STAPL_RUNTIME_TEST_REQUIRE(
      runtime::this_context::get().get_location_md().try_get_executor()==nullptr
    );
  }

  void execute(void)
  {
    STAPL_RUNTIME_TEST_REQUIRE(
      runtime::this_context::get().get_location_md().try_get_executor()==nullptr
    );

    auto& ex = get_executor();

    STAPL_RUNTIME_TEST_REQUIRE(
      runtime::this_context::get().get_location_md().try_get_executor()==&ex);
  }
};


exit_code stapl_main(int, char*[])
{
  p_test pt;

  auto f = construct<p_test>(pt.get_rmi_handle(), all_locations);
  rmi_handle::reference r = f.get();

  STAPL_RUNTIME_TEST_REQUIRE(
    runtime::this_context::get().get_location_md().try_get_executor()==nullptr);

  async_rmi(all_locations, r, &p_test::execute);

  rmi_fence();

  STAPL_RUNTIME_TEST_REQUIRE(
    runtime::this_context::get().get_location_md().try_get_executor()==nullptr);

  p_object_delete<p_test> d;
  d(r);

  STAPL_RUNTIME_TEST_REQUIRE(
    runtime::this_context::get().get_location_md().try_get_executor()==nullptr);

#ifndef _TEST_QUIET
  std::cout << get_location_id() << " successfully passed!" << std::endl;
#endif
  return EXIT_SUCCESS;
}
