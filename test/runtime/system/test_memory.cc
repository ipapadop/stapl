/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#define STAPL_RUNTIME_TEST_MODULE test_memory
#include "utility.h"
#include <stapl/runtime/system.hpp>
#include <cstring>

using namespace stapl::runtime;

BOOST_AUTO_TEST_CASE( total_memory )
{
  const std::size_t mem = get_total_physical_memory();
  BOOST_TEST_MESSAGE("Total memory (MB): " << (mem/1024.0/1024.0));
}


BOOST_AUTO_TEST_CASE( used_memory )
{
  const std::size_t mem = get_used_physical_memory();
  BOOST_TEST_MESSAGE("Used memory (MB): " << (mem/1024.0/1024.0));
}


BOOST_AUTO_TEST_CASE( available_memory )
{
  const std::size_t mem = get_available_physical_memory();
  BOOST_TEST_MESSAGE("Available memory (MB): " << (mem/1024.0/1024.0));
}


BOOST_AUTO_TEST_CASE( comparison )
{
  BOOST_CHECK( get_total_physical_memory() > get_used_physical_memory() );
  BOOST_CHECK( get_total_physical_memory() > get_available_physical_memory() );
}


BOOST_AUTO_TEST_CASE( detect )
{
  const std::size_t consume_mb = 200; // consume this many MBs
  const std::size_t size = consume_mb * (1024*1024); // memory in bytes

  char* volatile c = new char[size];
  std::memset(c, '0', size*sizeof(*c)); // touch memory

  const std::size_t total = get_total_physical_memory();
  const std::size_t free  = get_available_physical_memory();

  BOOST_CHECK( total >= (free + size) );

  delete[] c;
}
