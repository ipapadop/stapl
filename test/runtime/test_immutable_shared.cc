/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


//////////////////////////////////////////////////////////////////////
/// @file
/// Test @ref stapl::immutable().
//////////////////////////////////////////////////////////////////////

#include <stapl/runtime.hpp>
#include <algorithm>
#include <iostream>
#include <limits>
#include <unordered_map>
#include "test_utils.h"

using namespace stapl;

class A
{
private:
  int m_i;

public:
  explicit A(int i = 0)
  : m_i(i)
  { }

  void define_type(typer& t)
  { t.member(m_i); }
};

class B
{
private:
  p_object* m_p;

public:
  explicit B(p_object* p = nullptr)
  : m_p(p)
  { }

  void define_type(typer& t)
  { t.member(m_p); }
};


class p_test
: public p_object
{
public:
  p_test(void)
  { this->advance_epoch(); }

  template<typename T>
  void consume(immutable_shared<T> const& t, unsigned int n)
  {
    STAPL_RUNTIME_TEST_CHECK(t.use_count(), n);
  }

  template<typename T>
  void copy_consume(immutable_shared<T> t, unsigned int n)
  {
    STAPL_RUNTIME_TEST_CHECK(t.use_count(), n);
  }

  template<typename T>
  void move_consume(immutable_shared<T>&& t, unsigned int n)
  {
    STAPL_RUNTIME_TEST_CHECK(t.use_count(), n);
  }

  void test_no_copy(void)
  {
    auto t = make_immutable_shared<A>(10);

    sync_rmi(this->get_location_id(), this->get_rmi_handle(),
             &p_test::consume<A>, t, 2);

    sync_rmi(this->get_location_id(), this->get_rmi_handle(),
             &p_test::copy_consume<A>, t, 3);

    sync_rmi(this->get_location_id(), this->get_rmi_handle(),
             &p_test::move_consume<A>, std::move(t), 1);

    rmi_fence(); //quiescence before next test
  }

  void test_copy(void)
  {
    auto t = make_immutable_shared<B>(this);

    sync_rmi(this->get_location_id(), this->get_rmi_handle(),
             &p_test::consume<B>, t, 1);

    sync_rmi(this->get_location_id(), this->get_rmi_handle(),
             &p_test::copy_consume<B>, t, 2);

    rmi_fence(); //quiescence before next test
  }

  void test_external_serialization(void)
  {
    std::unordered_map<int, A> m;
    m.emplace(std::piecewise_construct,
              std::forward_as_tuple(0),
              std::forward_as_tuple(0));
    m.emplace(std::piecewise_construct,
              std::forward_as_tuple(1),
              std::forward_as_tuple(1));
    m.emplace(std::piecewise_construct,
              std::forward_as_tuple(3),
              std::forward_as_tuple(3));
    auto t = make_immutable_shared<decltype(m)>(std::move(m));

    sync_rmi(this->get_location_id(), this->get_rmi_handle(),
             &p_test::consume<decltype(m)>, t, 2);

    sync_rmi(this->get_location_id(), this->get_rmi_handle(),
             &p_test::copy_consume<decltype(m)>, t, 3);

    sync_rmi(this->get_location_id(), this->get_rmi_handle(),
             &p_test::move_consume<decltype(m)>, std::move(t), 1);

    rmi_fence(); //quiescence before next test
  }

  void execute(void)
  {
    test_no_copy();
    test_copy();
    test_external_serialization();
  }
};


exit_code stapl_main(int, char*[])
{
  p_test pt;
  pt.execute();

#ifndef _TEST_QUIET
  std::cout << get_location_id() << " successfully passed!" << std::endl;
#endif
  return EXIT_SUCCESS;
}
