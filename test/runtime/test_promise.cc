/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


//////////////////////////////////////////////////////////////////////
/// @file
/// Test @ref stapl::promise.
//////////////////////////////////////////////////////////////////////

#include <stapl/runtime.hpp>
#include <iostream>
#include <vector>
#include "test_utils.h"

using namespace stapl;

struct p_test
: public p_object
{
  unsigned int right;

  p_test(void)
  : right(this->get_location_id()==(this->get_num_locations()-1)
          ? 0
          : this->get_location_id()+1)
  { this->advance_epoch(); }

  template<typename T>
  void use_promise_none(promise<T> p)
  { p.set_value(); }

  template<typename T>
  void use_promise(promise<T> p, T const& t)
  { p.set_value(t); }

  void test_fundamental(void)
  {
    {
      typedef void value_type;
      promise<value_type> p;
      future<value_type> f = p.get_future();
      async_rmi(right, this->get_rmi_handle(),
                &p_test::use_promise_none<value_type>, std::move(p));
      f.get();
    }

    {
      typedef bool value_type;

      const value_type value = false;
      promise<value_type> p;
      future<value_type> f = p.get_future();
      async_rmi(right, this->get_rmi_handle(),
                &p_test::use_promise<value_type>, std::move(p), value);
      STAPL_RUNTIME_TEST_CHECK(value, f.get());
    }

    {
      typedef char value_type;

      const value_type value = 'a';
      promise<value_type> p;
      future<value_type> f = p.get_future();
      async_rmi(right, this->get_rmi_handle(),
                &p_test::use_promise<value_type>, std::move(p), value);
      STAPL_RUNTIME_TEST_CHECK(value, f.get());
    }

    {
      typedef int value_type;

      const value_type value = 42;
      promise<value_type> p;
      future<value_type> f = p.get_future();
      async_rmi(right, this->get_rmi_handle(),
                &p_test::use_promise<value_type>, std::move(p), value);
      STAPL_RUNTIME_TEST_CHECK(value, f.get());
    }

    {
      typedef long value_type;

      const value_type value = 42;
      promise<value_type> p;
      future<value_type> f = p.get_future();
      async_rmi(right, this->get_rmi_handle(),
                &p_test::use_promise<value_type>, std::move(p), value);
      STAPL_RUNTIME_TEST_CHECK(value, f.get());
    }

    {
      typedef long long value_type;

      const value_type value = 42;
      promise<value_type> p;
      future<value_type> f = p.get_future();
      async_rmi(right, this->get_rmi_handle(),
                &p_test::use_promise<value_type>, std::move(p), value);
      STAPL_RUNTIME_TEST_CHECK(value, f.get());
    }

    {
      typedef float value_type;

      const value_type value = 42.0;
      promise<value_type> p;
      future<value_type> f = p.get_future();
      async_rmi(right, this->get_rmi_handle(),
                &p_test::use_promise<value_type>, std::move(p), value);
      STAPL_RUNTIME_TEST_REQUIRE(
        (value - f.get()) < std::numeric_limits<value_type>::epsilon());
    }

    {
      typedef double value_type;

      const value_type value = 42.0;
      promise<value_type> p;
      future<value_type> f = p.get_future();
      async_rmi(right, this->get_rmi_handle(),
                &p_test::use_promise<value_type>, std::move(p), value);
      STAPL_RUNTIME_TEST_REQUIRE(
        (value - f.get()) < std::numeric_limits<value_type>::epsilon());
    }

    rmi_fence(); // quiescence before next test
  }

  template<typename T>
  bool check_vector(std::vector<T> const& v, const std::size_t size)
  {
    if (v.size() != size)
      return false;
    for (std::size_t i = size; v.size() != size; --i) {
      if (v[size - i] != T(i))
        return false;
    }
    return true;
  }

  template<typename T>
  void make_vector(std::vector<T>& v, const std::size_t size)
  {
    for (std::size_t i = size; v.size() != size; --i)
      v.push_back(T(i));
  }

  template<typename T>
  void vector_return(const std::size_t s, promise<std::vector<T>> p)
  {
    std::vector<T> v;
    make_vector(v, s);
    STAPL_RUNTIME_TEST_CHECK(check_vector(v, s), true);
    p.set_value(std::move(v));
  }

  void test_vector(void)
  {
    const std::size_t N = 100;

    {
      typedef int                     value_type;
      typedef std::vector<value_type> vector_type;

      for (std::size_t i = 0; i < N; ++i) {
        promise<vector_type> p;
        future<vector_type> f = p.get_future();
        async_rmi(right, this->get_rmi_handle(),
                  &p_test::vector_return<value_type>, i, std::move(p));
        const vector_type v = f.get();
        STAPL_RUNTIME_TEST_CHECK(check_vector(v, i), true);
      }
    }

    {
      typedef pod_stapl               value_type;
      typedef std::vector<value_type> vector_type;

      for (std::size_t i = 0; i < N; ++i) {
        promise<vector_type> p;
        future<vector_type> f = p.get_future();
        async_rmi(right, this->get_rmi_handle(),
                  &p_test::vector_return<value_type>, i, std::move(p));
        const vector_type v = f.get();
        STAPL_RUNTIME_TEST_CHECK(check_vector(v, i), true);
      }
    }

    rmi_fence(); // quiescence before next test
  }

  void test_vector_deferred(void)
  {
    const std::size_t N = 100;

    {
      typedef int                     value_type;
      typedef std::vector<value_type> vector_type;

      future<vector_type> f[N];
      for (std::size_t i = 0; i < N; ++i) {
        promise<vector_type> p;
        f[i] = p.get_future();
        async_rmi(right, this->get_rmi_handle(),
                  &p_test::vector_return<value_type>, i, std::move(p));
      }

      for (std::size_t i = 0; i < N; ++i) {
        f[i].wait();
      }

      for (std::size_t i = 0; i < N; ++i) {
        const vector_type v = f[i].get();
        STAPL_RUNTIME_TEST_CHECK(check_vector(v, i), true);
      }
    }

    {
      typedef pod_stapl               value_type;
      typedef std::vector<value_type> vector_type;

      future<vector_type> f[N];
      for (std::size_t i = 0; i < N; ++i) {
        promise<vector_type> p;
        f[i] = p.get_future();
        async_rmi(right, this->get_rmi_handle(),
                  &p_test::vector_return<value_type>, i, std::move(p));
      }

      for (std::size_t i = 0; i < N; ++i) {
        f[i].wait();
      }

      for (std::size_t i = 0; i < N; ++i) {
        const vector_type v = f[i].get();
        STAPL_RUNTIME_TEST_CHECK(check_vector(v, i), true);
      }
    }

    rmi_fence(); // quiescence before next test
  }

  void execute(void)
  {
    test_fundamental();
    test_vector();
    test_vector_deferred();
  }
};


exit_code stapl_main(int, char*[])
{
  p_test pt;
  pt.execute();

#ifndef _TEST_QUIET
  std::cout << get_location_id() << " successfully passed!" << std::endl;
#endif
  return EXIT_SUCCESS;
}
