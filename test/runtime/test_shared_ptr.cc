/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


//////////////////////////////////////////////////////////////////////
/// @file
/// Test @c std::shared_ptr with @ref p_object.
//////////////////////////////////////////////////////////////////////

#include <stapl/runtime.hpp>
#include <algorithm>
#include <iostream>
#include <memory>
#include "test_utils.h"

using namespace stapl;

class dummy_base
: public p_object,
  public std::enable_shared_from_this<dummy_base>
{ };

class dummy
: public dummy_base
{
public:
  std::shared_ptr<dummy> shared_from_this(void)
  { return std::static_pointer_cast<dummy>(dummy_base::shared_from_this()); }
};


class p_test
: public p_object
{
private:
  unsigned int           m_right;
  std::shared_ptr<dummy> m_p;

public:
  p_test(void)
  : m_right(this->get_location_id()==(this->get_num_locations()-1)
            ? 0
            : this->get_location_id()+1)
  { this->advance_epoch(); }

  void recv_shared_ptr(std::shared_ptr<dummy> p)
  {
    STAPL_RUNTIME_TEST_CHECK(p.get(), m_p.get());
  }

  void execute(void)
  {
    m_p = std::make_shared<dummy>();

    rmi_fence();

    async_rmi(m_right, this->get_rmi_handle(), &p_test::recv_shared_ptr, m_p);

    rmi_fence();
  }
};



exit_code stapl_main(int, char*[])
{
  p_test pt;
  pt.execute();

#ifndef _TEST_QUIET
  std::cout << get_location_id() << " successfully passed!" << std::endl;
#endif
  return EXIT_SUCCESS;
}
