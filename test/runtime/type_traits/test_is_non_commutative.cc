/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#define STAPL_RUNTIME_TEST_MODULE is_non_commutative
#include "utility.h"
#include <stapl/runtime/type_traits/is_non_commutative.hpp>
#include <functional>

using namespace stapl;

template<typename T>
void test_type(T, const bool b)
{
  // T
  BOOST_CHECK_EQUAL(is_non_commutative<T>::value,
                    b);
  BOOST_CHECK_EQUAL(is_non_commutative<const T>::value,
                    b);
  BOOST_CHECK_EQUAL(is_non_commutative<volatile T>::value,
                    b);
  BOOST_CHECK_EQUAL(is_non_commutative<const volatile T>::value,
                    b);
  BOOST_CHECK_EQUAL(is_non_commutative<T>::value,
                    b);
  BOOST_CHECK_EQUAL(is_non_commutative<const T>::value,
                    b);
  BOOST_CHECK_EQUAL(is_non_commutative<volatile T>::value,
                    b);
  BOOST_CHECK_EQUAL(is_non_commutative<const volatile T>::value,
                    b);

  // T&
  BOOST_CHECK_EQUAL(is_non_commutative<T&>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<const T&>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<volatile T&>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<const volatile T&>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<T&>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<const T&>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<volatile T&>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<const volatile T&>::value,
                    false);

  // T*
  BOOST_CHECK_EQUAL(is_non_commutative<T*>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<const T*>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<volatile T*>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<const volatile T*>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<T*>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<const T*>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<volatile T*>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<const volatile T*>::value,
                    false);

  // T* const
  BOOST_CHECK_EQUAL(is_non_commutative<T* const>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<const T* const>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<volatile T* const>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<const volatile T* const>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<T* const>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<const T* const>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<volatile T* const>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<const volatile T* const>::value,
                    false);

  // T* volatile
  BOOST_CHECK_EQUAL(is_non_commutative<T* volatile>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<const T* volatile>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<volatile T* volatile>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<const volatile T* volatile>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<T* volatile>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<const T* volatile>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<volatile T* volatile>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<const volatile T* volatile>::value,
                    false);

  // T* const volatile
  BOOST_CHECK_EQUAL(is_non_commutative<T* volatile const>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<const T* volatile const>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<volatile T* volatile const>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<const volatile T* volatile const>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<T* volatile const>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<const T* volatile const>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<volatile T* volatile const>::value,
                    false);
  BOOST_CHECK_EQUAL(is_non_commutative<const volatile T* volatile const>::value,
                    false);
}

constexpr int foo(int x, int y)
{
  return (x*y);
}


BOOST_AUTO_TEST_CASE( test_commutative )
{
  test_type(std::plus<int>(), false);
}

BOOST_AUTO_TEST_CASE( test_non_commutative )
{
  test_type(non_commutative(std::plus<int>()), true);
}

BOOST_AUTO_TEST_CASE( test_commutative_function )
{
  test_type(&foo, false);
}

BOOST_AUTO_TEST_CASE( test_non_commutative_function )
{
  test_type(non_commutative(&foo), true);
}

