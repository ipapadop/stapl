/*
 // Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
 // component of the Texas A&M University System.

 // All rights reserved.

 // Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
 */

#define STAPL_RUNTIME_TEST_MODULE is_shared_ptr
#include "utility.h"
#include <stapl/runtime/type_traits/is_shared_ptr.hpp>
#include <functional>    // std::reference_wrapper
#include <boost/ref.hpp> // boost::reference_wrapper

using stapl::runtime::is_shared_ptr;

template<typename T>
void test(T t)
{
  BOOST_CHECK_EQUAL(is_shared_ptr<std::shared_ptr<T>>::value, true);
  BOOST_CHECK_EQUAL(is_shared_ptr<std::shared_ptr<const T>>::value, true);
  BOOST_CHECK_EQUAL(is_shared_ptr<std::shared_ptr<volatile T>>::value, true);
  BOOST_CHECK_EQUAL(is_shared_ptr<std::shared_ptr<const volatile T>>::value,
                    true);

  BOOST_CHECK_EQUAL(is_shared_ptr<boost::shared_ptr<T>>::value, true);
  BOOST_CHECK_EQUAL(is_shared_ptr<boost::shared_ptr<const T>>::value, true);
  BOOST_CHECK_EQUAL(is_shared_ptr<boost::shared_ptr<volatile T>>::value, true);
  BOOST_CHECK_EQUAL(is_shared_ptr<boost::shared_ptr<const volatile T>>::value,
                    true);

  BOOST_CHECK_EQUAL(is_shared_ptr<decltype(std::make_shared<T>(t))>::value,
                    true);

  // other
  BOOST_CHECK_EQUAL(is_shared_ptr<const T>::value, false);
  BOOST_CHECK_EQUAL(is_shared_ptr<volatile T>::value, false);
  BOOST_CHECK_EQUAL(is_shared_ptr<const volatile T>::value, false);

  BOOST_CHECK_EQUAL(is_shared_ptr<T&>::value, false);
  BOOST_CHECK_EQUAL(is_shared_ptr<T const&>::value, false);
  BOOST_CHECK_EQUAL(is_shared_ptr<T volatile&>::value, false);
  BOOST_CHECK_EQUAL(is_shared_ptr<T const volatile&>::value, false);

  BOOST_CHECK_EQUAL(is_shared_ptr<T*>::value, false);
  BOOST_CHECK_EQUAL(is_shared_ptr<const T*>::value, false);
  BOOST_CHECK_EQUAL(is_shared_ptr<volatile T*>::value, false);
  BOOST_CHECK_EQUAL(is_shared_ptr<const volatile T*>::value, false);

  BOOST_CHECK_EQUAL(is_shared_ptr<T* const>::value, false);
  BOOST_CHECK_EQUAL(is_shared_ptr<const T* const>::value, false);
  BOOST_CHECK_EQUAL(is_shared_ptr<volatile T* const>::value, false);
  BOOST_CHECK_EQUAL(is_shared_ptr<const volatile T* const>::value, false);

  BOOST_CHECK_EQUAL(is_shared_ptr<T* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_shared_ptr<const T* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_shared_ptr<volatile T* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_shared_ptr<const volatile T* volatile>::value, false);

  BOOST_CHECK_EQUAL(is_shared_ptr<T* volatile const>::value, false);
  BOOST_CHECK_EQUAL(is_shared_ptr<const T* volatile const>::value, false);
  BOOST_CHECK_EQUAL(is_shared_ptr<volatile T* volatile const>::value, false);
  BOOST_CHECK_EQUAL(is_shared_ptr<const volatile T* volatile const>::value,
                    false);
}


BOOST_AUTO_TEST_CASE( test_fundamental )
{
  test(int(10));
  test(double(42));
}


class A
{ };

struct B
{ };

union C
{ };

BOOST_AUTO_TEST_CASE( test_class )
{
  test(A());
  test(B());
  test(C());
}
