/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#define STAPL_RUNTIME_TEST_MODULE comparable_proxy
#include "utility.h"
#include <stapl/runtime/utility/comparable_proxy.hpp>
#include <tuple>
#include <utility>

using stapl::runtime::comparable_proxy;


struct empty_A
{ };


struct empty_B
{ };


BOOST_AUTO_TEST_CASE( test_same_empty )
{
  empty_A a1, a2;
  comparable_proxy ca1(a1), ca2(a2);
  BOOST_REQUIRE(ca1==a1);
  BOOST_REQUIRE(ca2==a2);
  BOOST_REQUIRE(ca1==ca2);
}


BOOST_AUTO_TEST_CASE( test_diff_empty )
{
  empty_A a;
  empty_B b;
  comparable_proxy ca(a), cb(b);
  BOOST_REQUIRE(ca==a);
  BOOST_REQUIRE(cb==b);
  BOOST_REQUIRE(ca!=cb);
}



struct A
{
  typedef std::tuple<int> member_types;
  int i;
};

constexpr bool operator==(A const& t1, A const& t2) noexcept
{ return (t1.i==t2.i); }


struct B
{
  typedef std::tuple<int> member_types;
  int i;
};

constexpr bool operator==(B const& t1, B const& t2) noexcept
{ return (t1.i==t2.i); }


BOOST_AUTO_TEST_CASE( test_same_value )
{
  A a1 = { 10 }, a2 = { 10 };
  comparable_proxy ca1(a1), ca2(a2);
  BOOST_REQUIRE(ca1==a1);
  BOOST_REQUIRE(ca2==a2);
  BOOST_REQUIRE(ca1==ca2);
}


BOOST_AUTO_TEST_CASE( test_diff_value )
{
  A a1 = { 10 }, a2 = { 11 };
  comparable_proxy ca1(a1), ca2(a2);
  BOOST_REQUIRE(ca1==a1);
  BOOST_REQUIRE(ca2==a2);
  BOOST_REQUIRE(ca1!=ca2);
}


BOOST_AUTO_TEST_CASE( test_diff_type )
{
  A a = { 10 };
  B b = { 10 };
  comparable_proxy ca(a), cb(b);
  BOOST_REQUIRE(ca==a);
  BOOST_REQUIRE(cb==b);
  BOOST_REQUIRE(ca!=cb);
}



void fooA(void)
{ }


void fooB(void)
{ }


BOOST_AUTO_TEST_CASE( test_same_function )
{
  comparable_proxy ca1(&fooA), ca2(&fooA);
  BOOST_REQUIRE(ca1==&fooA);
  BOOST_REQUIRE(ca2==&fooA);
  BOOST_REQUIRE(ca1==ca2);
}


BOOST_AUTO_TEST_CASE( test_diff_function )
{
  comparable_proxy ca(&fooA), cb(&fooB);
  BOOST_REQUIRE(ca==&fooA);
  BOOST_REQUIRE(cb==&fooB);
  BOOST_REQUIRE(ca!=cb);
}



BOOST_AUTO_TEST_CASE( test_same_pair )
{
  comparable_proxy ca(std::make_pair(empty_A{}, B{10}));
  comparable_proxy cb(std::make_pair(empty_A{}, B{10}));
  comparable_proxy cc(std::make_pair(empty_A{}, B{1}));
  BOOST_REQUIRE(ca==cb);
  BOOST_REQUIRE(ca!=cc);
}

BOOST_AUTO_TEST_CASE( test_diff_pair )
{
  comparable_proxy ca(std::make_pair(empty_A{}, B{10}));
  comparable_proxy cb(std::make_pair(empty_A{}, B{1}));
  comparable_proxy cc(std::make_pair(empty_A{}, empty_B{}));

  BOOST_REQUIRE(ca!=cb);
  BOOST_REQUIRE(ca!=cc);
}
