/*
 // Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
 // component of the Texas A&M University System.

 // All rights reserved.

 // Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
 */

#include <stapl/runtime.hpp>
#include <stapl/utility/tuple/tuple.hpp>
#include <stapl/skeletons/operators/consumer_count.hpp>
#include "../expect.hpp"

template <typename Customizer>
struct flow
{
  Customizer m_customizer;

  template <typename Coord>
  std::size_t consumer_count(Coord&& coord) const
  {
    return m_customizer(coord);
  }
};

stapl::exit_code stapl_main(int argc, char* argv[])
{
  using stapl::tuple;
  using stapl::make_tuple;
  using stapl::get;
  using stapl::skeletons::consumer_count;
  using stapl::tests::expect_eq;

  // Creating a synthetic coordinate
  using coord_t = tuple<int, int>;
  coord_t coord{1, 2};

  // Creating customizers to test various cases of flows
  auto customizer1 = [](coord_t const& coord) {
    return get<0>(coord) + get<1>(coord);
  };

  auto customizer2 = [](coord_t const& coord) {
    return get<0>(coord) * get<1>(coord);
  };

  auto customizer3 = [](coord_t const& coord) {
    return get<0>(coord);
  };

  // Using aggregate initializers to create two synthetic flows
  auto flow1 = flow<decltype(customizer1)>{customizer1};
  auto flow2 = flow<decltype(customizer2)>{customizer2};
  auto flow3 = flow<decltype(customizer3)>{customizer3};

  auto test_flow1 = flow1;
  auto test_flow2 = flow2;
  auto test_flow3 = flow3;
  auto test_flow4 = make_tuple(flow1);
  auto test_flow5 = make_tuple(flow1, flow2);
  auto test_flow6 = make_tuple(flow1, flow2, flow3);
  auto test_flow7 = make_tuple(make_tuple(flow1, flow2), flow3);
  auto test_flow8 = make_tuple(flow1, make_tuple(flow2, flow3));
  auto test_flow9 = make_tuple(flow1, make_tuple(make_tuple(flow2), flow3));

  expect_eq(consumer_count(test_flow1, coord), 3ul) << "test_flow1";
  expect_eq(consumer_count(test_flow2, coord), 2ul) << "test_flow2";
  expect_eq(consumer_count(test_flow3, coord), 1ul) << "test_flow3";
  expect_eq(consumer_count(test_flow4, coord), 3ul) << "test_flow4";
  expect_eq(consumer_count(test_flow5, coord), 5ul) << "test_flow5";
  expect_eq(consumer_count(test_flow6, coord), 6ul) << "test_flow6";
  expect_eq(consumer_count(test_flow7, coord), 6ul) << "test_flow7";
  expect_eq(consumer_count(test_flow8, coord), 6ul) << "test_flow8";
  expect_eq(consumer_count(test_flow9, coord), 6ul) << "test_flow9";

  return EXIT_SUCCESS;
}
