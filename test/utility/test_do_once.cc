/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/utility/do_once.hpp>
#include <stapl/runtime.hpp>

#include <iostream>
#include <boost/bind.hpp>

void foo() {
  std::cout << __func__ << "() called" << std::endl;
}

void foo1() {
  std::cout << __func__ << "() called" << std::endl;
}

void foo1(int i) {
  std::cout << __func__ << "() called: " << i << std::endl;
}

void foo1(int i, int j) {
  std::cout << __func__ << "() called: " << i << "," << j << std::endl;
}

int foo_i() {
  return 42;
}

struct my_functor1 {
  void operator()() {
    std::cout << "my_functor1::" << __func__ << "() called" << std::endl;
  }
};

struct my_functor2 {
  int operator()() {
    return 27;
  }
};

struct my_functor3 {
  typedef int result_type;
  int operator()() {
    return 27;
  }
};


void test_functions() {
  stapl::do_once( foo );

  stapl::do_once( foo1 );

  stapl::do_once( foo1, 42 );

  stapl::do_once( foo1, 42, 42 );

  stapl::do_once(foo_i);

  int r = stapl::do_once(foo_i);
  std::cout << "Node " << stapl::get_location_id() << " r = " << r << std::endl;
  stapl::rmi_fence();
}



void test_function_objects() {
  using boost::bind;

  stapl::do_once( (bind<void>(foo1, 10, 20)) );

  stapl::do_once( (bind<void>(foo1, 10, 20)) );

  stapl::do_once( (bind<void>(foo1, _1, 20)), 9 );

  // somehow this should work - result_type is defined
  int k = stapl::do_once( (bind<int>(foo_i)) );
  std::cout << "Node " << stapl::get_location_id() << " k = " << k << std::endl;

  stapl::do_once( my_functor1() );
  int l = stapl::do_once( my_functor2() );
  std::cout << "Node " << stapl::get_location_id() << " l = " << l << std::endl;

  int m = stapl::do_once( my_functor3() );
  std::cout << "Node " << stapl::get_location_id() << " m = " << m << std::endl;

  bool r = true;
  stapl::do_once([=](void) {
    if (r == true)
      std::cout << "True...\n";
    else
      std::cout << "False...\n";
  });
}


stapl::exit_code stapl_main(int, char*[])
{
  test_functions();
  test_function_objects();
  return EXIT_SUCCESS;
}
