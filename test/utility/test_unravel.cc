/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/utility/unravel_index.hpp>
#include <stapl/runtime/runtime.hpp>

#include "../test_report.hpp"

using namespace stapl;

template<typename T>
bool are_same(T const& t, std::initializer_list<std::size_t> u)
{
  return std::equal(std::begin(t), std::end(t), std::begin(u));
}

void test_1d()
{
  std::vector<std::size_t> shape{2};

  auto u = unravel_index(0, shape);

  STAPL_TEST_REPORT(are_same(u, {0}), "1d 0");

  u = unravel_index(1, shape);
  STAPL_TEST_REPORT(are_same(u, {1}), "1d 1");

}

void test_2d()
{
  std::vector<std::size_t> shape{11, 100};

  auto u = unravel_index(0, shape);

  STAPL_TEST_REPORT(are_same(u, {0, 0}), "2d 0");

  u = unravel_index(1, shape);
  STAPL_TEST_REPORT(are_same(u, {0, 1}), "2d 1");

  u = unravel_index(10, shape);
  STAPL_TEST_REPORT(are_same(u, {0, 10}), "2d 10");

  u = unravel_index(11, shape);
  STAPL_TEST_REPORT(are_same(u, {0, 11}), "2d 11");

  u = unravel_index(99, shape);
  STAPL_TEST_REPORT(are_same(u, {0, 99}), "2d 99");

  u = unravel_index(100, shape);
  STAPL_TEST_REPORT(are_same(u, {1, 0}), "2d 100");

  u = unravel_index(1023, shape);
  STAPL_TEST_REPORT(are_same(u, {10, 23}), "2d 1023");

}

stapl::exit_code stapl_main(int argc, char* argv[])
{

  test_1d();
  test_2d();

  return 0;
}
