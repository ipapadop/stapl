/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_TEST_VIEWS_METADATA_UTILS
#define STAPL_TEST_VIEWS_METADATA_UTILS

#include <stapl/views/metadata/coarseners/multiview.hpp>

namespace stapl {

template<typename... V>
auto coarsen_views(V... v)
  -> decltype(stapl::default_coarsener()(std::make_tuple(v...)))
{
  return stapl::default_coarsener()(std::make_tuple(v...));
}

template<typename View, typename MD>
bool coarsening_covers_space(View const& v, MD const& md)
{
  auto md_container_dom = md.domain();

  using index_type = decltype(md_container_dom.first());

  std::size_t elems_in_all_domains = 0;

  domain_map(md_container_dom, [&](index_type const& idx) {
    elems_in_all_domains += md[idx].domain().size();
  });

  // all locations have sum
  stapl::rmi_fence();

  return elems_in_all_domains == v.size();
}

template<typename V>
typename metadata::extract_metadata<
  typename std::decay<V>::type
>::return_type
extract_metadata_from_view(V&& v)
{
  typedef metadata::extract_metadata<
    typename std::decay<V>::type
  > extractor_t;

  return extractor_t()(&std::forward<V>(v));
}

} // namespace stapl

#endif
