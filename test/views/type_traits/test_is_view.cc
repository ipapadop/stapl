/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/views/type_traits/is_view.hpp>
#include <stapl/containers/array/array.hpp>
#include <stapl/skeletons/map.hpp>
#include <type_traits>
#include "../../test_report.hpp"

struct empty_wf
{
  typedef void result_type;

  template <typename View>
  void operator()(View) const
  { }
};

template <typename T>
bool trait_holds(T const&, bool expected,
                 typename std::enable_if<stapl::is_view<T>::value>::type* = 0)
{
  return true == expected;
}

template <typename T>
bool trait_holds(T const&, bool expected,
                 typename std::enable_if<!stapl::is_view<T>::value>::type* = 0)
{
  return false == expected;
}


stapl::exit_code stapl_main(int argc, char* argv[])
{
  typedef stapl::array<int>                            parray;
  typedef stapl::array_view<parray>                    parray_view;

  parray      pa(100);
  parray_view pav(pa);

  stapl::map_func(empty_wf(), pav);

  bool result = true;
  result &= trait_holds(pa, false);
  result &= trait_holds(pav, true);

  STAPL_TEST_REPORT(result, "Testing is_view<T>");

  return EXIT_SUCCESS;
}
