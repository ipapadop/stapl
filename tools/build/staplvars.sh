#!/bin/bash
# Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
# component of the Texas A&M University System.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# error printing
error()
{
  local parent_lineno="$1"
  local message="$2"
  local code="${3:-1}"
  if [[ -n "$message" ]] ; then
    echo -e "Line ${parent_lineno}: ${message}; exiting with status ${code}"
  else
    echo -e "Line ${parent_lineno}; exiting with status ${code}"
  fi
  exit "${code}"
}

# check if debug was added
if [[ "x$1" != "x" ]]; then
  if [[ "$1" != "debug" ]]; then
    error ${LINENO} "Only valid option is \"debug\"" 1
  fi
  debug=1
fi

# find the absolute parent directory the script is in
script_path="$( realpath ${BASH_SOURCE[0]} )"

# STAPL root path
STAPL_ROOT="$( cd -P "$( dirname "${script_path}" )" && cd .. && pwd)"
export STAPL_ROOT

# find which libstd we are using
if [[ "x${STAPL_STL_DIR}" = "x" ]] ; then
  # check if gcc version was given
  if [[ "x${GCC_VERSION}" = "x" ]] ; then
    GCC_VERSION=`gcc -dumpversion`
    # In some gcc installation, dumpversion is not working correctly and gives
    # only major and minor version. Use this line to attempt to fix that.
    #GCC_VERSION=`gcc --version | grep ^gcc | sed 's/^.* //g'`
    if [[ "x${GCC_VERSION}" = "x" ]] ; then
      error ${LINENO} "Could not automatically find gcc version. Either define one with GCC_VERSION or set STAPL_STL_DIR to the directory in tools/ that contains the headers for the compiler that will be used to compile STAPL applications." 1
    fi
    STAPL_STL_DIR="${STAPL_ROOT}/tools/libstdc++/${GCC_VERSION}"
  fi
fi

# check if the required STL directory exists
if [ ! -d ${STAPL_STL_DIR} ] ; then
  error ${LINENO} "${STAPL_STL_DIR} could not be found" 1
fi

# filled out by external methods (makefile, script etc)
ADDITIONAL_FLAGS="%ADDITIONAL_FLAGS%"
ADDITIONAL_LIBS="%ADDITIONAL_LIBS%"
LIB_SUFFIX="%LIB_SUFFIX%"

# STAPL required flags and libs
if [[ "x$debug" = "x" ]]; then
  # release
  STAPL_FLAGS="-D_STAPL -DSTAPL_NDEBUG -I${STAPL_STL_DIR} -I${STAPL_ROOT}/tools -I${STAPL_ROOT} ${ADDITIONAL_FLAGS}"
  STAPL_LIBS="-L${STAPL_ROOT}/lib -lstapl${LIB_SUFFIX} ${ADDITIONAL_LIBS}"
else
  # debug
  STAPL_FLAGS="-D_STAPL -I${STAPL_STL_DIR} -I${STAPL_ROOT}/tools -I${STAPL_ROOT} ${ADDITIONAL_FLAGS}"
  STAPL_LIBS="-L${STAPL_ROOT}/lib -lstapl${LIB_SUFFIX}_debug  ${ADDITIONAL_LIBS}"
fi
export STAPL_FLAGS
export STAPL_LIBS

printf "STAPL variables loaded:\n"
printf "  STAPL_FLAGS=${STAPL_FLAGS}\n"
printf "  STAPL_LIBS=${STAPL_LIBS}\n"
printf "\nCompile your STAPL application using:\n"
printf "  \$CC \$STAPL_FLAGS \$CXX_FLAGS app.cc -o app \$LIBS \$STAPL_LIBS\n"
