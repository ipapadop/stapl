/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_PROXY_PAIR_H
#define STAPL_PROXY_PAIR_H

#include <stapl/views/proxy_macros.hpp>

namespace stapl {

STAPL_PROXY_HEADER_TEMPLATE(std::pair, T, U)
{
  STAPL_PROXY_TYPES(STAPL_PROXY_CONCAT(std::pair<T, U>), Accessor)
  STAPL_PROXY_IMPORT_TYPES(first_type, second_type)
  STAPL_PROXY_METHODS(STAPL_PROXY_CONCAT(std::pair<T, U>), Accessor)

  STAPL_PROXY_MEMBER(first, T)
  STAPL_PROXY_MEMBER(second, U)

  explicit proxy(Accessor const& acc)
   : Accessor(acc),
     first(member_referencer<first_accessor>()(acc)),
     second(member_referencer<second_accessor>()(acc))
  { }
};

} // namespace stapl

#endif
